<?php
// Heading
$_['heading_title']     = 'POS Receipt';

// Text
$_['text_success']          = 'Success: You have modified Receipt!';
$_['text_list']             = 'Receipt List';
$_['text_add']              = 'Add Receipt';
$_['text_edit']             = 'Edit Receipt';
$_['text_thermal']          = 'Thermal';
$_['text_mini_thermal']     = 'Mini Thermal';
$_['text_a4']               = 'A4';
$_['text_form']             = 'Receipt Form';
$_['text_store_logo']       = 'Store logo';
$_['text_normal']           = 'Normal';
$_['text_bold']             = 'Bold';
$_['text_assign_outlet']    = 'Assign to outlets';
$_['text_not_found']        = 'Reciept not found';
$_['text_select_first']     = 'Please select first';

// Column
$_['column_name']     = 'Name';
$_['column_image']    = 'Image';
$_['column_status']   = 'Status';
$_['column_action']   = 'Action';
$_['column_outlet']   = 'Outlet';
$_['button_preview']  = 'Preview';


//Print
$_['text_date']             = 'Date';
$_['text_time']             = 'Time';
$_['text_order_id']         = 'Order Id';
$_['text_cashier']          = 'Cashier';
$_['text_customer']         = 'Customer';
$_['text_shipping_mode']    = 'Shipping Mode';
$_['text_payment_mode']     = 'Payment Mode';
$_['column_product']        = 'Product';
$_['column_quantity']       = 'Quantity';
$_['column_price']          = 'Price';
$_['column_amount']         = 'Amount';
$_['text_subtotal']         = '';
$_['text_grandtotal']       = 'Grand Total';
$_['text_note']             = 'Note';
$_['text_pickup']           = 'Pick Up';
$_['text_total_qua']        = 'Total Quantity';



// Entry
$_['entry_name']               = 'Enter the name';
$_['entry_store_logo']         = 'Show store logo';
$_['entry_store_name']         = 'Show store name';
$_['entry_store_address']      = 'Show store address';
$_['entry_order_date']         = 'Show order date';
$_['entry_order_time']         = 'Show order time';
$_['entry_order_id']           = 'Show order ID';
$_['entry_cashier_name']       = 'Show cashier name';
$_['entry_customer_name']      = 'Show customer name';
$_['entry_shipping_mode']      = 'Show shipping mode';
$_['entry_payment_mode']       = 'Show payment mode';
$_['entry_show_note']          = 'Show order note in invoice';
$_['entry_store_detail']       = 'Extra information (like TIN No., ST No.)';
$_['entry_paper_size']         = 'Paper Size';
$_['entry_order_note']         = 'Show order note';
$_['entry_font_weight']        = 'Font Weight For Print';
$_['entry_content_header']     = 'Content Header';
$_['entry_content_footer']     = 'Content Footer';
$_['status_content_header']    = 'Show Content Header';
$_['status_content_footer']    = 'Show Content Footer';
$_['entry_outlet']             = 'Outlets';
$_['help_outlet']              = 'Assign receipt to the Outlets';




$_['help_print_size']         = 'Please choose size according to your printer. A thermal has 80mm, mini thermal has 58mm and A4 has 158mm (595px) width respectively.';
$_['help_store_detail']  = 'This field will be visible after the address in the receipt, you can add your store details like your TIN number, service tax number etc, each in new line';

//tab
$_['tab_logo']          = 'Receipt Logo';
$_['tab_content']       = 'Receipt Content';

// Error
$_['error_name']        = 'Receipt Name must be greater than 3 and less than 64 characters!';
$_['error_permission']  = 'Warning: You do not have permission to modify Receipt!';
$_['error_warning']     = 'Warning: Please check the form carefully for errors!';