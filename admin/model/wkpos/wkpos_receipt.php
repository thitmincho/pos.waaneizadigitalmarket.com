<?php
class ModelWkposWkposReceipt extends Model {

  public function addReceipt($data = array()) {

    $this->db->query("INSERT INTO `" . DB_PREFIX . "wkpos_receipt` SET receipt_name = '" . $this->db->escape($data['receipt_name']) . "', print_size = '" . $this->db->escape($data['print_size']) . "', print_font_weight = '" . $this->db->escape($data['print_font_weight']) . "', store_name = '" . (int)$data['store_name'] . "', store_address = '" . (int)$data['store_address'] . "', order_id = '" . (int)$data['order_id'] . "', order_date = '" . (int)$data['order_date'] . "', order_time = '" . (int)$data['order_time'] . "', order_note = '" . (int)$data['order_note'] . "', cashier_name = '" . (int)$data['cashier_name'] . "', customer_name = '" . (int)$data['customer_name'] . "', shipping_mode = '" . (int)$data['shipping_mode'] . "', payment_mode = '" . (int)$data['payment_mode'] . "', store_logo = '" . (int)$data['store_logo'] . "', image = '" . $this->db->escape($data['image']) . "', extra_information = '" . $this->db->escape($data['extra_information']) . "', show_content_header = '" . (int)$data['show_content_header'] . "', content_header = '" . $this->db->escape($data['content_header']) . "', show_content_footer = '" . (int)$data['show_content_footer'] . "', content_footer = '" . $this->db->escape($data['content_footer']) . "', status = '" . (int)$data['status'] . "', date_added = NOW()");

    $receipt_id = $this->db->getLastId();

    if(isset($data['outlets'])) {
      foreach($data['outlets'] as $outlet_id) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "wkpos_receipt_mapping` WHERE outlet_id = '" . (int)$outlet_id . "'");

        if($query->num_rows) {
          $this->db->query("UPDATE `" . DB_PREFIX . "wkpos_receipt_mapping` SET receipt_id = '" . (int)$receipt_id . "' WHERE outlet_id = '" . (int)$outlet_id . "'");
        } else {
          $this->db->query("INSERT INTO `" . DB_PREFIX . "wkpos_receipt_mapping` SET receipt_id = '" . (int)$receipt_id . "', outlet_id = '" . (int)$outlet_id . "'");
        }
      }
    }
  }

  public function editReceipt($receipt_id, $data = array()) {

    $this->db->query("UPDATE `" . DB_PREFIX . "wkpos_receipt` SET receipt_name = '" . $this->db->escape($data['receipt_name']) . "', print_size = '" . $this->db->escape($data['print_size']) . "', print_font_weight = '" . $this->db->escape($data['print_font_weight']) . "', store_name = '" . (int)$data['store_name'] . "', store_address = '" . (int)$data['store_address'] . "', order_id = '" . (int)$data['order_id'] . "', order_date = '" . (int)$data['order_date'] . "', order_time = '" . (int)$data['order_time'] . "', order_note = '" . (int)$data['order_note'] . "', cashier_name = '" . (int)$data['cashier_name'] . "', customer_name = '" . (int)$data['customer_name'] . "', shipping_mode = '" . (int)$data['shipping_mode'] . "', payment_mode = '" . (int)$data['payment_mode'] . "', store_logo = '" . (int)$data['store_logo'] . "', image = '" . $this->db->escape($data['image']) . "', extra_information = '" . $this->db->escape($data['extra_information']) . "', show_content_header = '" . (int)$data['show_content_header'] . "', content_header = '" . $this->db->escape($data['content_header']) . "', show_content_footer = '" . (int)$data['show_content_footer'] . "', content_footer = '" . $this->db->escape($data['content_footer']) . "', status = '" . (int)$data['status'] . "' WHERE receipt_id = '" . (int)$receipt_id . "'");

    $this->db->query("DELETE FROM `" . DB_PREFIX . "wkpos_receipt_mapping` WHERE receipt_id = '" . (int)$receipt_id . "'");

    if(isset($data['outlets'])) {
      foreach($data['outlets'] as $outlet_id) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "wkpos_receipt_mapping` WHERE outlet_id = '" . (int)$outlet_id . "'");

        if($query->num_rows) {
          $this->db->query("UPDATE `" . DB_PREFIX . "wkpos_receipt_mapping` SET receipt_id = '" . (int)$receipt_id . "' WHERE outlet_id = '" . (int)$outlet_id . "'");
        } else {
          $this->db->query("INSERT INTO `" . DB_PREFIX . "wkpos_receipt_mapping` SET receipt_id = '" . (int)$receipt_id . "', outlet_id = '" . (int)$outlet_id . "'");
        }
      }
    }

  }

  public function getReceipts($data = array()) {

    $sql = "SELECT receipt_id , receipt_name, image , `status` FROM `" . DB_PREFIX . "wkpos_receipt` WHERE receipt_id > 0";

    if(!empty($data['filter_name'])) {
      $sql .= " AND receipt_name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
    }

    if(isset($data['filter_status']) && $data['filter_status'] !== '') {
      $sql .= " AND `status` = '" . (int)$data['filter_status'] . "'";
    }

    if (isset($data['sort']) && $data['sort'] == 'status') {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY receipt_name";
		}

    if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

    if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
  }

  public function getTotalReceipts($data = array()) {

    $sql = "SELECT COUNT(DISTINCT receipt_id) AS total FROM `" . DB_PREFIX . "wkpos_receipt` WHERE receipt_id > 0";

    if(!empty($data['filter_name'])) {
      $sql .= " AND receipt_name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
    }

    if(isset($data['filter_status']) && $data['filter_status'] !== '') {
      $sql .= " AND `status` = '" . (int)$data['filter_status'] . "'";
    }

    $query = $this->db->query($sql);

		return $query->row['total'];
  }

  public function getReceipt($receipt_id) {
    return $this->db->query("SELECT * FROM `" . DB_PREFIX . "wkpos_receipt` WHERE receipt_id = '" . (int)$receipt_id . "'")->row;
  }

  public function getOutletsByReceiptID($receipt_id) {
    $row = $this->db->query("SELECT outlet_id FROM `" . DB_PREFIX . "wkpos_receipt_mapping` WHERE receipt_id = '" . (int)$receipt_id . "'")->rows;
    $outlets = array();
    if($row) {
      foreach ($row as $value) {
        $outlets[] = $value['outlet_id'];
      }
    }
    return $outlets;
  }

  public function getOutlet($outlet_id) {
		return $this->db->query("SELECT outlet_id , name FROM " . DB_PREFIX . "wkpos_outlet WHERE outlet_id = '" . (int)$outlet_id . "'")->row;
	}

  public function deleteReceipt($receipt_id) {
    $this->db->query("DELETE FROM `" . DB_PREFIX . "wkpos_receipt` WHERE receipt_id = '" . (int)$receipt_id . "'");
  }

  public function getOutlets($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "wkpos_outlet";

    if(!empty($data['filter_name'])) {
      $sql .= " WHERE name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
    }

    $sql .= " ORDER BY name ASC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

  public function createTable() {
    $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "wkpos_receipt` (
		  `receipt_id`          INT(11) NOT NULL AUTO_INCREMENT,
      `receipt_name`        VARCHAR(100) NOT NULL,
      `print_size`          VARCHAR(10) NOT NULL,
		  `print_font_weight`   VARCHAR(10) NOT NULL,
		  `store_name`          TINYINT(1) NOT NULL,
      `store_address`       TINYINT(1) NOT NULL,
      `order_id`            TINYINT(1) NOT NULL,
      `order_date`          TINYINT(1) NOT NULL,
      `order_time`          TINYINT(1) NOT NULL,
      `order_note`          TINYINT(1) NOT NULL,
      `cashier_name`        TINYINT(1) NOT NULL,
      `customer_name`       TINYINT(1) NOT NULL,
      `shipping_mode`       TINYINT(1) NOT NULL,
      `payment_mode`        TINYINT(1) NOT NULL,
      `store_logo`          TINYINT(1) NOT NULL,
      `image`               VARCHAR(255) NOT NULL,
      `extra_information`   VARCHAR(400) NOT NULL,
      `show_content_header` TINYINT(1) NOT NULL,
      `content_header`      TEXT NOT NULL,
      `show_content_footer` TINYINT(1) NOT NULL,
      `content_footer`      TEXT NOT NULL,
      `status`              TINYINT(1) NOT NULL,
		  `date_added`          DATETIME NOT NULL,
		  PRIMARY KEY (`receipt_id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1");

    $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "wkpos_receipt_mapping` (
		  `receipt_id`  INT(11),
      `outlet_id`   INT(11)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");

  }

  public function deleteTable() {
    $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "wkpos_receipt`");
    $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "wkpos_receipt_mapping`");

  }


}
