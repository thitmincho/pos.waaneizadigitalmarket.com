<?php
class ControllerWkposWkposReceipt extends Controller {

  public function index() {
    $json = array();
    $this->load->model('wkpos/user');
    $outlets = array();

    if ($this->config->get('module_wkpos_receipt_status')) {
      $outlets = $this->model_wkpos_user->getOutlet();

      foreach($outlets as $outlet) {
        $outlet_info = $this->model_wkpos_user->getReceiptInfo($outlet['outlet_id']);
        if ($outlet_info) {
          $this->session->data['outlet_id'] = $outlet['outlet_id'];
          $recept = $this->load->controller('wkpos/wkpos_receipt/customizePosReceipt');
          $json['receipt'][] = array(
            'user_id' => $outlet['user_id'],
            'receipt' => $recept,
          );
          $json['success'] = true;
        }
      }
    }
    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  public function customizePosReceipt($data) {
    $data = array();
    $data = array_merge($data , $this->load->language('wkpos/wkpos'));
    $this->load->model('wkpos/user');
    $this->load->model('tool/image');

    $outlet_info = $this->model_wkpos_user->getReceiptInfo($this->session->data['outlet_id']);

    $data['store_name']         = $this->config->get('config_name');
    $data['show_store_name']    = $outlet_info['store_name'];
    $data['show_store_address'] = $outlet_info['store_address'];
    $data['show_order_date']    = $outlet_info['order_date'];
    $data['show_order_time']    = $outlet_info['order_time'];
    $data['show_order_id']      = $outlet_info['order_id'];
    $data['show_cashier_name']  = $outlet_info['cashier_name'];
    $data['show_customer_name'] = $outlet_info['customer_name'];
    $data['show_shipping_mode'] = $outlet_info['shipping_mode'];
    $data['show_payment_mode']  = $outlet_info['payment_mode'];
    $data['show_store_logo']    = $outlet_info['store_logo'];
    $data['paper_size']         = $outlet_info['print_size'];
    $data['font_weight']        = $outlet_info['print_font_weight'];
    $data['show_note']          = $outlet_info['order_note'];
    $data['store_logo']         = $this->model_tool_image->resize($outlet_info['image'], 200, 50);
    $data['show_header']        = $outlet_info['show_content_header'];
    $data['show_footer']        = $outlet_info['show_content_footer'];

    $store_address = preg_replace('~\r?\n~', "\n", $this->config->get('config_address'));
		$data['store_address'] = implode('<br>', explode("\n", ($store_address)));

    $data['text_header'] = html_entity_decode($outlet_info['content_header'], ENT_QUOTES, 'UTF-8');
    $data['text_footer'] = html_entity_decode($outlet_info['content_footer'], ENT_QUOTES, 'UTF-8');
    $store_detail = preg_replace('~\r?\n~', "\n", $outlet_info['extra_information']);
    $data['store_detail'] = implode('<br>', explode("\n", ($store_detail)));

    return $this->load->view('wkpos/wkpos_receipt' , $data);
  }
}
