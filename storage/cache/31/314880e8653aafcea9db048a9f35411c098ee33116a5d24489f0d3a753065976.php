<?php

/* wkpos/products.twig */
class __TwigTemplate_9871ad35f00d621b7f5bc54cc56a7ccac59a8f3475fd226f2c830ceba8308912 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1>";
        // line 5
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 8
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "      </ul>
      <div class=\"btn-group pull-right\">
        <button class=\"btn btn-success radius-0\" data-original-title=\"";
        // line 12
        echo (isset($context["help_mass_print"]) ? $context["help_mass_print"] : null);
        echo "\" data-toggle=\"tooltip\" onclick=\"massPrint();\"><i class=\"fa fa-print\"></i> ";
        echo (isset($context["text_mass_print"]) ? $context["text_mass_print"] : null);
        echo "</button>
      </div>
      <button type=\"button\" class=\"btn btn-info pull-right radius-0\" id=\"generate-barcodes\" onclick=\"massGenerate();\"><i class=\"fa fa-file\" aria-hidden=\"true\"></i>";
        // line 14
        echo (isset($context["text_mass_generate"]) ? $context["text_mass_generate"] : null);
        echo "</button>
    </div>
  </div>
  <div class=\"container-fluid\">
    <div class=\"progress\" style=\"display: none;\">
      <div class=\"progress-bar progress-bar-success progress-bar-striped active\" role=\"progressbar\"
      aria-valuenow=\"0\" aria-valuemin=\"0\" aria-valuemax=\"";
        // line 20
        echo (isset($context["product_total"]) ? $context["product_total"] : null);
        echo "\" style=\"width:0\">
      </div>
    </div>
      <div class=\"alert alert-success hide\" id=\"progress-bar-success\">
      </div>
    ";
        // line 25
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 26
            echo "    <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 30
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 32
        echo (isset($context["text_list"]) ? $context["text_list"] : null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <div class=\"well\">
          <div class=\"row\">
            <div class=\"col-sm-3\">
              <div class=\"form-group\">
                <label class=\"control-label\" for=\"input-name\">";
        // line 39
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo "</label>
                <input type=\"text\" name=\"filter_name\" value=\"\" placeholder=\"";
        // line 40
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo "\" id=\"input-name\" class=\"form-control\" />
              </div>
              <div class=\"form-group\">
                <label class=\"control-label\" for=\"input-model\">";
        // line 43
        echo (isset($context["entry_model"]) ? $context["entry_model"] : null);
        echo "</label>
                <input type=\"text\" name=\"filter_model\" value=\"\" placeholder=\"";
        // line 44
        echo (isset($context["entry_model"]) ? $context["entry_model"] : null);
        echo "\" id=\"input-model\" class=\"form-control\" />
              </div>
            </div>
            <div class=\"col-sm-3\">
              <div class=\"form-group\">
                <label class=\"control-label\" for=\"input-price\">";
        // line 49
        echo (isset($context["entry_price"]) ? $context["entry_price"] : null);
        echo "</label>
                <input type=\"text\" name=\"filter_price\" value=\"\" placeholder=\"";
        // line 50
        echo (isset($context["entry_price"]) ? $context["entry_price"] : null);
        echo "\" id=\"input-price\" class=\"form-control\" />
              </div>
              <div class=\"form-group\">
                <label class=\"control-label\" for=\"input-quantity\">";
        // line 53
        echo (isset($context["entry_quantity"]) ? $context["entry_quantity"] : null);
        echo "</label>
                <input type=\"text\" name=\"filter_quantity\" value=\"\" placeholder=\"";
        // line 54
        echo (isset($context["entry_quantity"]) ? $context["entry_quantity"] : null);
        echo "\" id=\"input-quantity\" class=\"form-control\" />
              </div>
            </div>
            <div class=\"col-sm-3\">
              <div class=\"form-group\">
                <label class=\"control-label\" for=\"input-status\">";
        // line 59
        echo (isset($context["entry_status"]) ? $context["entry_status"] : null);
        echo "</label>
                <select name=\"filter_status\" id=\"input-status\" class=\"form-control\">
                  <option value=\"\"></option>
                  <option value=\"1\">";
        // line 62
        echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
        echo "</option>
                  <option value=\"0\">";
        // line 63
        echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
        echo "</option>
                </select>
              </div>
              <div class=\"form-group\">
                <label class=\"control-label\" for=\"input-barcode\">";
        // line 67
        echo (isset($context["column_barcode"]) ? $context["column_barcode"] : null);
        echo "</label>
                <input type=\"text\" name=\"filter_barcode\" value=\"\" placeholder=\"";
        // line 68
        echo (isset($context["column_barcode"]) ? $context["column_barcode"] : null);
        echo "\" id=\"input-barcode\" class=\"form-control\" />
              </div>
            </div>
            <div class=\"col-sm-3\">
              <div class=\"form-group\">
                <label class=\"control-label\" for=\"input-assign\">";
        // line 73
        echo (isset($context["entry_assign"]) ? $context["entry_assign"] : null);
        echo "</label>
                <input type=\"text\" name=\"filter_assign\" value=\"\" placeholder=\"";
        // line 74
        echo (isset($context["entry_assign"]) ? $context["entry_assign"] : null);
        echo "\" id=\"input-assign\" class=\"form-control\" />
              </div>
              <button type=\"button\" id=\"button-clear\" class=\"btn btn-default\"><i class=\"fa fa-search\"></i> ";
        // line 76
        echo (isset($context["button_clear"]) ? $context["button_clear"] : null);
        echo "</button>
              <button type=\"button\" id=\"button-filter\" class=\"btn btn-primary pull-right\"><i class=\"fa fa-search\"></i> ";
        // line 77
        echo (isset($context["button_filter"]) ? $context["button_filter"] : null);
        echo "</button>
            </div>
          </div>
        </div>
        <form action=\"";
        // line 81
        echo (isset($context["mass_print"]) ? $context["mass_print"] : null);
        echo "\" method=\"post\" id=\"mass-print-form\" enctype=\"multipart/form-data\">
          <div class=\"table-responsive\">
            <table class=\"table table-bordered table-hover\">
              <thead>
                <tr>
                  <td style=\"width: 1px;\" class=\"text-center\"><input type=\"checkbox\" name=\"allcheckbox\" id=\"allcheckbox\" onclick=\"\$('input[name*=\\'selected\\']').prop('checked', this.checked);\" value=\"1\" /></td>
                  <td class=\"text-center\">";
        // line 87
        echo (isset($context["column_image"]) ? $context["column_image"] : null);
        echo "</td>
                  <td class=\"text-left\">
                    <a class=\"sort asc\" sort=\"pd.name\" order=\"asc\">";
        // line 89
        echo (isset($context["column_name"]) ? $context["column_name"] : null);
        echo "</a>
                  </td>
                  <td class=\"text-left\">
                    <a class=\"sort\" sort=\"p.model\" order=\"\">";
        // line 92
        echo (isset($context["column_model"]) ? $context["column_model"] : null);
        echo "</a>
                  </td>
                  <td class=\"text-center\" style=\"width: 145px;\">
                    <a class=\"sort\" sort=\"wb.barcode\" order=\"\">";
        // line 95
        echo (isset($context["column_barcode"]) ? $context["column_barcode"] : null);
        echo "</a>
                  </td>
                  <td class=\"text-right\">
                    <a class=\"sort\" sort=\"p.price\" order=\"\">";
        // line 98
        echo (isset($context["column_price"]) ? $context["column_price"] : null);
        echo "</a>
                  </td>
                  <td class=\"text-right\">
                    <a class=\"sort\" sort=\"p.quantity\" order=\"\">";
        // line 101
        echo (isset($context["column_quantity"]) ? $context["column_quantity"] : null);
        echo "</a>
                  </td>
                  <td class=\"text-left\">
                    <a class=\"sort\" sort=\"p.status\" order=\"\">";
        // line 104
        echo (isset($context["column_status"]) ? $context["column_status"] : null);
        echo "</a>
                  </td>
                  <!-- <td class=\"text-left\">
                    <a class=\"sort\" sort=\"wp.status\" order=\"\">";
        // line 107
        echo (isset($context["column_pos_status"]) ? $context["column_pos_status"] : null);
        echo "</a>
                  </td> -->
                  <td class=\"text-right\" style=\"width: 100px;\">
                    <a class=\"sort\" sort=\"wp.quantity\" order=\"\">";
        // line 110
        echo (isset($context["column_assign"]) ? $context["column_assign"] : null);
        echo "</a>
                  </td>
                  <td class=\"text-center\">";
        // line 112
        echo (isset($context["column_action"]) ? $context["column_action"] : null);
        echo "</td>
                </tr>
              </thead>
              <tbody id=\"productBody\">
                <tr></tr>
              </tbody>
              <tfoot>
                <tr><td colspan=\"11\" class=\"alert-info text-center\" id=\"productFoot\"></td></tr>
              </tfoot>
            </table>
          </div>
          <input type=\"hidden\" name=\"print_quantity\" id=\"printQuant\">
        </form>
      </div>
    </div>
  </div>

  <div id=\"printBarcode\" class=\"modal fade\" role=\"dialog\" tabindex=\"-1\">
    <div class=\"modal-dialog modal-sm\">
      <div class=\"modal-content\">
        <div class=\"modal-header\">
          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
          <h4 class=\"modal-title\">";
        // line 134
        echo (isset($context["text_enter_barcode"]) ? $context["text_enter_barcode"] : null);
        echo "</h4>
        </div>
        <div class=\"modal-body\" style=\"padding-bottom: 45px;\">
          <form class=\"form-horizontal\" action=\"";
        // line 137
        echo (isset($context["print_action"]) ? $context["print_action"] : null);
        echo "\" method=\"post\">
            <div class=\"form-group\">
              <label class=\"control-label col-sm-3\" for=\"barcode-quantity\">";
        // line 139
        echo (isset($context["entry_quantity"]) ? $context["entry_quantity"] : null);
        echo "</label>
              <div class=\"col-sm-9\">
                <input type=\"number\" min=\"1\" name=\"quantity\" id=\"barcode-quantity\" class=\"form-control\">
                <input type=\"hidden\" name=\"product_id\" id=\"productId\">
              </div>
            </div>
            <div class=\"col-sm-12 text-center\">
              <input type=\"submit\" class=\"btn btn-primary\" value=\"";
        // line 146
        echo (isset($context["button_print"]) ? $context["button_print"] : null);
        echo "\">
            </div>
          </form>
        </div>
        <div class=\"modal-footer\">
          <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 151
        echo (isset($context["button_close"]) ? $context["button_close"] : null);
        echo "</button>
        </div>
      </div>
    </div>
  </div>
  <button class=\"btn btn-info hide\" id=\"showModal\" data-toggle=\"modal\" data-target=\"#printBarcode\"><i class=\"fa fa-eye\"></i></button>
  <script type=\"text/javascript\" src=\"../wkpos/js/toast.js\"></script>
  <script type=\"text/javascript\" src=\"../wkpos/js/hash.js\"></script>
  <script type=\"text/javascript\"><!--
  var total_products = ";
        // line 160
        echo (isset($context["product_total"]) ? $context["product_total"] : null);
        echo ", number_inslot = ";
        echo (isset($context["number_inslot"]) ? $context["number_inslot"] : null);
        echo ";
  \$('input[name=\\'filter_name\\']').autocomplete({
    'source': function(request, response) {
      \$.ajax({
        url: 'index.php?route=catalog/product/autocomplete&user_token=";
        // line 164
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_name=' +  encodeURIComponent(request),
        dataType: 'json',
        success: function(json) {
          response(\$.map(json, function(item) {
            return {
              label: item['name'],
              value: item['product_id']
            }
          }));
        },
        error: function (error) {
          alert(error.responseText);
          // location = 'index.php?route=wkpos/products&user_token=";
        // line 176
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "';
        }
      });
    },
    'select': function(item) {
      \$('input[name=\\'filter_name\\']').val(item['label']);
    }
  });

  \$('input[name=\\'filter_model\\']').autocomplete({
    'source': function(request, response) {
      \$.ajax({
        url: 'index.php?route=catalog/product/autocomplete&user_token=";
        // line 188
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_model=' +  encodeURIComponent(request),
        dataType: 'json',
        success: function(json) {
          response(\$.map(json, function(item) {
            return {
              label: item['model'],
              value: item['product_id']
            }
          }));
        },
        error: function (error) {
          alert(error.responseText);
          // location = 'index.php?route=wkpos/products&user_token=";
        // line 200
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "';
        }
      });
    },
    'select': function(item) {
      \$('input[name=\\'filter_model\\']').val(item['label']);
    }
  });
//--></script>
  <script type=\"text/javascript\"><!--
  var order = 'ASC';
  var sort = 'pd.name';
  var filter_name = '', filter_model = '', filter_barcode = '', filter_price = '', filter_quantity = '', filter_assign = '', filter_status = '';
  var product_listed = 0;
  var product_prev = 0;
  var start = 0;
  var all = false;
  var in_process = false;

  function loadProducts() {
    var product_data = {
      start: start,
      order: order,
      sort: sort,
      filter_name: filter_name,
      filter_model: filter_model,
      filter_barcode: filter_barcode,
      filter_price: filter_price,
      filter_quantity: filter_quantity,
      filter_assign: filter_assign,
      filter_status: filter_status
    };

    \$.ajax({
      url: 'index.php?route=wkpos/products/loadProducts&user_token=";
        // line 234
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "',
      data: product_data,
      type: 'post',
      dataType: 'json',
      beforeSend: function () {
        \$('#productFoot').append(' <i class=\"fa fa-spin fa-spinner\"></i>');
        in_process = true;
      },
      success: function(json) {
        in_process = false;
        if (json['success']) {
          var product_html = '';
          var products = json['products'];
          for (var i = 0; i < products.length; i++) {
            product_listed++;
            product_html += '<tr product-id=\"' + products[i]['product_id'] + '\">';

            if(\$('#allcheckbox').is(':checked')) {
              product_html += '  <td class=\"text-center\"><input type=\"checkbox\" checked name=\"selected[]\" value=\"' + products[i]['product_id'] + '\" /></td>';
            } else {
              product_html += '  <td class=\"text-center\"><input type=\"checkbox\" name=\"selected[]\" value=\"' + products[i]['product_id'] + '\" /></td>';
            }
            product_html += '  <td class=\"text-center\">';
            if (products[i]['image']) {
              product_html += '    <img src=\"' + products[i]['image'] + '\" alt=\"' + products[i]['name'] + '\" class=\"img-thumbnail\" />';
            } else {
              product_html += '    <span class=\"img-thumbnail list\"><i class=\"fa fa-camera fa-2x\"></i></span>';
            }
            product_html += '    </td>';
            product_html += '  <td class=\"text-left\">' + products[i]['name'] + '</td>';
            product_html += '  <td class=\"text-left\">' + products[i]['model'] + '</td>';
            product_html += '  <td class=\"text-center\">';
            if (products[i]['barcode']) {
              product_html += '  <img src=\"' + products[i]['barcode'] + '\">';
            } else {
              product_html += '  <span class=\"img-thumbnail list\"><i class=\"fa fa-barcode fa-2x\"></i></span>';
            }
            product_html += '  </td>';
            product_html += '  <td class=\"text-right\">';
            if (products[i]['special']) {
              product_html += '    <span style=\"text-decoration: line-through;\">' + products[i]['price'] + '</span><br/>';
              product_html += '    <div class=\"text-danger\">' + products[i]['special'] + '</div>';
            } else {
              product_html += '    ' + products[i]['price'] + '';
            }
            product_html += '  <td class=\"text-right\">';
            if (products[i]['quantity'] <= 0) {
              product_html += '    <span class=\"label label-warning\">' + products[i]['quantity'] + '</span>';
            } else if (products[i]['quantity'] <= 5) {
              product_html += '    <span class=\"label label-danger\">' + products[i]['quantity'] + '</span>';
            } else {
              product_html += '    <span class=\"label label-success\">' + products[i]['quantity'] + '</span>';
            }
            product_html += '  <td class=\"text-left\">' + products[i]['status'] + '</td>';
            product_html += '  <td class=\"text-center\"><span class=\"label label-default\">' + products[i]['pos_quantity'] + '</span></td>';

            product_html += '  <td class=\"text-right\">';

            if (products[i]['barcode']) {
              product_html += '  <button type=\"button\" class=\"btn btn-success showModal\" title=\"";
        // line 293
        echo (isset($context["title_print"]) ? $context["title_print"] : null);
        echo "\"><i class=\"fa fa-print\"></i></button>';
              product_html += '  <button type=\"button\" class=\"btn btn-warning active generateBarcode\" title=\"";
        // line 294
        echo (isset($context["title_regenerate"]) ? $context["title_regenerate"] : null);
        echo "\"><i class=\"fa fa-barcode\"></i></button>';
            } else {
              product_html += '  <button type=\"button\" class=\"btn btn-warning generateBarcode\" title=\"";
        // line 296
        echo (isset($context["title_generate"]) ? $context["title_generate"] : null);
        echo "\"><i class=\"fa fa-barcode\"></i></button>';
            }

            product_html += '  </td>';

            product_html += '</tr>';
            if (product_listed == json['product_total']) {
              all = true;
            }
          }
          \$('#productBody').append(product_html);
          \$('#productFoot').text('Showing ' + product_listed + ' of ' + json['product_total']);
        } else {
          \$('#productBody').html('<tr></tr>');
          \$('#productFoot').text('";
        // line 310
        echo (isset($context["text_no_products"]) ? $context["text_no_products"] : null);
        echo "');
        }
      },
      error: function (error) {
        alert(error.responseText);
        in_process = false;
        // location = 'index.php?route=wkpos/products&user_token=";
        // line 316
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "';
      }
    });
  }

  \$(window).on('scroll', function () {
    var diff = \$(document).height() - \$(window).height();
    var foot = diff - \$(window).scrollTop();

    if ((foot < 200) && !in_process) {
      start = product_listed;

      if (product_listed && (product_listed != product_prev) && !all) {
        loadProducts();
        product_prev = product_listed;
      }
    };
  });

  \$('.sort').on('click', function () {
    var thisthis = \$(this);
    var sort_it = thisthis.attr('sort');
    \$('.desc').removeClass('desc');
    \$('.asc').removeClass('asc');

    if (sort_it == sort) {
      if (thisthis.attr('order') == 'asc') {
        order = 'DESC';
      } else {
        order = 'ASC';
      }
    } else {
      sort = sort_it;
    }
    if (order == 'ASC') {
      thisthis.addClass('asc');
      thisthis.attr('order', 'asc');
    } else {
      thisthis.addClass('desc');
      thisthis.attr('order', 'desc');
    }
    product_listed = 0;
    product_prev = 0;
    start = 0;
    all = false;
    \$('#productBody').html('<tr></tr>');
    \$('#productFoot').html('');
    loadProducts();
  });

  \$('body').on('click', '.generateBarcode', function () {
    var thisthis = \$(this);
    var product_id = thisthis.parent().parent().attr('product-id');

    \$.ajax({
      url: 'index.php?route=wkpos/products/generateBarcode&user_token=";
        // line 371
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "',
      data: {product_id: product_id},
      type: 'post',
      dataType: 'json',
      beforeSend: function () {
        thisthis.attr('disabled', 'disabled').html('<i class=\"fa fa-spin fa-spinner\"></i>');
      },
      success: function(json) {
        thisthis.removeAttr('disabled').html('<i class=\"fa fa-barcode\"></i>');
        if (json['success']) {
          thisthis.parent().parent().children('td:nth-child(5)').html('<img src=\"' + json['image'] + '\">');
          thisthis.parent().html('<button type=\"button\" class=\"btn btn-success showModal\" title=\"";
        // line 382
        echo (isset($context["title_print"]) ? $context["title_print"] : null);
        echo "\"><i class=\"fa fa-print\"></i></button><button type=\"button\" class=\"btn btn-warning active generateBarcode\" title=\"";
        echo (isset($context["title_regenerate"]) ? $context["title_regenerate"] : null);
        echo "\"><i class=\"fa fa-barcode\"></i></button>');
          \$.toaster({
              priority: 'success',
              title: 'Notice',
              message: json['success'],
              timeout: 3000
          });
        }
      },
      error: function (error) {
        alert(error.responseText);
        // location = 'index.php?route=wkpos/products&user_token=";
        // line 393
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "';
      }
    });
  });

  \$('body').on('click', '.showModal', function () {
    \$('.modal-body .text-center').html('<input type=\"submit\" class=\"btn btn-primary\" value=\"";
        // line 399
        echo (isset($context["button_print"]) ? $context["button_print"] : null);
        echo "\" />');
    \$('.modal-title').text('";
        // line 400
        echo (isset($context["text_enter_barcode"]) ? $context["text_enter_barcode"] : null);
        echo "');
    \$('#showModal').trigger('click');
    var thisthis = \$(this);
    var product_id = thisthis.parent().parent().attr('product-id');
    \$('#productId').val(product_id);
  });

  \$('body').on('click', '.printBarcode', function () {
    var thisthis = \$(this);
    var product_id = thisthis.parent().parent().attr('product-id');

    \$.ajax({
      url: 'index.php?route=wkpos/products/printBarcode&user_token=";
        // line 412
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "',
      data: {product_id: product_id},
      type: 'post',
      dataType: 'json',
      beforeSend: function () {
        thisthis.attr('disabled', 'disabled').html('<i class=\"fa fa-spin fa-spinner\"></i>');
      },
      success: function(json) {
        thisthis.removeAttr('disabled').html('<i class=\"fa fa-barcode\"></i>');
        if (json['success']) {
          thisthis.parent().parent().children('td:nth-child(4)').html('<img src=\"' + json['image'] + '\">');
          thisthis.parent().html('<button class=\"btn btn-success printBarcode\" title=\"";
        // line 423
        echo (isset($context["title_print"]) ? $context["title_print"] : null);
        echo "\"><i class=\"fa fa-print\"></i></button>');
          \$.toaster({
              priority: 'success',
              title: 'Notice',
              message: json['success'],
              timeout: 3000
          });
        }
      },
      error: function (error) {
        alert(error.responseText);
        // location = 'index.php?route=wkpos/products&user_token=";
        // line 434
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "';
      }
    });
  });

  \$('body').on('click','#btn-mass-print-form',function(){
    \$('.text-danger').remove();
    var bar_quantity = \$('#barcode-quantity').val();
    if(bar_quantity <= 1000){
      \$('#mass-print-form').submit();
    } else {
      \$('#barcode-quantity').after('<div class=\"text-danger\">";
        // line 445
        echo (isset($context["test_quantity_max"]) ? $context["test_quantity_max"] : null);
        echo "</div>');
    }
  });

  function massPrint() {
    \$('.modal-body .text-center').html('<button type=\"button\" id=\"btn-mass-print-form\" class=\"btn btn-primary\">";
        // line 450
        echo (isset($context["button_print_all"]) ? $context["button_print_all"] : null);
        echo "</button>');
    \$('.modal-title').text('";
        // line 451
        echo (isset($context["text_multi_print"]) ? $context["text_multi_print"] : null);
        echo "');
    \$('#showModal').trigger('click');
  }

  \$('#barcode-quantity').on('change', function(){
    \$('#printQuant').val(\$('#barcode-quantity').val());
  });
//--></script>
  <script type=\"text/javascript\"><!--
  \$('#button-filter').on('click', function() {
    filter_name = \$('input[name=\\'filter_name\\']').val();
    if (filter_name) {
      setHash('filter_name', filter_name);
    } else {
      removeHash('filter_name');
    }

    filter_model = \$('input[name=\\'filter_model\\']').val();
    if (filter_model) {
      setHash('filter_model', filter_model);
    } else {
      removeHash('filter_model');
    }

    filter_barcode = \$('input[name=\\'filter_barcode\\']').val();
    if (filter_barcode) {
      setHash('filter_barcode', filter_barcode);
    } else {
      removeHash('filter_barcode');
    }

    filter_price = \$('input[name=\\'filter_price\\']').val();
    if (filter_price) {
      setHash('filter_price', filter_price);
    } else {
      removeHash('filter_price');
    }

    filter_quantity = \$('input[name=\\'filter_quantity\\']').val();
    if (filter_quantity) {
      setHash('filter_quantity', filter_quantity);
    } else {
      removeHash('filter_quantity');
    }

    filter_assign = \$('input[name=\\'filter_assign\\']').val();
    if (filter_assign) {
      setHash('filter_assign', filter_assign);
    } else {
      removeHash('filter_assign');
    }

    filter_status = \$('select[name=\\'filter_status\\']').val();
    if (filter_status) {
      setHash('filter_status', filter_status);
    } else {
      removeHash('filter_status');
    }

    product_listed = 0;
    product_prev = 0;
    start = 0;
    all = false;
    \$('#productBody').html('<tr></tr>');
    \$('#productFoot').html('');
    loadProducts();
  });
//--></script>
<script type=\"text/javascript\">
  \$(document).ready(function () {
    var checkFilterName = getHash('filter_name');
    if (checkFilterName) {
      filter_name = checkFilterName;
      \$('input[name=\\'filter_name\\']').val(filter_name);
    }
    var checkFilterModel = getHash('filter_model');
    if (checkFilterModel) {
      filter_model = checkFilterModel;
      \$('input[name=\\'filter_model\\']').val(filter_model);
    }
    var checkFilterBarcode = getHash('filter_barcode');
    if (checkFilterBarcode) {
      filter_barcode = checkFilterBarcode;
      \$('input[name=\\'filter_barcode\\']').val(filter_barcode);
    }
    var checkFilterPrice = getHash('filter_price');
    if (checkFilterPrice) {
      filter_price = checkFilterPrice;
      \$('input[name=\\'filter_price\\']').val(filter_price);
    }
    var checkFilterStatus = getHash('filter_status');
    if (checkFilterStatus) {
      filter_status = checkFilterStatus;
      \$('select[name=\\'filter_status\\'] option[value=\"' + filter_status + '\"]').prop('selected', true);
    }
    var checkFilterQuantity = getHash('filter_quantity');
    if (checkFilterQuantity) {
      filter_quantity = checkFilterQuantity;
      \$('input[name=\\'filter_quantity\\']').val(filter_quantity);
    }
    var checkFilterAssign = getHash('filter_assign');
    if (checkFilterAssign) {
      filter_assign = checkFilterAssign;
      \$('input[name=\\'filter_assign\\']').val(filter_assign);
    }
    loadProducts();
  });
  \$(document).on('keypress', '#barcode-quantity', function(event){
    if (event.which <= 46 || event.which >= 58) {
      return false;
    }
  });
  var count_barcode = 0;
  function massGenerate() {
    \$('.progress').css('display', 'block');
    \$('#generate-barcodes').addClass('disabled');
    \$.ajax({
      url: 'index.php?route=wkpos/products/massGenerate&user_token=";
        // line 568
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "',
      type: 'post',
      data: {count_barcode: count_barcode},
      dataType: 'json',
      success: function(json) {
        if (typeof json['count'] != 'undefined') {
          \$('#progress-bar-success').removeClass('hide').html('<i class=\"fa fa-check-circle\"></i>&nbsp;' + json['success'].replace('%s', count_barcode + json['count']) + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>');
          \$('.progress-bar-success').css('width', ((count_barcode+number_inslot)/total_products)*100 + '%');
          \$('.progress-bar-success').attr('aria-valuenow', count_barcode+number_inslot);
        }
      },
    }).done(function() {
      if (count_barcode < total_products) {
        count_barcode += number_inslot;
          massGenerate();
      } else {
        \$('.progress-bar-success').removeClass('active');
        \$('#generate-barcodes').removeClass('disabled');
        location.reload();
      }
    });
  }
  \$(document).ready(function(){
      \$('#generate-barcodes').tooltip({
        title:\"";
        // line 592
        echo (isset($context["help_mass_generate"]) ? $context["help_mass_generate"] : null);
        echo "\",
        placement : 'left',
        animation: false,
        trigger : 'hover'
      });
  });
  \$(document).on('click', '#button-clear', function() {
    \$('.well input').val('');
    \$('#input-status option:first').prop('selected', true);
    \$('#button-filter').click();
  });
</script></div>
<style type=\"text/css\">
  .well .form-group {
    padding-top: 0px;
    padding-bottom: 0;
    margin-bottom: 0;
  }
  #button-clear, #button-filter {
    margin-top: 24px;
  }
  .radius-0 {
    border-radius: 0;
  }
  thead td a {
    cursor: pointer;
  }
</style>
";
        // line 620
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "wkpos/products.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  855 => 620,  824 => 592,  797 => 568,  677 => 451,  673 => 450,  665 => 445,  651 => 434,  637 => 423,  623 => 412,  608 => 400,  604 => 399,  595 => 393,  579 => 382,  565 => 371,  507 => 316,  498 => 310,  481 => 296,  476 => 294,  472 => 293,  410 => 234,  373 => 200,  358 => 188,  343 => 176,  328 => 164,  319 => 160,  307 => 151,  299 => 146,  289 => 139,  284 => 137,  278 => 134,  253 => 112,  248 => 110,  242 => 107,  236 => 104,  230 => 101,  224 => 98,  218 => 95,  212 => 92,  206 => 89,  201 => 87,  192 => 81,  185 => 77,  181 => 76,  176 => 74,  172 => 73,  164 => 68,  160 => 67,  153 => 63,  149 => 62,  143 => 59,  135 => 54,  131 => 53,  125 => 50,  121 => 49,  113 => 44,  109 => 43,  103 => 40,  99 => 39,  89 => 32,  85 => 30,  77 => 26,  75 => 25,  67 => 20,  58 => 14,  51 => 12,  47 => 10,  36 => 8,  32 => 7,  27 => 5,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*       <div class="btn-group pull-right">*/
/*         <button class="btn btn-success radius-0" data-original-title="{{ help_mass_print }}" data-toggle="tooltip" onclick="massPrint();"><i class="fa fa-print"></i> {{ text_mass_print }}</button>*/
/*       </div>*/
/*       <button type="button" class="btn btn-info pull-right radius-0" id="generate-barcodes" onclick="massGenerate();"><i class="fa fa-file" aria-hidden="true"></i>{{ text_mass_generate }}</button>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">*/
/*     <div class="progress" style="display: none;">*/
/*       <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"*/
/*       aria-valuenow="0" aria-valuemin="0" aria-valuemax="{{ product_total }}" style="width:0">*/
/*       </div>*/
/*     </div>*/
/*       <div class="alert alert-success hide" id="progress-bar-success">*/
/*       </div>*/
/*     {% if error_warning %}*/
/*     <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*       <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*     {% endif %}*/
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-list"></i> {{ text_list }}</h3>*/
/*       </div>*/
/*       <div class="panel-body">*/
/*         <div class="well">*/
/*           <div class="row">*/
/*             <div class="col-sm-3">*/
/*               <div class="form-group">*/
/*                 <label class="control-label" for="input-name">{{ entry_name }}</label>*/
/*                 <input type="text" name="filter_name" value="" placeholder="{{ entry_name }}" id="input-name" class="form-control" />*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="control-label" for="input-model">{{ entry_model }}</label>*/
/*                 <input type="text" name="filter_model" value="" placeholder="{{ entry_model }}" id="input-model" class="form-control" />*/
/*               </div>*/
/*             </div>*/
/*             <div class="col-sm-3">*/
/*               <div class="form-group">*/
/*                 <label class="control-label" for="input-price">{{ entry_price }}</label>*/
/*                 <input type="text" name="filter_price" value="" placeholder="{{ entry_price }}" id="input-price" class="form-control" />*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="control-label" for="input-quantity">{{ entry_quantity }}</label>*/
/*                 <input type="text" name="filter_quantity" value="" placeholder="{{ entry_quantity }}" id="input-quantity" class="form-control" />*/
/*               </div>*/
/*             </div>*/
/*             <div class="col-sm-3">*/
/*               <div class="form-group">*/
/*                 <label class="control-label" for="input-status">{{ entry_status }}</label>*/
/*                 <select name="filter_status" id="input-status" class="form-control">*/
/*                   <option value=""></option>*/
/*                   <option value="1">{{ text_enabled }}</option>*/
/*                   <option value="0">{{ text_disabled }}</option>*/
/*                 </select>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="control-label" for="input-barcode">{{ column_barcode }}</label>*/
/*                 <input type="text" name="filter_barcode" value="" placeholder="{{ column_barcode }}" id="input-barcode" class="form-control" />*/
/*               </div>*/
/*             </div>*/
/*             <div class="col-sm-3">*/
/*               <div class="form-group">*/
/*                 <label class="control-label" for="input-assign">{{ entry_assign }}</label>*/
/*                 <input type="text" name="filter_assign" value="" placeholder="{{ entry_assign }}" id="input-assign" class="form-control" />*/
/*               </div>*/
/*               <button type="button" id="button-clear" class="btn btn-default"><i class="fa fa-search"></i> {{ button_clear }}</button>*/
/*               <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> {{ button_filter }}</button>*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/*         <form action="{{ mass_print }}" method="post" id="mass-print-form" enctype="multipart/form-data">*/
/*           <div class="table-responsive">*/
/*             <table class="table table-bordered table-hover">*/
/*               <thead>*/
/*                 <tr>*/
/*                   <td style="width: 1px;" class="text-center"><input type="checkbox" name="allcheckbox" id="allcheckbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" value="1" /></td>*/
/*                   <td class="text-center">{{ column_image }}</td>*/
/*                   <td class="text-left">*/
/*                     <a class="sort asc" sort="pd.name" order="asc">{{ column_name }}</a>*/
/*                   </td>*/
/*                   <td class="text-left">*/
/*                     <a class="sort" sort="p.model" order="">{{ column_model }}</a>*/
/*                   </td>*/
/*                   <td class="text-center" style="width: 145px;">*/
/*                     <a class="sort" sort="wb.barcode" order="">{{ column_barcode }}</a>*/
/*                   </td>*/
/*                   <td class="text-right">*/
/*                     <a class="sort" sort="p.price" order="">{{ column_price }}</a>*/
/*                   </td>*/
/*                   <td class="text-right">*/
/*                     <a class="sort" sort="p.quantity" order="">{{ column_quantity }}</a>*/
/*                   </td>*/
/*                   <td class="text-left">*/
/*                     <a class="sort" sort="p.status" order="">{{ column_status }}</a>*/
/*                   </td>*/
/*                   <!-- <td class="text-left">*/
/*                     <a class="sort" sort="wp.status" order="">{{ column_pos_status }}</a>*/
/*                   </td> -->*/
/*                   <td class="text-right" style="width: 100px;">*/
/*                     <a class="sort" sort="wp.quantity" order="">{{ column_assign }}</a>*/
/*                   </td>*/
/*                   <td class="text-center">{{ column_action }}</td>*/
/*                 </tr>*/
/*               </thead>*/
/*               <tbody id="productBody">*/
/*                 <tr></tr>*/
/*               </tbody>*/
/*               <tfoot>*/
/*                 <tr><td colspan="11" class="alert-info text-center" id="productFoot"></td></tr>*/
/*               </tfoot>*/
/*             </table>*/
/*           </div>*/
/*           <input type="hidden" name="print_quantity" id="printQuant">*/
/*         </form>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* */
/*   <div id="printBarcode" class="modal fade" role="dialog" tabindex="-1">*/
/*     <div class="modal-dialog modal-sm">*/
/*       <div class="modal-content">*/
/*         <div class="modal-header">*/
/*           <button type="button" class="close" data-dismiss="modal">&times;</button>*/
/*           <h4 class="modal-title">{{ text_enter_barcode }}</h4>*/
/*         </div>*/
/*         <div class="modal-body" style="padding-bottom: 45px;">*/
/*           <form class="form-horizontal" action="{{ print_action }}" method="post">*/
/*             <div class="form-group">*/
/*               <label class="control-label col-sm-3" for="barcode-quantity">{{ entry_quantity }}</label>*/
/*               <div class="col-sm-9">*/
/*                 <input type="number" min="1" name="quantity" id="barcode-quantity" class="form-control">*/
/*                 <input type="hidden" name="product_id" id="productId">*/
/*               </div>*/
/*             </div>*/
/*             <div class="col-sm-12 text-center">*/
/*               <input type="submit" class="btn btn-primary" value="{{ button_print }}">*/
/*             </div>*/
/*           </form>*/
/*         </div>*/
/*         <div class="modal-footer">*/
/*           <button type="button" class="btn btn-default" data-dismiss="modal">{{ button_close }}</button>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*   <button class="btn btn-info hide" id="showModal" data-toggle="modal" data-target="#printBarcode"><i class="fa fa-eye"></i></button>*/
/*   <script type="text/javascript" src="../wkpos/js/toast.js"></script>*/
/*   <script type="text/javascript" src="../wkpos/js/hash.js"></script>*/
/*   <script type="text/javascript"><!--*/
/*   var total_products = {{ product_total }}, number_inslot = {{ number_inslot }};*/
/*   $('input[name=\'filter_name\']').autocomplete({*/
/*     'source': function(request, response) {*/
/*       $.ajax({*/
/*         url: 'index.php?route=catalog/product/autocomplete&user_token={{ user_token }}&filter_name=' +  encodeURIComponent(request),*/
/*         dataType: 'json',*/
/*         success: function(json) {*/
/*           response($.map(json, function(item) {*/
/*             return {*/
/*               label: item['name'],*/
/*               value: item['product_id']*/
/*             }*/
/*           }));*/
/*         },*/
/*         error: function (error) {*/
/*           alert(error.responseText);*/
/*           // location = 'index.php?route=wkpos/products&user_token={{ user_token }}';*/
/*         }*/
/*       });*/
/*     },*/
/*     'select': function(item) {*/
/*       $('input[name=\'filter_name\']').val(item['label']);*/
/*     }*/
/*   });*/
/* */
/*   $('input[name=\'filter_model\']').autocomplete({*/
/*     'source': function(request, response) {*/
/*       $.ajax({*/
/*         url: 'index.php?route=catalog/product/autocomplete&user_token={{ user_token }}&filter_model=' +  encodeURIComponent(request),*/
/*         dataType: 'json',*/
/*         success: function(json) {*/
/*           response($.map(json, function(item) {*/
/*             return {*/
/*               label: item['model'],*/
/*               value: item['product_id']*/
/*             }*/
/*           }));*/
/*         },*/
/*         error: function (error) {*/
/*           alert(error.responseText);*/
/*           // location = 'index.php?route=wkpos/products&user_token={{ user_token }}';*/
/*         }*/
/*       });*/
/*     },*/
/*     'select': function(item) {*/
/*       $('input[name=\'filter_model\']').val(item['label']);*/
/*     }*/
/*   });*/
/* //--></script>*/
/*   <script type="text/javascript"><!--*/
/*   var order = 'ASC';*/
/*   var sort = 'pd.name';*/
/*   var filter_name = '', filter_model = '', filter_barcode = '', filter_price = '', filter_quantity = '', filter_assign = '', filter_status = '';*/
/*   var product_listed = 0;*/
/*   var product_prev = 0;*/
/*   var start = 0;*/
/*   var all = false;*/
/*   var in_process = false;*/
/* */
/*   function loadProducts() {*/
/*     var product_data = {*/
/*       start: start,*/
/*       order: order,*/
/*       sort: sort,*/
/*       filter_name: filter_name,*/
/*       filter_model: filter_model,*/
/*       filter_barcode: filter_barcode,*/
/*       filter_price: filter_price,*/
/*       filter_quantity: filter_quantity,*/
/*       filter_assign: filter_assign,*/
/*       filter_status: filter_status*/
/*     };*/
/* */
/*     $.ajax({*/
/*       url: 'index.php?route=wkpos/products/loadProducts&user_token={{ user_token }}',*/
/*       data: product_data,*/
/*       type: 'post',*/
/*       dataType: 'json',*/
/*       beforeSend: function () {*/
/*         $('#productFoot').append(' <i class="fa fa-spin fa-spinner"></i>');*/
/*         in_process = true;*/
/*       },*/
/*       success: function(json) {*/
/*         in_process = false;*/
/*         if (json['success']) {*/
/*           var product_html = '';*/
/*           var products = json['products'];*/
/*           for (var i = 0; i < products.length; i++) {*/
/*             product_listed++;*/
/*             product_html += '<tr product-id="' + products[i]['product_id'] + '">';*/
/* */
/*             if($('#allcheckbox').is(':checked')) {*/
/*               product_html += '  <td class="text-center"><input type="checkbox" checked name="selected[]" value="' + products[i]['product_id'] + '" /></td>';*/
/*             } else {*/
/*               product_html += '  <td class="text-center"><input type="checkbox" name="selected[]" value="' + products[i]['product_id'] + '" /></td>';*/
/*             }*/
/*             product_html += '  <td class="text-center">';*/
/*             if (products[i]['image']) {*/
/*               product_html += '    <img src="' + products[i]['image'] + '" alt="' + products[i]['name'] + '" class="img-thumbnail" />';*/
/*             } else {*/
/*               product_html += '    <span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span>';*/
/*             }*/
/*             product_html += '    </td>';*/
/*             product_html += '  <td class="text-left">' + products[i]['name'] + '</td>';*/
/*             product_html += '  <td class="text-left">' + products[i]['model'] + '</td>';*/
/*             product_html += '  <td class="text-center">';*/
/*             if (products[i]['barcode']) {*/
/*               product_html += '  <img src="' + products[i]['barcode'] + '">';*/
/*             } else {*/
/*               product_html += '  <span class="img-thumbnail list"><i class="fa fa-barcode fa-2x"></i></span>';*/
/*             }*/
/*             product_html += '  </td>';*/
/*             product_html += '  <td class="text-right">';*/
/*             if (products[i]['special']) {*/
/*               product_html += '    <span style="text-decoration: line-through;">' + products[i]['price'] + '</span><br/>';*/
/*               product_html += '    <div class="text-danger">' + products[i]['special'] + '</div>';*/
/*             } else {*/
/*               product_html += '    ' + products[i]['price'] + '';*/
/*             }*/
/*             product_html += '  <td class="text-right">';*/
/*             if (products[i]['quantity'] <= 0) {*/
/*               product_html += '    <span class="label label-warning">' + products[i]['quantity'] + '</span>';*/
/*             } else if (products[i]['quantity'] <= 5) {*/
/*               product_html += '    <span class="label label-danger">' + products[i]['quantity'] + '</span>';*/
/*             } else {*/
/*               product_html += '    <span class="label label-success">' + products[i]['quantity'] + '</span>';*/
/*             }*/
/*             product_html += '  <td class="text-left">' + products[i]['status'] + '</td>';*/
/*             product_html += '  <td class="text-center"><span class="label label-default">' + products[i]['pos_quantity'] + '</span></td>';*/
/* */
/*             product_html += '  <td class="text-right">';*/
/* */
/*             if (products[i]['barcode']) {*/
/*               product_html += '  <button type="button" class="btn btn-success showModal" title="{{ title_print }}"><i class="fa fa-print"></i></button>';*/
/*               product_html += '  <button type="button" class="btn btn-warning active generateBarcode" title="{{ title_regenerate }}"><i class="fa fa-barcode"></i></button>';*/
/*             } else {*/
/*               product_html += '  <button type="button" class="btn btn-warning generateBarcode" title="{{ title_generate }}"><i class="fa fa-barcode"></i></button>';*/
/*             }*/
/* */
/*             product_html += '  </td>';*/
/* */
/*             product_html += '</tr>';*/
/*             if (product_listed == json['product_total']) {*/
/*               all = true;*/
/*             }*/
/*           }*/
/*           $('#productBody').append(product_html);*/
/*           $('#productFoot').text('Showing ' + product_listed + ' of ' + json['product_total']);*/
/*         } else {*/
/*           $('#productBody').html('<tr></tr>');*/
/*           $('#productFoot').text('{{ text_no_products }}');*/
/*         }*/
/*       },*/
/*       error: function (error) {*/
/*         alert(error.responseText);*/
/*         in_process = false;*/
/*         // location = 'index.php?route=wkpos/products&user_token={{ user_token }}';*/
/*       }*/
/*     });*/
/*   }*/
/* */
/*   $(window).on('scroll', function () {*/
/*     var diff = $(document).height() - $(window).height();*/
/*     var foot = diff - $(window).scrollTop();*/
/* */
/*     if ((foot < 200) && !in_process) {*/
/*       start = product_listed;*/
/* */
/*       if (product_listed && (product_listed != product_prev) && !all) {*/
/*         loadProducts();*/
/*         product_prev = product_listed;*/
/*       }*/
/*     };*/
/*   });*/
/* */
/*   $('.sort').on('click', function () {*/
/*     var thisthis = $(this);*/
/*     var sort_it = thisthis.attr('sort');*/
/*     $('.desc').removeClass('desc');*/
/*     $('.asc').removeClass('asc');*/
/* */
/*     if (sort_it == sort) {*/
/*       if (thisthis.attr('order') == 'asc') {*/
/*         order = 'DESC';*/
/*       } else {*/
/*         order = 'ASC';*/
/*       }*/
/*     } else {*/
/*       sort = sort_it;*/
/*     }*/
/*     if (order == 'ASC') {*/
/*       thisthis.addClass('asc');*/
/*       thisthis.attr('order', 'asc');*/
/*     } else {*/
/*       thisthis.addClass('desc');*/
/*       thisthis.attr('order', 'desc');*/
/*     }*/
/*     product_listed = 0;*/
/*     product_prev = 0;*/
/*     start = 0;*/
/*     all = false;*/
/*     $('#productBody').html('<tr></tr>');*/
/*     $('#productFoot').html('');*/
/*     loadProducts();*/
/*   });*/
/* */
/*   $('body').on('click', '.generateBarcode', function () {*/
/*     var thisthis = $(this);*/
/*     var product_id = thisthis.parent().parent().attr('product-id');*/
/* */
/*     $.ajax({*/
/*       url: 'index.php?route=wkpos/products/generateBarcode&user_token={{ user_token }}',*/
/*       data: {product_id: product_id},*/
/*       type: 'post',*/
/*       dataType: 'json',*/
/*       beforeSend: function () {*/
/*         thisthis.attr('disabled', 'disabled').html('<i class="fa fa-spin fa-spinner"></i>');*/
/*       },*/
/*       success: function(json) {*/
/*         thisthis.removeAttr('disabled').html('<i class="fa fa-barcode"></i>');*/
/*         if (json['success']) {*/
/*           thisthis.parent().parent().children('td:nth-child(5)').html('<img src="' + json['image'] + '">');*/
/*           thisthis.parent().html('<button type="button" class="btn btn-success showModal" title="{{ title_print }}"><i class="fa fa-print"></i></button><button type="button" class="btn btn-warning active generateBarcode" title="{{ title_regenerate }}"><i class="fa fa-barcode"></i></button>');*/
/*           $.toaster({*/
/*               priority: 'success',*/
/*               title: 'Notice',*/
/*               message: json['success'],*/
/*               timeout: 3000*/
/*           });*/
/*         }*/
/*       },*/
/*       error: function (error) {*/
/*         alert(error.responseText);*/
/*         // location = 'index.php?route=wkpos/products&user_token={{ user_token }}';*/
/*       }*/
/*     });*/
/*   });*/
/* */
/*   $('body').on('click', '.showModal', function () {*/
/*     $('.modal-body .text-center').html('<input type="submit" class="btn btn-primary" value="{{ button_print }}" />');*/
/*     $('.modal-title').text('{{ text_enter_barcode }}');*/
/*     $('#showModal').trigger('click');*/
/*     var thisthis = $(this);*/
/*     var product_id = thisthis.parent().parent().attr('product-id');*/
/*     $('#productId').val(product_id);*/
/*   });*/
/* */
/*   $('body').on('click', '.printBarcode', function () {*/
/*     var thisthis = $(this);*/
/*     var product_id = thisthis.parent().parent().attr('product-id');*/
/* */
/*     $.ajax({*/
/*       url: 'index.php?route=wkpos/products/printBarcode&user_token={{ user_token }}',*/
/*       data: {product_id: product_id},*/
/*       type: 'post',*/
/*       dataType: 'json',*/
/*       beforeSend: function () {*/
/*         thisthis.attr('disabled', 'disabled').html('<i class="fa fa-spin fa-spinner"></i>');*/
/*       },*/
/*       success: function(json) {*/
/*         thisthis.removeAttr('disabled').html('<i class="fa fa-barcode"></i>');*/
/*         if (json['success']) {*/
/*           thisthis.parent().parent().children('td:nth-child(4)').html('<img src="' + json['image'] + '">');*/
/*           thisthis.parent().html('<button class="btn btn-success printBarcode" title="{{ title_print }}"><i class="fa fa-print"></i></button>');*/
/*           $.toaster({*/
/*               priority: 'success',*/
/*               title: 'Notice',*/
/*               message: json['success'],*/
/*               timeout: 3000*/
/*           });*/
/*         }*/
/*       },*/
/*       error: function (error) {*/
/*         alert(error.responseText);*/
/*         // location = 'index.php?route=wkpos/products&user_token={{ user_token }}';*/
/*       }*/
/*     });*/
/*   });*/
/* */
/*   $('body').on('click','#btn-mass-print-form',function(){*/
/*     $('.text-danger').remove();*/
/*     var bar_quantity = $('#barcode-quantity').val();*/
/*     if(bar_quantity <= 1000){*/
/*       $('#mass-print-form').submit();*/
/*     } else {*/
/*       $('#barcode-quantity').after('<div class="text-danger">{{ test_quantity_max }}</div>');*/
/*     }*/
/*   });*/
/* */
/*   function massPrint() {*/
/*     $('.modal-body .text-center').html('<button type="button" id="btn-mass-print-form" class="btn btn-primary">{{ button_print_all }}</button>');*/
/*     $('.modal-title').text('{{ text_multi_print }}');*/
/*     $('#showModal').trigger('click');*/
/*   }*/
/* */
/*   $('#barcode-quantity').on('change', function(){*/
/*     $('#printQuant').val($('#barcode-quantity').val());*/
/*   });*/
/* //--></script>*/
/*   <script type="text/javascript"><!--*/
/*   $('#button-filter').on('click', function() {*/
/*     filter_name = $('input[name=\'filter_name\']').val();*/
/*     if (filter_name) {*/
/*       setHash('filter_name', filter_name);*/
/*     } else {*/
/*       removeHash('filter_name');*/
/*     }*/
/* */
/*     filter_model = $('input[name=\'filter_model\']').val();*/
/*     if (filter_model) {*/
/*       setHash('filter_model', filter_model);*/
/*     } else {*/
/*       removeHash('filter_model');*/
/*     }*/
/* */
/*     filter_barcode = $('input[name=\'filter_barcode\']').val();*/
/*     if (filter_barcode) {*/
/*       setHash('filter_barcode', filter_barcode);*/
/*     } else {*/
/*       removeHash('filter_barcode');*/
/*     }*/
/* */
/*     filter_price = $('input[name=\'filter_price\']').val();*/
/*     if (filter_price) {*/
/*       setHash('filter_price', filter_price);*/
/*     } else {*/
/*       removeHash('filter_price');*/
/*     }*/
/* */
/*     filter_quantity = $('input[name=\'filter_quantity\']').val();*/
/*     if (filter_quantity) {*/
/*       setHash('filter_quantity', filter_quantity);*/
/*     } else {*/
/*       removeHash('filter_quantity');*/
/*     }*/
/* */
/*     filter_assign = $('input[name=\'filter_assign\']').val();*/
/*     if (filter_assign) {*/
/*       setHash('filter_assign', filter_assign);*/
/*     } else {*/
/*       removeHash('filter_assign');*/
/*     }*/
/* */
/*     filter_status = $('select[name=\'filter_status\']').val();*/
/*     if (filter_status) {*/
/*       setHash('filter_status', filter_status);*/
/*     } else {*/
/*       removeHash('filter_status');*/
/*     }*/
/* */
/*     product_listed = 0;*/
/*     product_prev = 0;*/
/*     start = 0;*/
/*     all = false;*/
/*     $('#productBody').html('<tr></tr>');*/
/*     $('#productFoot').html('');*/
/*     loadProducts();*/
/*   });*/
/* //--></script>*/
/* <script type="text/javascript">*/
/*   $(document).ready(function () {*/
/*     var checkFilterName = getHash('filter_name');*/
/*     if (checkFilterName) {*/
/*       filter_name = checkFilterName;*/
/*       $('input[name=\'filter_name\']').val(filter_name);*/
/*     }*/
/*     var checkFilterModel = getHash('filter_model');*/
/*     if (checkFilterModel) {*/
/*       filter_model = checkFilterModel;*/
/*       $('input[name=\'filter_model\']').val(filter_model);*/
/*     }*/
/*     var checkFilterBarcode = getHash('filter_barcode');*/
/*     if (checkFilterBarcode) {*/
/*       filter_barcode = checkFilterBarcode;*/
/*       $('input[name=\'filter_barcode\']').val(filter_barcode);*/
/*     }*/
/*     var checkFilterPrice = getHash('filter_price');*/
/*     if (checkFilterPrice) {*/
/*       filter_price = checkFilterPrice;*/
/*       $('input[name=\'filter_price\']').val(filter_price);*/
/*     }*/
/*     var checkFilterStatus = getHash('filter_status');*/
/*     if (checkFilterStatus) {*/
/*       filter_status = checkFilterStatus;*/
/*       $('select[name=\'filter_status\'] option[value="' + filter_status + '"]').prop('selected', true);*/
/*     }*/
/*     var checkFilterQuantity = getHash('filter_quantity');*/
/*     if (checkFilterQuantity) {*/
/*       filter_quantity = checkFilterQuantity;*/
/*       $('input[name=\'filter_quantity\']').val(filter_quantity);*/
/*     }*/
/*     var checkFilterAssign = getHash('filter_assign');*/
/*     if (checkFilterAssign) {*/
/*       filter_assign = checkFilterAssign;*/
/*       $('input[name=\'filter_assign\']').val(filter_assign);*/
/*     }*/
/*     loadProducts();*/
/*   });*/
/*   $(document).on('keypress', '#barcode-quantity', function(event){*/
/*     if (event.which <= 46 || event.which >= 58) {*/
/*       return false;*/
/*     }*/
/*   });*/
/*   var count_barcode = 0;*/
/*   function massGenerate() {*/
/*     $('.progress').css('display', 'block');*/
/*     $('#generate-barcodes').addClass('disabled');*/
/*     $.ajax({*/
/*       url: 'index.php?route=wkpos/products/massGenerate&user_token={{ user_token }}',*/
/*       type: 'post',*/
/*       data: {count_barcode: count_barcode},*/
/*       dataType: 'json',*/
/*       success: function(json) {*/
/*         if (typeof json['count'] != 'undefined') {*/
/*           $('#progress-bar-success').removeClass('hide').html('<i class="fa fa-check-circle"></i>&nbsp;' + json['success'].replace('%s', count_barcode + json['count']) + '<button type="button" class="close" data-dismiss="alert">&times;</button>');*/
/*           $('.progress-bar-success').css('width', ((count_barcode+number_inslot)/total_products)*100 + '%');*/
/*           $('.progress-bar-success').attr('aria-valuenow', count_barcode+number_inslot);*/
/*         }*/
/*       },*/
/*     }).done(function() {*/
/*       if (count_barcode < total_products) {*/
/*         count_barcode += number_inslot;*/
/*           massGenerate();*/
/*       } else {*/
/*         $('.progress-bar-success').removeClass('active');*/
/*         $('#generate-barcodes').removeClass('disabled');*/
/*         location.reload();*/
/*       }*/
/*     });*/
/*   }*/
/*   $(document).ready(function(){*/
/*       $('#generate-barcodes').tooltip({*/
/*         title:"{{ help_mass_generate }}",*/
/*         placement : 'left',*/
/*         animation: false,*/
/*         trigger : 'hover'*/
/*       });*/
/*   });*/
/*   $(document).on('click', '#button-clear', function() {*/
/*     $('.well input').val('');*/
/*     $('#input-status option:first').prop('selected', true);*/
/*     $('#button-filter').click();*/
/*   });*/
/* </script></div>*/
/* <style type="text/css">*/
/*   .well .form-group {*/
/*     padding-top: 0px;*/
/*     padding-bottom: 0;*/
/*     margin-bottom: 0;*/
/*   }*/
/*   #button-clear, #button-filter {*/
/*     margin-top: 24px;*/
/*   }*/
/*   .radius-0 {*/
/*     border-radius: 0;*/
/*   }*/
/*   thead td a {*/
/*     cursor: pointer;*/
/*   }*/
/* </style>*/
/* {{ footer }}*/
/* */
