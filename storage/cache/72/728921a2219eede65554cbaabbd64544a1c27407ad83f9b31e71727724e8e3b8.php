<?php

/* default/template/wkpos/wkpos.twig */
class __TwigTemplate_f56ada7ff5317c45b917a1c36b4f2826a780994df54e579d891831881b814a77 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir=\"";
        // line 3
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\" class=\"ie8\"><![endif]-->
<!--[if IE 9 ]><html dir=\"";
        // line 4
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\" class=\"ie9\"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<!-- <html dir=\"";
        // line 6
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\" manifest=\"../manifest.appcache\"> -->
<html dir=\"";
        // line 7
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\">
<!--<![endif]-->
<head>
<meta charset=\"UTF-8\" />
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
<title>";
        // line 13
        echo (isset($context["title"]) ? $context["title"] : null);
        echo "</title>
<base href=\"";
        // line 14
        echo (isset($context["base"]) ? $context["base"] : null);
        echo "\" />
";
        // line 15
        if ((isset($context["description"]) ? $context["description"] : null)) {
            // line 16
            echo "<meta name=\"description\" content=\"";
            echo (isset($context["description"]) ? $context["description"] : null);
            echo "\" />
";
        }
        // line 18
        if ((isset($context["keywords"]) ? $context["keywords"] : null)) {
            // line 19
            echo "<meta name=\"keywords\" content= \"";
            echo (isset($context["keywords"]) ? $context["keywords"] : null);
            echo "\" />
";
        }
        // line 21
        echo "<script src=\"catalog/view/javascript/jquery/jquery-2.1.1.min.js\" type=\"text/javascript\"></script>
";
        // line 22
        if (((isset($context["direction"]) ? $context["direction"] : null) == "ltr")) {
            // line 23
            echo "<link id=\"bootstrap-css\" href=\"catalog/view/javascript/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\"
 media=\"screen\" />
";
        } else {
            // line 26
            echo " <link id=\"bootstrap-css\" href=\"wkpos/css/bootstrap-rtl.css\" rel=\"stylesheet\" media=\"screen\" />
";
        }
        // line 28
        echo "<script src=\"catalog/view/javascript/bootstrap/js/bootstrap.min.js\" type=\"text/javascript\"></script>
<link href=\"catalog/view/javascript/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />

<link href=\"//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700\" rel=\"stylesheet\" type=\"text/css\" />
";
        // line 32
        if (((isset($context["direction"]) ? $context["direction"] : null) == "ltr")) {
            // line 33
            echo "  <link id=\"stylesheet\" href=\"catalog/view/theme/default/stylesheet/stylesheet.css\" rel=\"stylesheet\">
";
        } else {
            // line 35
            echo "  <link id=\"stylesheet\" href=\"wkpos/css/stylesheet-rtl.css\" rel=\"stylesheet\">
";
        }
        // line 37
        if (((isset($context["direction"]) ? $context["direction"] : null) == "ltr")) {
            // line 38
            echo "<link id=\"wkpos-css\" href=\"wkpos/css/wkpos.css\" rel=\"stylesheet\" media=\"screen\">
";
        } else {
            // line 40
            echo "<link id=\"wkpos-css\" href=\"wkpos/css/wkpos-rtl.css\" rel=\"stylesheet\" media=\"screen\">
";
        }
        // line 42
        echo "<script src=\"catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js\" type=\"text/javascript\"></script>
<script src=\"catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js\" type=\"text/javascript\"></script>
<script src=\"catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js\" type=\"text/javascript\"></script>
<link href=\"catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css\" type=\"text/css\" rel=\"stylesheet\" media=\"screen\" />
";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["styles"]) ? $context["styles"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 47
            echo "<link href=\"";
            echo $this->getAttribute($context["style"], "href", array());
            echo "\" type=\"text/css\" rel=\"";
            echo $this->getAttribute($context["style"], "rel", array());
            echo "\" media=\"";
            echo $this->getAttribute($context["style"], "media", array());
            echo "\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "<script src=\"wkpos/js/localforage.js\" type=\"text/javascript\"></script>
<script src=\"wkpos/js/common.js\" type=\"text/javascript\"></script>
<script src=\"wkpos/js/wkpos.js\" type=\"text/javascript\"></script>

        <script src=\"wkpos/js/wkpos_receipt.js\" type=\"text/javascript\"></script>
<script src=\"wkpos/js/jQuery.print.min.js\" type=\"text/javascript\"></script>      
";
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["links"]) ? $context["links"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
            // line 56
            echo "<link href=\"";
            echo $this->getAttribute($context["link"], "href", array());
            echo "\" rel=\"";
            echo $this->getAttribute($context["link"], "rel", array());
            echo "\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["scripts"]) ? $context["scripts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 59
            echo "<script src=\"";
            echo $context["script"];
            echo "\" type=\"text/javascript\"></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "<style type=\"text/css\">

 .thermal {
   width: ";
        // line 64
        echo (isset($context["paper_size"]) ? $context["paper_size"] : null);
        echo ";
   font-weight: ";
        // line 65
        echo (isset($context["font_weight"]) ? $context["font_weight"] : null);
        echo ";
 }
 ";
        // line 67
        if (((isset($context["paper_size"]) ? $context["paper_size"] : null) == "595px")) {
            // line 68
            echo "  .thermal {
    max-width: 630px;
  }
  .font-store {
    font-size: 14px;
  }
 ";
        }
        // line 75
        echo " ";
        if (((isset($context["paper_size"]) ? $context["paper_size"] : null) == "58mm")) {
            // line 76
            echo "  .font-store {
    font-size: 11px;
  }
 ";
        }
        // line 80
        echo " ";
        if (((isset($context["paper_size"]) ? $context["paper_size"] : null) == "80mm")) {
            // line 81
            echo "  .font-store {
    font-size: 13px;
  }
 ";
        }
        // line 85
        echo "</style>
</head>
<body>
  <div id=\"top-div\">
    <div id=\"clockin\">
      <div class=\"modal-dialog modal-sm\">
        <div class=\"modal-content\">
          <div class=\"modal-body\">
            <h3><b>";
        // line 93
        echo (isset($context["text_resume_sess"]) ? $context["text_resume_sess"] : null);
        echo "</b></h3>
            <button class=\"btn buttons-sp\" id=\"resumeSession\" tabindex=\"5\"><i class=\"fa fa-hourglass-half\"></i> ";
        // line 94
        echo (isset($context["text_resume"]) ? $context["text_resume"] : null);
        echo "</button>
            <button class=\"btn buttons-sp\" id=\"startSession\" tabindex=\"6\"><i class=\"fa fa-hourglass-start\"></i> ";
        // line 95
        echo (isset($context["text_start_sess"]) ? $context["text_start_sess"] : null);
        echo "</button>
          </div>
        </div>
      </div>
    </div>
    <div id=\"postorder\">
      <div class=\"modal-dialog modal-sm\">
        <div class=\"modal-content\">
          <div class=\"modal-body\">
            <h3 style=\"margin-top: 0;\"><b>";
        // line 104
        echo (isset($context["heading_invoice"]) ? $context["heading_invoice"] : null);
        echo "</b></h3>
            <button class=\"btn buttons-sp\" onclick=\"printInvoice();\"><i class=\"fa fa-print\"></i> ";
        // line 105
        echo (isset($context["button_invoice"]) ? $context["button_invoice"] : null);
        echo "</button>
            <button class=\"btn buttons-sp pull-right\" onclick=\"\$('#postorder, #loader').css('display', 'none');\">";
        // line 106
        echo (isset($context["button_skip"]) ? $context["button_skip"] : null);
        echo " <i class=\"fa fa-arrow-right\"></i></button>
          </div>
        </div>
      </div>
    </div>
    <div class=\"modal\" id=\"loginModalParent\">
      <div class=\"col-sm-7 col-xs-12 home-div\">
        <h2><b>";
        // line 113
        if ((isset($context["pos_heading1"]) ? $context["pos_heading1"] : null)) {
            echo (isset($context["pos_heading1"]) ? $context["pos_heading1"] : null);
        } else {
            echo "A Complete Retail Management Solution";
        }
        echo "</b></h2>
        <div>
      <h3><b>";
        // line 115
        if ((isset($context["pos_heading2"]) ? $context["pos_heading2"] : null)) {
            echo (isset($context["pos_heading2"]) ? $context["pos_heading2"] : null);
        } else {
            echo "Branding";
        }
        echo "</b></h3>
          ";
        // line 116
        if ((isset($context["pos_content"]) ? $context["pos_content"] : null)) {
            // line 117
            echo "            ";
            echo (isset($context["pos_content"]) ? $context["pos_content"] : null);
            echo "
          ";
        } else {
            // line 119
            echo "          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
          ";
        }
        // line 121
        echo "        </div>
      </div>
      <div class=\"col-sm-5 col-xs-12 text-center login-div\">
        <div class=\"form-horizontal\">
          <h3>";
        // line 125
        echo (isset($context["heading_login"]) ? $context["heading_login"] : null);
        echo "</h3>
          <div class=\"form-group input-group\">
            <span class=\"input-group-addon\"><i class=\"fa fa-envelope\"></i></span>
            <input type=\"text\" class=\"form-control\" id=\"input-username\" placeholder=\"";
        // line 128
        echo (isset($context["entry_username"]) ? $context["entry_username"] : null);
        echo "\" value=\"\" name=\"username\" tabindex=\"2\">
          </div>
          <div class=\"form-group input-group\">
            <span class=\"input-group-addon\"><i class=\"fa fa-key\"></i></span>
            <input type=\"password\" class=\"form-control\" id=\"input-password\" placeholder=\"";
        // line 132
        echo (isset($context["entry_password"]) ? $context["entry_password"] : null);
        echo "\" value=\"\" name=\"password\" tabindex=\"3\">
          </div>
          <button class=\"buttons-sp\" id=\"login-submit\" onclick=\"loginUser();\" tabindex=\"4\">";
        // line 134
        echo (isset($context["button_signin"]) ? $context["button_signin"] : null);
        echo "</button>
        </div>
      </div>
    </div>
    <div class=\"col-xs-1\" id=\"pos-side-panel\">
      <div class=\"col-xs-12\" id=\"left-panel\">
        <div class=\"wksidepanel button-payment\" id=\"button-payment\"><i class=\"fa fa-credit-card\"></i><br><span class=\"hidden-xs\">";
        // line 140
        echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
        echo "</span></div>
        <div class=\"wksidepanel\" id=\"button-order\"><i class=\"fa fa-circle-o\"></i><br><span class=\"hidden-xs\">";
        // line 141
        echo (isset($context["text_orders"]) ? $context["text_orders"] : null);
        echo "</span></div>
        <div class=\"wksidepanel\" id=\"button-return\"><i class=\"fa fa-reply\"></i><br><span class=\"hidden-xs\">";
        // line 142
        echo (isset($context["text_returns"]) ? $context["text_returns"] : null);
        echo "</span></div>
        <div class=\"wksidepanel\" id=\"button-account\"><i class=\"fa fa-wrench\"></i><br><span class=\"hidden-xs\">";
        // line 143
        echo (isset($context["text_settings"]) ? $context["text_settings"] : null);
        echo "</span></div>
        <div class=\"wksidepanel\" id=\"button-other\"><i class=\"fa fa-gears\"></i><br><span class=\"hidden-xs\">";
        // line 144
        echo (isset($context["text_others"]) ? $context["text_others"] : null);
        echo "</span></div>
        <div class=\"wksidepanel\" id=\"button-report\"><i class=\"fa fa-list\"></i><br><span class=\"hidden-xs\">";
        // line 145
        echo (isset($context["text_reports"]) ? $context["text_reports"] : null);
        echo "</span></div>
        
      </div>
      <div class=\"mode-div\">
        <button class=\"btn label label-success\" id=\"mode\"><i class=\"fa fa-toggle-on\"></i> <span class=\"hidden-xs\">";
        // line 149
        echo (isset($context["text_online"]) ? $context["text_online"] : null);
        echo "</span></button>
      </div>
    </div>
    <div class=\"col-xs-8 category-div\">
      <div class=\"col-xs-12 upper-category\">
        <div class=\"col-xs-9 col-md-8 lower-category\">
          <div class=\"wkcategory categoryProduct\" category-id=\"0\" style=\"color: #0c8c00;\"><span class=\"hidden-xs\">";
        // line 155
        echo (isset($context["text_populars"]) ? $context["text_populars"] : null);
        echo "</span><span class=\"hidden-sm hidden-md hidden-lg\"><i class=\"fa fa-fire font-22\"></i></span></div>
          <span class=\"hidden-xs\">
          ";
        // line 157
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["category"]) {
            if (($context["key"] < 2)) {
                // line 158
                echo "            <div class=\"wkcategory categoryProduct\" category-id=\"";
                echo $this->getAttribute($context["category"], "category_id", array());
                echo "\">";
                echo $this->getAttribute($context["category"], "name", array());
                echo "</div>
          ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 160
        echo "          </span>
          <span data-toggle=\"modal\" data-target=\"#categoryList\" class=\"wkcategory\"><i class=\"fa fa-list font-22\"></i></span>
        </div>
        <div class=\"col-xs-3 col-md-4\" id=\"search-div\">
          <input type=\"text\" name=\"search\" id=\"search\" class=\"form-control\" placeholder=\"";
        // line 164
        echo (isset($context["text_search"]) ? $context["text_search"] : null);
        echo "\">
        </div>
      </div>
      <div class=\"col-xs-12 text-center\" style=\"padding: 0;\">
        <h4>";
        // line 168
        echo (isset($context["text_all_products"]) ? $context["text_all_products"] : null);
        echo "</h4>
        <div class=\"col-xs-12\" id=\"product-panel\">
        </div>
      </div>
      <div class=\"parents account-parent\">
        <div class=\"payment-child\" style=\"width: 35%;\">
          <div class=\"wkpaymentmethod wkaccounts\" type=\"basic\"><i class=\"fa fa-wrench\"></i> <span class=\"margin-10\">";
        // line 174
        echo (isset($context["text_basic_settings"]) ? $context["text_basic_settings"] : null);
        echo "</span><i class=\"fa ";
        if (((isset($context["direction"]) ? $context["direction"] : null) == "ltr")) {
            echo "fa-chevron-right";
        } else {
            echo "fa-chevron-left";
        }
        echo " hide\"></i></div>
          <div class=\"wkpaymentmethod wkaccounts\" type=\"other\"><i class=\"fa fa-cogs\"></i> <span class=\"margin-10\">";
        // line 175
        echo (isset($context["text_other_settings"]) ? $context["text_other_settings"] : null);
        echo "</span><i class=\"fa ";
        if (((isset($context["direction"]) ? $context["direction"] : null) == "ltr")) {
            echo "fa-chevron-right";
        } else {
            echo "fa-chevron-left";
        }
        echo " hide\"></i></div>
        </div>
        <div class=\"payment-child2 form-horizontal\" style=\"width: 65%;\">
          <span class=\"pull-right cursor color-black close-it\"><i class=\"fa fa-times font-25\"></i></span>
          <div class=\"basic-account setting-account hide\">
            <div class=\"scroll-account\">
              <div class=\"form-group required\">
                <label class=\"col-sm-4 control-label\" for=\"first-name\">";
        // line 182
        echo (isset($context["text_firstname"]) ? $context["text_firstname"] : null);
        echo "</label>
                <div class=\"col-sm-8\">
                  <input class=\"form-control\" id=\"first-name\" name=\"firstname\" placeholder=\"";
        // line 184
        echo (isset($context["text_firstname"]) ? $context["text_firstname"] : null);
        echo "\" type=\"text\" value=\"";
        echo (isset($context["firstname"]) ? $context["firstname"] : null);
        echo "\">
                </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-4 control-label\" for=\"last-name\">";
        // line 188
        echo (isset($context["text_lastname"]) ? $context["text_lastname"] : null);
        echo "</label>
                <div class=\"col-sm-8\">
                  <input class=\"form-control\" id=\"last-name\" name=\"lastname\" placeholder=\"";
        // line 190
        echo (isset($context["text_lastname"]) ? $context["text_lastname"] : null);
        echo "\" type=\"text\" value=\"";
        echo (isset($context["lastname"]) ? $context["lastname"] : null);
        echo "\">
                </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-4 control-label\" for=\"user-name\">";
        // line 194
        echo (isset($context["text_username"]) ? $context["text_username"] : null);
        echo "</label>
                <div class=\"col-sm-8\">
                  <input class=\"form-control\" id=\"user-name\" name=\"username\" placeholder=\"";
        // line 196
        echo (isset($context["text_username"]) ? $context["text_username"] : null);
        echo "\" type=\"text\" value=\"";
        echo (isset($context["username"]) ? $context["username"] : null);
        echo "\">
                </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-4 control-label\" for=\"account-email\">";
        // line 200
        echo (isset($context["text_account_email"]) ? $context["text_account_email"] : null);
        echo "</label>
                <div class=\"col-sm-8\">
                  <input class=\"form-control\" id=\"account-email\" name=\"account_email\" placeholder=\"";
        // line 202
        echo (isset($context["text_account_email"]) ? $context["text_account_email"] : null);
        echo "\" type=\"text\" value=\"";
        echo (isset($context["email"]) ? $context["email"] : null);
        echo "\">
                </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-4 control-label\" for=\"account-ppwd\">";
        // line 206
        echo (isset($context["text_ppassword"]) ? $context["text_ppassword"] : null);
        echo "</label>
                <div class=\"col-sm-8\">
                  <input class=\"form-control\" id=\"account-ppwd\" name=\"account_ppwd\" placeholder=\"";
        // line 208
        echo (isset($context["text_ppassword"]) ? $context["text_ppassword"] : null);
        echo "\" type=\"password\" value=\"\">
                </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-4 control-label\" for=\"account-npwd\">";
        // line 212
        echo (isset($context["text_npassword"]) ? $context["text_npassword"] : null);
        echo "</label>
                <div class=\"col-sm-8\">
                  <input class=\"form-control\" id=\"account-npwd\" name=\"account_npwd\" placeholder=\"";
        // line 214
        echo (isset($context["text_npassword"]) ? $context["text_npassword"] : null);
        echo "\" type=\"password\" value=\"\">
                </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-4 control-label\" for=\"account-cpwd\">";
        // line 218
        echo (isset($context["text_cpassword"]) ? $context["text_cpassword"] : null);
        echo "</label>
                <div class=\"col-sm-8\">
                  <input class=\"form-control\" id=\"account-cpwd\" name=\"account_cpwd\" placeholder=\"";
        // line 220
        echo (isset($context["text_cpassword"]) ? $context["text_cpassword"] : null);
        echo "\" type=\"password\" value=\"\">
                </div>
              </div>
            </div>
            <button class=\"col-xs-12 btn buttons-sp button-accounts\" stype=\"basic\"><strong>";
        // line 224
        echo (isset($context["text_save_details"]) ? $context["text_save_details"] : null);
        echo "</strong></button>
          </div>
          <div class=\"other-account setting-account hide\">
            <div class=\"form-group\">
              <label class=\"col-sm-4 control-label\" for=\"input-langauge\">";
        // line 228
        echo (isset($context["text_language"]) ? $context["text_language"] : null);
        echo "</label>
              <div class=\"col-sm-8\">
                <select class=\"form-control\" id=\"input-language\">
                  ";
        // line 231
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language_value"]) {
            // line 232
            echo "                  ";
            if (((isset($context["language"]) ? $context["language"] : null) == $this->getAttribute($context["language_value"], "code", array()))) {
                // line 233
                echo "                  <option value=\"";
                echo $this->getAttribute($context["language_value"], "code", array());
                echo "\" selected=\"selected\">";
                echo $this->getAttribute($context["language_value"], "name", array());
                echo "</option>
                  ";
            } else {
                // line 235
                echo "                  <option value=\"";
                echo $this->getAttribute($context["language_value"], "code", array());
                echo "\">";
                echo $this->getAttribute($context["language_value"], "name", array());
                echo "</option>
                  ";
            }
            // line 237
            echo "                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language_value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 238
        echo "                </select>
              </div>
            </div>
            <div class=\"form-group\">
              <label class=\"col-sm-4 control-label\" for=\"input-currency\">";
        // line 242
        echo (isset($context["text_currency"]) ? $context["text_currency"] : null);
        echo "</label>
              <div class=\"col-sm-8\">
                <select class=\"form-control\" id=\"input-currency\">
                  ";
        // line 245
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["currencies"]) ? $context["currencies"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["currency_value"]) {
            // line 246
            echo "                  ";
            if (((isset($context["currency"]) ? $context["currency"] : null) == $this->getAttribute($context["currency_value"], "code", array()))) {
                // line 247
                echo "                    <option value=\"";
                echo $this->getAttribute($context["currency_value"], "code", array());
                echo "\" selected=\"selected\">";
                echo $this->getAttribute($context["currency_value"], "title", array());
                echo "</option>
                  ";
            } else {
                // line 249
                echo "                    <option value=\"";
                echo $this->getAttribute($context["currency_value"], "code", array());
                echo "\">";
                echo $this->getAttribute($context["currency_value"], "title", array());
                echo "</option>
                  ";
            }
            // line 251
            echo "                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currency_value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 252
        echo "                </select>
              </div>
            </div>
            <button class=\"col-xs-12 btn buttons-sp button-accounts\" stype=\"other\"><strong>";
        // line 255
        echo (isset($context["text_save"]) ? $context["text_save"] : null);
        echo "</strong></button>
          </div>
        </div>
      </div>
      <div class=\"parents payment-parent\">
        <div class=\"payment-child\">
          <h3 class=\"payment-heading\"><strong>";
        // line 261
        echo (isset($context["heading_payments"]) ? $context["heading_payments"] : null);
        echo ":</strong></h3>
          <div id=\"payment-methods\">
            ";
        // line 263
        if ((isset($context["cash_payment_status"]) ? $context["cash_payment_status"] : null)) {
            // line 264
            echo "            <div class=\"wkpaymentmethod\" type=\"cash-payment\"><i class=\"fa fa-money\"></i> <span class=\"margin-10\">";
            echo (isset($context["cash_payment_title"]) ? $context["cash_payment_title"] : null);
            echo "</span><i class=\"fa ";
            if (((isset($context["direction"]) ? $context["direction"] : null) == "ltr")) {
                echo "fa-chevron-right";
            } else {
                echo "fa-chevron-left";
            }
            echo "t hide\"></i></div>
            ";
        }
        // line 266
        echo "            ";
        if ((isset($context["card_payment_status"]) ? $context["card_payment_status"] : null)) {
            // line 267
            echo "            <div class=\"wkpaymentmethod\" type=\"card-payment\"><i class=\"fa fa-credit-card\"></i> <span class=\"margin-10\">";
            echo (isset($context["card_payment_title"]) ? $context["card_payment_title"] : null);
            echo "</span><i class=\"fa ";
            if (((isset($context["direction"]) ? $context["direction"] : null) == "ltr")) {
                echo "fa-chevron-right";
            } else {
                echo "fa-chevron-left";
            }
            echo " hide\"></i></div>
            ";
        }
        // line 269
        echo "          </div>
        </div>
        <div class=\"payment-child2 form-horizontal\">
          <span class=\"pull-right cursor color-black close-it\"><i class=\"fa fa-times font-25\"></i></span>
          <div class=\"cash-payment hide\">
            <div class=\"form-group\">
              <label class=\"col-sm-4 control-label\">";
        // line 275
        echo (isset($context["text_balance_due"]) ? $context["text_balance_due"] : null);
        echo "</label>
              <div class=\"col-sm-8\">
                <div class=\"form-control\" id=\"balance-due\"></div>
              </div>
            </div>
            <div class=\"form-group\">
              <label class=\"col-sm-4 control-label\" for=\"amount-tendered\">";
        // line 281
        echo (isset($context["text_tendered"]) ? $context["text_tendered"] : null);
        echo "</label>
              <div class=\"col-sm-8\">
                <div class=\"input-group input-group1\">
                  ";
        // line 284
        if (((isset($context["symbol_position"]) ? $context["symbol_position"] : null) == "L")) {
            // line 285
            echo "                  <span class=\"input-group-addon currency\"><strong>";
            echo (isset($context["currency_code"]) ? $context["currency_code"] : null);
            echo "</strong></span>
                  ";
        } else {
            // line 287
            echo "                  <span class=\"input-group-addon currency\" style=\"display: none;\">";
            echo (isset($context["currency_code"]) ? $context["currency_code"] : null);
            echo "</span>
                  ";
        }
        // line 289
        echo "                  <input class=\"form-control\" id=\"amount-tendered\" placeholder=\"\" type=\"text\" onkeypress=\"return validate(event, this)\">
                  ";
        // line 290
        if (((isset($context["symbol_position"]) ? $context["symbol_position"] : null) == "R")) {
            // line 291
            echo "                  <span class=\"input-group-addon currency\"><strong>";
            echo (isset($context["currency_code"]) ? $context["currency_code"] : null);
            echo "</strong></span>
                  ";
        } else {
            // line 293
            echo "                  <span class=\"input-group-addon currency\" style=\"display: none;\">";
            echo (isset($context["currency_code"]) ? $context["currency_code"] : null);
            echo "</span>
                  ";
        }
        // line 295
        echo "                </div>
              </div>
            </div>
            <div class=\"form-group\">
              <label class=\"col-sm-4 control-label\">";
        // line 299
        echo (isset($context["text_change"]) ? $context["text_change"] : null);
        echo "</label>
              <div class=\"col-sm-8\">
                <div class=\"form-control\" id=\"change\"></div>
              </div>
            </div>
          </div>
          <div class=\"all-payment hide\">
            ";
        // line 306
        if ((isset($context["credit_status"]) ? $context["credit_status"] : null)) {
            // line 307
            echo "            <div class=\"form-group\">
              <label class=\"col-sm-4 control-label\">";
            // line 308
            echo (isset($context["text_balance_credit"]) ? $context["text_balance_credit"] : null);
            echo "</label>
              <div class=\"col-sm-8\">
                <div class=\"form-control input-group\" id=\"balance-credit\">
                  ";
            // line 311
            echo (isset($context["select_customer_first"]) ? $context["select_customer_first"] : null);
            echo "
                </div>
              </div>
            </div>
            ";
        }
        // line 316
        echo "            <div class=\"form-group\">
              <div class=\"col-sm-12\">
                <textarea class=\"form-control\" id=\"orderNote\" placeholder=\"";
        // line 318
        echo (isset($context["text_add_order_note"]) ? $context["text_add_order_note"] : null);
        echo "\"></textarea>
              </div>
            </div>
            <button class=\"col-xs-12 btn buttons-sp accept-payment\" ptype=\"\"><strong>";
        // line 321
        echo (isset($context["button_payment"]) ? $context["button_payment"] : null);
        echo "</strong></button>
          </div>
        </div>
      </div>
      <!-- POS Update Code for Return  -->
      <div class=\"parents return-parent\">
        <!-- <h3 class=\"pull-left\" id=\"return-heading\">";
        // line 327
        echo (isset($context["text_returned_product"]) ? $context["text_returned_product"] : null);
        echo "</h3> -->
          <span class=\"pull-right cursor color-black close-it\" onclick=\"\$('#returns').html('');\"><i class=\"fa fa-times font-25\"></i></span>
        <div class=\"col-sm-12\" id=\"return-container\">
          <div class=\"row\" id=\"returns\"></div>
        </div>
      </div>
      <div class=\"parents return-form-parent\">
        <div class=\"col-xs-12\">
          <h3 class=\"text-center\">";
        // line 335
        echo (isset($context["return_heading"]) ? $context["return_heading"] : null);
        echo "<span class=\"pull-right cursor color-black close-it\"><i class=\"fa fa-times font-25\"></i></span></h3>
        </div>
        <div class=\"col-sm-12\" style=\"overflow-y:auto;\">
          <div class=\"row\"  id=\"return-form\">

          </div>
        </div>
      </div>
      <!-- POS Update Code for Return  -->
      <div class=\"parents order-parent\">
        <div class=\"col-xs-12 order-child\">
          <div class=\"wkcategory wkorder\" otype=\"1\">";
        // line 346
        echo (isset($context["text_previous"]) ? $context["text_previous"] : null);
        echo "</div>
          <div class=\"wkcategory wkorder\" otype=\"2\">";
        // line 347
        echo (isset($context["text_onhold"]) ? $context["text_onhold"] : null);
        echo "</div>
          <div class=\"wkcategory wkorder\" otype=\"3\">";
        // line 348
        echo (isset($context["text_offline"]) ? $context["text_offline"] : null);
        echo "</div>
          <span class=\"pull-right cursor color-black close-it\"><i class=\"fa fa-times font-25\"></i></span>
        </div>
        <div class=\"col-sm-12\" id=\"order-container\">
          <div class=\"row\" id=\"orders\"></div>
        </div>
      </div>
      <div class=\"parents other-parent\">
        <div class=\"col-xs-12 other-child\">
          <div class=\"wkcategory wkother\" otype=\"1\">";
        // line 357
        echo (isset($context["text_low_stock"]) ? $context["text_low_stock"] : null);
        echo "</div>
          <div class=\"wkcategory wkother\" otype=\"2\">";
        // line 358
        echo (isset($context["text_request"]) ? $context["text_request"] : null);
        echo "</div>
          <div class=\"wkcategory wkother\" otype=\"3\">";
        // line 359
        echo (isset($context["text_request_history"]) ? $context["text_request_history"] : null);
        echo "</div>
          <!-- <div class=\"wkcategory wkother\" otype=\"4\">";
        // line 360
        echo (isset($context["text_inventory_report"]) ? $context["text_inventory_report"] : null);
        echo "</div> -->
          <span class=\"pull-right cursor color-black close-it\"><i class=\"fa fa-times font-25\"></i></span>
        </div>
        <div class=\"col-sm-12\" id=\"other-container\">
          <div class=\"row\" id=\"others\"></div>
        </div>
      </div>
      <!-- New tab start-->
      <div class=\"parents report-parent\">
        <div class=\"col-xs-12 report-child\">
          <div class=\"wkcategory wkreport\" otype=\"1\">";
        // line 370
        echo (isset($context["text_inventory_report"]) ? $context["text_inventory_report"] : null);
        echo "</div>
          <span class=\"pull-right cursor color-black close-it\"><i class=\"fa fa-times font-25\"></i></span>
        </div>
        <div class=\"col-sm-12\" id=\"report-container\">
          <div class=\"row\">
            
            <div class=\"col-sm-8\" id=\"reports\"></div>
          </div>
        </div>
      </div>
      <!-- New tab end-->
    </div>
    <div class=\"col-xs-3\" id=\"cart-panel\">
      <div id=\"logger-detail\">
        <div class=\"logger-div pull-left\">
          <img src=\"";
        // line 385
        echo (isset($context["image"]) ? $context["image"] : null);
        echo "\" class=\"img-responsive logger-img pull-left\" width=\"40\" height=\"40\">
          <div class=\"pull-left\" style=\"margin-left: 5px;\">
            <span class=\"logger-name\">";
        // line 387
        echo (isset($context["name"]) ? $context["name"] : null);
        echo "</span><br>
            <span class=\"logger-post hidden-xs\">(";
        // line 388
        echo (isset($context["group_name"]) ? $context["group_name"] : null);
        echo ")</span><br>
            <div class=\"dropdown\">
              <button class=\"btn label label-info dropdown-toggle\" data-toggle=\"dropdown\">";
        // line 390
        echo (isset($context["text_more"]) ? $context["text_more"] : null);
        echo " <i class=\"caret\"></i></button>
              <ul class=\"dropdown-menu dropdown-menu-left cursor\">
                <li>
                  <a onclick=\"accountSettings(this);\">";
        // line 393
        echo (isset($context["text_account_settings"]) ? $context["text_account_settings"] : null);
        echo "</a>
                </li>
                <li>
                  <a onclick=\"logout();\">";
        // line 396
        echo (isset($context["text_logout"]) ? $context["text_logout"] : null);
        echo "</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class=\"sidepanel color-black\" style=\"overflow-y: auto;\">
        <div class=\"upper-category\"><i class=\"fa ";
        // line 404
        if (((isset($context["direction"]) ? $context["direction"] : null) == "ltr")) {
            echo "fa-chevron-right";
        } else {
            echo "fa-chevron-left";
        }
        echo " pull-right close-it cursor\"></i><span class=\"order-txn\">";
        echo (isset($context["text_order_id"]) ? $context["text_order_id"] : null);
        echo "</span> #<span class=\"oid\"></span></div>
        <div class=\"table-responsive hide\" id=\"return-section\">

        </div>
        <div class=\"col-xs-12 hide\" id=\"return-details\">

        </div>
        <div id=\"sidepanel-inner\" style=\"display: none;\">
          <div class=\"col-xs-12\" id=\"order-upper-div\">
            <div class=\"pull-left\">
              <span class=\"order-date\"></span><br/>
              <span class=\"order-time\"></span><br/>
              <span>";
        // line 416
        echo (isset($context["text_purchase"]) ? $context["text_purchase"] : null);
        echo "</span>
            </div>
            <div class=\"pull-right\" id=\"order-address\"></div>
          </div>
          <div class=\"col-xs-12\" style=\"margin-top: 10px;\">
            <span class=\"pull-left font-bold\">";
        // line 421
        echo (isset($context["text_shipping_mode"]) ? $context["text_shipping_mode"] : null);
        echo ":&nbsp;</span>
            <span class=\"oshipping\">";
        // line 422
        echo (isset($context["text_pickup"]) ? $context["text_pickup"] : null);
        echo "</span>
          </div>
          <h5 class=\"col-xs-12 font-bold\">";
        // line 424
        echo (isset($context["text_order_details"]) ? $context["text_order_details"] : null);
        echo "</h5>
          <div class=\"width-100 order-scroll\">
            <div class=\"col-xs-12 table-responsive\">
              <table class=\"table table-hover margin-bottom-0\" id=\"oitem-table\">
                <tbody id=\"oitem-body\">
                </tbody>
              </table>
            </div>
            <hr class=\"hr-tag\">
            <hr class=\"hr-tag\">
            <div class=\"col-xs-12 table-responsive\">
              <table class=\"table table-hover margin-bottom-0\">
                <tbody id=\"oTotals\">
                </tbody>
              </table>
            </div>
          </div>
          <hr class=\"hr-tag\">
          <div class=\"col-xs-12 table-responsive table-total\" style=\"margin: 0; width: 100%;\">
            <table class=\"table table-hover margin-bottom-0\">
              <tbody>
                <tr>
                  <td class=\"text-left\">";
        // line 446
        echo (isset($context["text_total"]) ? $context["text_total"] : null);
        echo "</td>
                  <td class=\"text-right oTotal\">00.00</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class=\"col-xs-12\" style=\"margin-top: 5px;\">
            <span class=\"pull-left font-bold\">";
        // line 453
        echo (isset($context["text_payment_mode"]) ? $context["text_payment_mode"] : null);
        echo ":&nbsp;</span>
            <span class=\"opayment\"></span>
          </div>
          <div class=\"col-xs-12\" style=\"margin-top: 5px;\">
            <span class=\"pull-left font-bold\">";
        // line 457
        echo (isset($context["text_note"]) ? $context["text_note"] : null);
        echo ":&nbsp;</span>
            <span class=\"onote\"></span>
          </div>
          <div class=\"col-xs-12\" style=\"margin-top: 5px;\">
            <button onclick=\"printBill();\" class=\"buttons-sp width-100\">";
        // line 461
        echo (isset($context["text_print"]) ? $context["text_print"] : null);
        echo "</button>
          </div>
        </div>
        <div class=\"order-loader hide\">
          <div class=\"cp-spinner cp-round\"></div>
        </div>
      </div>
      <div class=\"col-xs-12 upper-cart-detail\">
        <div class=\"col-xs-12 lower-cart-detail\">
          <div class=\"pull-left\"><i class=\"fa fa-trash cursor font-22\" title=\"";
        // line 470
        echo (isset($context["text_delete_cart"]) ? $context["text_delete_cart"] : null);
        echo "\" data-toggle=\"tooltip\" onclick=\"cart_list.delete(1)\"></i></div>
          <b class=\"cursor\" id=\"more-carts\"><span class=\"\">";
        // line 471
        echo (isset($context["text_cart_details"]) ? $context["text_cart_details"] : null);
        echo "</span> (<span class=\"cart-total\">0</span>) <span class=\"caret\"> </span></b>
          <div id=\"upper-cart\">
          </div>
          <div class=\"pull-right\"><i class=\"fa fa-plus-circle cursor font-22\" title=\"";
        // line 474
        echo (isset($context["text_add_product"]) ? $context["text_add_product"] : null);
        echo "\" data-toggle=\"modal\" data-target=\"#addProduct\"></i></div>
        </div>
        <br>
        <div id=\"cart-detail\">
          <div class=\"table-responsive\">
            <table class=\"table table-hover margin-bottom-0\" id=\"item-table\">
              <tbody id=\"item-body\">
                <tr><td class=\"text-center\">";
        // line 481
        echo (isset($context["text_empty_cart"]) ? $context["text_empty_cart"] : null);
        echo "</td></tr>
              </tbody>
            </table>
          </div>
          <hr class=\"hr-tag\">
          <hr class=\"hr-tag\">
          <div class=\"table-responsive div-totals\">
            <table class=\"table table-hover margin-bottom-0\">
              <tbody>
                <tr>
                  <td class=\"text-left\">";
        // line 491
        echo (isset($context["text_subtotal"]) ? $context["text_subtotal"] : null);
        echo "</td>
                  <td class=\"text-right\" id=\"subtotal\">0.00</td>
                </tr>
                ";
        // line 494
        if (((isset($context["discount_sort_order"]) ? $context["discount_sort_order"] : null) < (isset($context["tax_sort_order"]) ? $context["tax_sort_order"] : null))) {
            // line 495
            echo "                <tr id=\"couprow\" style=\"display: none;\">
                  <td class=\"text-left\">";
            // line 496
            echo (isset($context["button_coupon"]) ? $context["button_coupon"] : null);
            echo "</td>
                  <td class=\"text-right\" id=\"coupondisc\">\$0.00</td>
                </tr>
                  ";
            // line 499
            if ((isset($context["tax_status"]) ? $context["tax_status"] : null)) {
                // line 500
                echo "                  <tr>
                    <td class=\"text-left\">";
                // line 501
                echo (isset($context["button_tax"]) ? $context["button_tax"] : null);
                echo "</td>
                    <td class=\"text-right\" id=\"tax\">\$0.00</td>
                  </tr>
                  ";
            }
            // line 505
            echo "                ";
        } else {
            // line 506
            echo "                  ";
            if ((isset($context["tax_status"]) ? $context["tax_status"] : null)) {
                // line 507
                echo "                  <tr>
                    <td class=\"text-left\">";
                // line 508
                echo (isset($context["button_tax"]) ? $context["button_tax"] : null);
                echo "</td>
                    <td class=\"text-right\" id=\"tax\">\$0.00</td>
                  </tr>
                  ";
            }
            // line 512
            echo "                  <tr id=\"couprow\" style=\"display: none;\">
                    <td class=\"text-left\">";
            // line 513
            echo (isset($context["button_coupon"]) ? $context["button_coupon"] : null);
            echo "</td>
                    <td class=\"text-right\" id=\"coupondisc\">\$0.00</td>
                  </tr>
                ";
        }
        // line 517
        echo "                  <tr id=\"discrow\" style=\"display: none;\">
                    <td class=\"text-left\" id=\"discname\">";
        // line 518
        echo (isset($context["button_discount"]) ? $context["button_discount"] : null);
        echo "</td>
                    <td class=\"text-right\" id=\"discount\">\$0.00</td>
                  </tr>
                ";
        // line 521
        if ((isset($context["home_delivery_status"]) ? $context["home_delivery_status"] : null)) {
            // line 522
            echo "                <tr>
                  <td>
                    <div class=\"checkbox checkbox-primary\"><input id=\"checkbox-home-delivery\" type=\"checkbox\"><label for=\"checkbox-home-delivery\">";
            // line 524
            echo (isset($context["home_delivery_title"]) ? $context["home_delivery_title"] : null);
            echo "</label>
                    </div>
                  </td>
                  <td><input type=\"number\" class=\"form-control pull-right\" placeholder=\"";
            // line 527
            echo (isset($context["entry_delivery_charge"]) ? $context["entry_delivery_charge"] : null);
            echo "\" id=\"delivery-charge\" min=\"0\" onkeypress=\"return validate(event, this)\"></td>
                </tr>
                ";
        }
        // line 530
        echo "              </tbody>
            </table>
          </div>
        </div>
        <hr class=\"hr-tag\">
        <div class=\"table-responsive table-total\">
          <table class=\"table table-hover margin-bottom-0\">
            <tbody>
              <tr>
                <td class=\"text-left\">";
        // line 539
        echo (isset($context["text_total"]) ? $context["text_total"] : null);
        echo "</td>
                <td class=\"text-right\" id=\"cartTotal\">00.00</td>
              </tr>
            </tbody>
          </table>
        </div>

        <button class=\"col-xs-12 btn\" id=\"button-customer\" data-toggle=\"modal\" data-target=\"#customerSearch\"><i class=\"fa fa-user font-22\"></i> <strong class=\"\" id=\"customer-name\">";
        // line 546
        echo (isset($context["button_customer"]) ? $context["button_customer"] : null);
        echo "</strong></button>
        <div class=\"btn-group\" style=\"width: 100%;\">
          ";
        // line 548
        if (((isset($context["discount_status"]) ? $context["discount_status"] : null) && (isset($context["coupon_status"]) ? $context["coupon_status"] : null))) {
            // line 549
            echo "          <button class=\"col-xs-6 btn buttons-sp\" data-toggle=\"modal\" data-target=\"#addDiscount\" id=\"button-discount\"><i class=\"fa fa-minus-square\"></i> <strong class=\"\">";
            echo (isset($context["button_discount"]) ? $context["button_discount"] : null);
            echo "</strong></button>
          <button class=\"col-xs-6 btn buttons-sp\" data-toggle=\"modal\" data-target=\"#addCoupon\" id=\"button-coupon\"><i class=\"fa fa-ticket\"></i> <strong class=\"\">";
            // line 550
            echo (isset($context["button_coupon"]) ? $context["button_coupon"] : null);
            echo "</strong></button>
          ";
        } else {
            // line 552
            echo "          ";
            if ((isset($context["discount_status"]) ? $context["discount_status"] : null)) {
                // line 553
                echo "          <button class=\"col-xs-12 btn buttons-sp\" data-toggle=\"modal\" data-target=\"#addDiscount\" id=\"button-discount\"><i class=\"fa fa-minus-square\"></i> <strong class=\"\">";
                echo (isset($context["button_discount"]) ? $context["button_discount"] : null);
                echo "</strong></button>
          ";
            }
            // line 555
            echo "          ";
            if ((isset($context["coupon_status"]) ? $context["coupon_status"] : null)) {
                // line 556
                echo "          <button class=\"col-xs-12 btn buttons-sp\" data-toggle=\"modal\" data-target=\"#addCoupon\" id=\"button-coupon\"><i class=\"fa fa-ticket\"></i> <strong class=\"\">";
                echo (isset($context["button_coupon"]) ? $context["button_coupon"] : null);
                echo "</strong></button>
          ";
            }
            // line 558
            echo "          ";
        }
        // line 559
        echo "        </div>
        <button class=\"col-xs-12 btn buttons-sp button-payment btn-checkout\"><i class=\"fa fa-money\"></i> <strong class=\"\">";
        // line 560
        echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
        echo "</strong></button>
        <button class=\"col-xs-12 btn buttons-sp btn-checkout\" data-toggle=\"modal\" data-target=\"#holdOrder\" style=\"background-color: #f1b40f;\"><i class=\"fa fa-pause\"></i> <strong class=\"\">";
        // line 561
        echo (isset($context["text_hold_order"]) ? $context["text_hold_order"] : null);
        echo "</strong></button>
        <!-- POS Update Code -->
        <button class=\"col-xs-12 btn buttons-sp btn-return\" style=\"background-color: #5bc0de;\"><i class=\"fa fa-reply\"></i> <strong>";
        // line 563
        echo (isset($context["button_return"]) ? $context["button_return"] : null);
        echo "</strong></button>
      </div>
    </div>
    <div id=\"return-order\" class=\"modal fade\" role=\"dialog\" tabindex=\"-1\">
      <div class=\"modal-dialog modal-sm\">
        <div class=\"modal-content\">
          <div class=\"product-option-modal\" data-dismiss=\"modal\"><i class=\"fa fa-times\"></i></div>
          <div class=\"modal-header\">
            <h4 class=\"modal-title\"></h4>
          </div>
          <div class=\"modal-body color-black\">
            <div class=\"form-group\">
              <input type=\"text\" id=\"search-order-id\" class=\"form-control\" placeholder=\"";
        // line 575
        echo (isset($context["placeholder_order_id"]) ? $context["placeholder_order_id"] : null);
        echo "\" onkeypress=\"return isNumber(event);\" ondrop=\"return false;\">
            </div>
          </div>
          <div class=\"table\" id=\"search-order-result\">

          </div>
          <div class=\"modal-footer\">
            <button type=\"button\" class=\"btn btn-info\" data-dismiss=\"modal\">";
        // line 582
        echo (isset($context["button_close"]) ? $context["button_close"] : null);
        echo "</button>
          </div>
        </div>
      </div>
    </div>
    <div class=\"pull-right color-white\" id=\"fixed-div\">
      <button class=\"btn label label-info barcode-scan\" data-toggle=\"modal\" data-target=\"#barcodeScan\" title=\"";
        // line 588
        echo "Barcode Scan";
        echo "\" data-toggle=\"tooltip\"><i class=\"fa fa-barcode\"></i></button>
      <span id=\"hold-carts\">
        <button class=\"btn label label-info\" title=\"";
        // line 590
        echo (isset($context["text_view_hold"]) ? $context["text_view_hold"] : null);
        echo "\" data-toggle=\"tooltip\">
          <span class=\"fa fa-pause\"></span>
        </button>
        <div class=\"label label-danger cart-hold fix-label\">0</div>
      </span>
      <span id=\"show-cart\">
        <div class=\"btn label label-info\" title=\"";
        // line 596
        echo (isset($context["text_cart_items"]) ? $context["text_cart_items"] : null);
        echo "\" data-toggle=\"tooltip\">
          <span class=\"fa fa-shopping-cart\"></span>
        </div>
        <div class=\"label label-danger cart-total fix-label\">0</div>
      </span>
    </div>

    <div id=\"customerSearch\" class=\"modal fade\" role=\"dialog\" tabindex=\"-1\">
      <div class=\"modal-dialog modal-sm\">
        <div class=\"modal-content\">
          <div class=\"product-option-modal\" data-dismiss=\"modal\"><i class=\"fa fa-times\"></i></div>
          <div class=\"modal-header\">
            <h4 class=\"modal-title\">";
        // line 608
        echo (isset($context["text_customer_details"]) ? $context["text_customer_details"] : null);
        echo "</h4>
          </div>
          <div class=\"modal-body\">
            <div class=\"searchCustomer\">
              <input type=\"text\" id=\"searchCustomer\" placeholder=\"";
        // line 612
        echo (isset($context["entry_search_customer"]) ? $context["entry_search_customer"] : null);
        echo "\" class=\"form-control\" autocomplete=\"off\">
              <div id=\"putCustomer\">
              </div>
            </div>
            <div class=\"addCustomer form-horizontal hide\">
              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\" for=\"input-customer-firstname\">";
        // line 618
        echo (isset($context["entry_firstname"]) ? $context["entry_firstname"] : null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"firstname\" value=\"\" placeholder=\"";
        // line 620
        echo (isset($context["entry_firstname"]) ? $context["entry_firstname"] : null);
        echo "\" id=\"input-customer-firstname\" class=\"form-control\" autocomplete=\"off\" />
                </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\" for=\"input-customer-lastname\">";
        // line 624
        echo (isset($context["entry_lastname"]) ? $context["entry_lastname"] : null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"lastname\" value=\"\" placeholder=\"";
        // line 626
        echo (isset($context["entry_lastname"]) ? $context["entry_lastname"] : null);
        echo "\" id=\"input-customer-lastname\" class=\"form-control\" autocomplete=\"off\" />
                </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\" for=\"input-telephone\">";
        // line 630
        echo (isset($context["entry_telephone"]) ? $context["entry_telephone"] : null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"tel\" name=\"telephone\" value=\"\" placeholder=\"";
        // line 632
        echo (isset($context["entry_telephone"]) ? $context["entry_telephone"] : null);
        echo "\" id=\"input-telephone\" class=\"form-control\" autocomplete=\"off\" />
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-email\">";
        // line 636
        echo (isset($context["entry_email"]) ? $context["entry_email"] : null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"email\" name=\"email\" value=\"\" placeholder=\"";
        // line 638
        echo (isset($context["entry_email"]) ? $context["entry_email"] : null);
        echo "\" id=\"input-email\" class=\"form-control\" autocomplete=\"off\" />
                </div>
              </div>
              
              <legend>";
        // line 642
        echo (isset($context["text_address"]) ? $context["text_address"] : null);
        echo "</legend>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-address-1\">";
        // line 644
        echo (isset($context["entry_address_1"]) ? $context["entry_address_1"] : null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"address_1\" value=\"\" placeholder=\"";
        // line 646
        echo (isset($context["entry_address_1"]) ? $context["entry_address_1"] : null);
        echo "\" id=\"input-address-1\" class=\"form-control\" autocomplete=\"off\" />
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-city\">";
        // line 650
        echo (isset($context["entry_city"]) ? $context["entry_city"] : null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"city\" value=\"\" placeholder=\"";
        // line 652
        echo (isset($context["entry_city"]) ? $context["entry_city"] : null);
        echo "\" id=\"input-city\" class=\"form-control\" autocomplete=\"off\" />
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-postcode\">";
        // line 656
        echo (isset($context["entry_postcode"]) ? $context["entry_postcode"] : null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"postcode\" value=\"\" placeholder=\"";
        // line 658
        echo (isset($context["entry_postcode"]) ? $context["entry_postcode"] : null);
        echo "\" id=\"input-postcode\" class=\"form-control\" autocomplete=\"off\" />
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-country\">";
        // line 662
        echo (isset($context["entry_country"]) ? $context["entry_country"] : null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"country_id\" id=\"input-country\" class=\"form-control\" autocomplete=\"off\">
                    <option value=\"\">";
        // line 665
        echo (isset($context["text_select"]) ? $context["text_select"] : null);
        echo "</option>
                    ";
        // line 666
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["countries"]) ? $context["countries"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
            // line 667
            echo "                    <option value=\"";
            echo $this->getAttribute($context["country"], "country_id", array());
            echo "\">";
            echo $this->getAttribute($context["country"], "name", array());
            echo "</option>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 669
        echo "                  </select>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-zone\">";
        // line 673
        echo (isset($context["entry_zone"]) ? $context["entry_zone"] : null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"zone_id\" id=\"input-zone\" class=\"form-control\" autocomplete=\"off\">
                  </select>
                </div>
              </div>
              <div class=\"text-center\">
                <button class=\"btn btn-default\" onclick=\"registerCustomer(\$(this));\"><strong>";
        // line 680
        echo (isset($context["text_register"]) ? $context["text_register"] : null);
        echo "</strong></button>
                <button class=\"btn btn-default\" onclick=\"registerCustomer(\$(this), true);\"><strong>";
        // line 681
        echo (isset($context["text_register_select"]) ? $context["text_register_select"] : null);
        echo "</strong></button>
              </div>
            </div>
          </div>
          <div class=\"modal-footer\">
            <button type=\"button\" class=\"btn btn-success pull-left\" id=\"addCustomer\">";
        // line 686
        echo (isset($context["text_add_customer"]) ? $context["text_add_customer"] : null);
        echo "</button>
            <button type=\"button\" class=\"btn btn-danger pull-left\" id=\"removeCustomer\">";
        // line 687
        echo (isset($context["text_remove"]) ? $context["text_remove"] : null);
        echo "</button>
            <button type=\"button\" class=\"btn btn-info\" data-dismiss=\"modal\">";
        // line 688
        echo (isset($context["button_close"]) ? $context["button_close"] : null);
        echo "</button>
          </div>
        </div>
      </div>
    </div>

    <div id=\"holdOrder\" class=\"modal fade\" role=\"dialog\" tabindex=\"-1\">
      <div class=\"modal-dialog modal-sm\">
        <div class=\"modal-content\">
          <div class=\"product-option-modal\" data-dismiss=\"modal\"><i class=\"fa fa-times\"></i></div>
          <div class=\"modal-header\">
            <h4 class=\"modal-title\">";
        // line 699
        echo (isset($context["heading_hold_note"]) ? $context["heading_hold_note"] : null);
        echo "</h4>
          </div>
          <div class=\"modal-body\">
            <textarea class=\"form-control\" placeholder=\"";
        // line 702
        echo (isset($context["entry_hold_note"]) ? $context["entry_hold_note"] : null);
        echo "\" id=\"holdNote\" rows=\"4\"></textarea>
          </div>
          <div class=\"modal-footer\">
            <button type=\"button\" class=\"btn btn-success pull-left\" onclick=\"holdOrder();\">";
        // line 705
        echo (isset($context["button_hold_order"]) ? $context["button_hold_order"] : null);
        echo "</button>
            <button type=\"button\" class=\"btn btn-info\" data-dismiss=\"modal\">";
        // line 706
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "</button>
          </div>
        </div>
      </div>
    </div>
    <span data-toggle=\"modal\" data-target=\"#productOptions\" id=\"buttonModal\"></span>
    <div id=\"productOptions\" class=\"modal fade\" role=\"dialog\" tabindex=\"-1\">
      <div class=\"modal-dialog\">
        <div class=\"modal-content\">
          <div class=\"product-option-modal\" data-dismiss=\"modal\"><i class=\"fa fa-times\"></i></div>
          <div class=\"modal-header\">
            <h4 class=\"modal-title\" id=\"global-modal-title\">";
        // line 717
        echo (isset($context["text_product_options"]) ? $context["text_product_options"] : null);
        echo "</h4>
          </div>
          <div class=\"modal-body color-black\" id=\"posProductOptions\">
          </div>
          <div class=\"modal-footer\">
            <button type=\"button\" class=\"btn btn-info\" data-dismiss=\"modal\">";
        // line 722
        echo (isset($context["button_close"]) ? $context["button_close"] : null);
        echo "</button>
          </div>
        </div>
      </div>
    </div>
    <span data-toggle=\"modal\" data-target=\"#productDetails\" id=\"buttonProductDetails\"></span>
    <div id=\"productDetails\" class=\"modal fade\" role=\"dialog\" tabindex=\"-1\">
      <div class=\"modal-dialog modal-sm\">
        <div class=\"modal-content\">
          <div class=\"product-option-modal\" data-dismiss=\"modal\"><i class=\"fa fa-times\"></i></div>
          <div class=\"modal-header\">
            <h4 class=\"modal-title\">";
        // line 733
        echo (isset($context["heading_product_detail"]) ? $context["heading_product_detail"] : null);
        echo "</h4>
          </div>
          <div class=\"modal-body color-black\">
            <img src=\"\" class=\"img-responsive\" style=\"margin: 0 auto\" id=\"detail-image\">
            <div class=\"table-responsive\">
              <table class=\"table table-hover margin-bottom-0\">
                <tbody>
                  <tr>
                    <td class=\"text-right\">";
        // line 741
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo "</td>
                    <td class=\"text-left\" id=\"productName\"></td>
                  </tr>
                  <tr>
                    <td class=\"text-right\">";
        // line 745
        echo (isset($context["entry_price"]) ? $context["entry_price"] : null);
        echo "</td>
                    <td class=\"text-left\" id=\"productPrice\"></td>
                  </tr>
                  <tr>
                    <td class=\"text-right\">";
        // line 749
        echo (isset($context["entry_item_left"]) ? $context["entry_item_left"] : null);
        echo "</td>
                    <td class=\"text-left\" id=\"productItem\"></td>
                  </tr>
                  <tr id=\"supplier-info\">
                    <td class=\"text-right\">";
        // line 753
        echo (isset($context["entry_suppliers"]) ? $context["entry_suppliers"] : null);
        echo ":</td>
                    <td class=\"text-left\" id=\"productSuppliers\"></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class=\"modal-footer\">
            <button type=\"button\" class=\"btn btn-info\" data-dismiss=\"modal\">";
        // line 761
        echo (isset($context["button_close"]) ? $context["button_close"] : null);
        echo "</button>
          </div>
        </div>
      </div>
    </div>
    <div id=\"categoryList\" class=\"modal fade\" role=\"dialog\" tabindex=\"-1\">
      <div class=\"modal-dialog modal-sm\">
        <div class=\"modal-content\">
          <div class=\"product-option-modal\" data-dismiss=\"modal\"><i class=\"fa fa-times\"></i></div>
          <div class=\"modal-header\">
            <h4 class=\"modal-title\">";
        // line 771
        echo (isset($context["heading_category"]) ? $context["heading_category"] : null);
        echo "</h4>
          </div>
          <div class=\"modal-body color-black\">
            <button class=\"btn btn-default\" onclick=\"showAllProducts()\">";
        // line 774
        echo (isset($context["button_show_product"]) ? $context["button_show_product"] : null);
        echo "</button>
          </div>
          <div class=\"modal-footer\">
            <button type=\"button\" class=\"btn btn-info\" data-dismiss=\"modal\">";
        // line 777
        echo (isset($context["button_close"]) ? $context["button_close"] : null);
        echo "</button>
          </div>
        </div>
      </div>
    </div>
    <div id=\"barcodeScan\" class=\"modal fade\" role=\"dialog\" tabindex=\"-1\">
      <div class=\"modal-dialog modal-sm\">
        <div class=\"modal-content\">
          <div class=\"product-option-modal\" data-dismiss=\"modal\"><i class=\"fa fa-times\"></i></div>
          <div class=\"modal-header\">
            <h4 class=\"modal-title\">";
        // line 787
        echo (isset($context["heading_scan_barcode"]) ? $context["heading_scan_barcode"] : null);
        echo "</h4>
          </div>
          <div class=\"modal-body color-black\">
            <input type=\"text\" id=\"bar-code\" placeholder=\"";
        // line 790
        echo (isset($context["entry_barcode"]) ? $context["entry_barcode"] : null);
        echo "\" class=\"form-control\">
          </div>
          <div class=\"modal-footer\">
            <button type=\"button\" class=\"btn btn-info\" data-dismiss=\"modal\">";
        // line 793
        echo (isset($context["button_close"]) ? $context["button_close"] : null);
        echo "</button>
          </div>
        </div>
      </div>
    </div>
    <div id=\"addDiscount\" class=\"modal fade\" role=\"dialog\" tabindex=\"-1\">
      <div class=\"modal-dialog modal-md\">
        <div class=\"modal-content\">
          <div class=\"product-option-modal\" data-dismiss=\"modal\"><i class=\"fa fa-times\"></i></div>
          <div class=\"modal-header\">
            <h4 class=\"modal-title\">";
        // line 803
        echo (isset($context["heading_discount"]) ? $context["heading_discount"] : null);
        echo "</h4>
          </div>
          <div class=\"modal-body color-black\">
            <div class=\"row\">
              <div class=\"col-sm-3 text-center\">
                <strong>Fixed</strong>
                <div class=\"input-group input-group2\">
                  ";
        // line 810
        if (((isset($context["symbol_position"]) ? $context["symbol_position"] : null) == "L")) {
            // line 811
            echo "                  <span class=\"input-group-addon currency\"><strong>";
            echo (isset($context["currency_code"]) ? $context["currency_code"] : null);
            echo "</strong></span>
                  ";
        } else {
            // line 813
            echo "                  <span class=\"input-group-addon currency\" style=\"display: none;\"><strong>";
            echo (isset($context["currency_code"]) ? $context["currency_code"] : null);
            echo "</strong></span>
                  ";
        }
        // line 815
        echo "                  <input type=\"text\" id=\"fixedDisc\" value=\"0\" placeholder=\"";
        echo (isset($context["entry_fixed"]) ? $context["entry_fixed"] : null);
        echo "\" class=\"form-control\" onkeypress=\"return validate(event, this)\">
                  ";
        // line 816
        if (((isset($context["symbol_position"]) ? $context["symbol_position"] : null) == "R")) {
            // line 817
            echo "                  <span class=\"input-group-addon currency\"><strong>";
            echo (isset($context["currency_code"]) ? $context["currency_code"] : null);
            echo "</strong></span>
                  ";
        } else {
            // line 819
            echo "                  <span class=\"input-group-addon currency\" style=\"display: none;\"><strong>";
            echo (isset($context["currency_code"]) ? $context["currency_code"] : null);
            echo "</strong></span>
                  ";
        }
        // line 821
        echo "                </div>
              </div>
              <div class=\"col-sm-1 text-center\" style=\"margin: 28px 0;\">
                <i class=\"fa fa-plus\"></i>
              </div>
              <div class=\"col-sm-3 text-center\">
                <strong>";
        // line 827
        echo (isset($context["entry_percent"]) ? $context["entry_percent"] : null);
        echo "</strong>
                <div class=\"input-group\">
                  <input type=\"text\" id=\"percentDisc\" value=\"0\" placeholder=\"";
        // line 829
        echo (isset($context["entry_percent"]) ? $context["entry_percent"] : null);
        echo "\" class=\"form-control\" onkeypress=\"return validate(event, this)\" style=\"border-radius: 0;\">
                  <span class=\"input-group-addon\"><strong>%</strong></span>
                </div>
              </div>
              <div class=\"col-sm-1 text-center\" style=\"margin: 28px 0; font-size: 20px;\">
                <strong>=</strong>
              </div>
              <div class=\"col-sm-4 text-center\">
                <strong>";
        // line 837
        echo (isset($context["text_total"]) ? $context["text_total"] : null);
        echo "</strong>
                <div class=\"input-group input-group3\" style=\"margin-top: 3px;\">
                  ";
        // line 839
        if (((isset($context["symbol_position"]) ? $context["symbol_position"] : null) == "L")) {
            // line 840
            echo "                  <span class=\"input-group-addon currency\"><strong>";
            echo (isset($context["currency_code"]) ? $context["currency_code"] : null);
            echo "</strong></span>
                  ";
        } else {
            // line 842
            echo "                  <span class=\"input-group-addon currency\" style=\"display: none;\"><strong>";
            echo (isset($context["currency_code"]) ? $context["currency_code"] : null);
            echo "</strong></span>
                  ";
        }
        // line 844
        echo "                  <span class=\"input-group-addon\" style=\"max-width: 140px; overflow: hidden;\"><strong id=\"total-discount\">0.00</strong></span>
                  ";
        // line 845
        if (((isset($context["symbol_position"]) ? $context["symbol_position"] : null) == "R")) {
            // line 846
            echo "                  <span class=\"input-group-addon currency\"><strong>";
            echo (isset($context["currency_code"]) ? $context["currency_code"] : null);
            echo "</strong></span>
                  ";
        } else {
            // line 848
            echo "                  <span class=\"input-group-addon currency\" style=\"display: none;\"><strong>";
            echo (isset($context["currency_code"]) ? $context["currency_code"] : null);
            echo "</strong></span>
                  ";
        }
        // line 850
        echo "                </div>
              </div>
              <div class=\"col-sm-12\">
                <label class=\"col-sm-3\" for=\"input-discname\">";
        // line 853
        echo (isset($context["entry_discount"]) ? $context["entry_discount"] : null);
        echo "</label>
                <div class=\"col-sm-9\">
                  <input type=\"text\" class=\"form-control\" id=\"input-discname\" placeholder=\"";
        // line 855
        echo (isset($context["entry_discount"]) ? $context["entry_discount"] : null);
        echo "\" value=\"";
        echo (isset($context["button_discount"]) ? $context["button_discount"] : null);
        echo "\">
                </div>
              </div>
            </div>
            <div class=\"col-sm-12 text-center\" style=\"float: none;\">
              <button type=\"button\" class=\"btn buttons-sp\" id=\"addDiscountbtn\">";
        // line 860
        echo (isset($context["button_apply"]) ? $context["button_apply"] : null);
        echo "</button>
              <button type=\"button\" class=\"btn buttons-sp\" id=\"removeDiscount\" style=\"background-color: #ff6666;\">";
        // line 861
        echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
        echo "</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id=\"addCoupon\" class=\"modal fade\" role=\"dialog\" tabindex=\"-1\">
      <div class=\"modal-dialog modal-sm\">
        <div class=\"modal-content\">
          <div class=\"product-option-modal\" data-dismiss=\"modal\"><i class=\"fa fa-times\"></i></div>
          <div class=\"modal-header\">
            <h4 class=\"modal-title\">";
        // line 872
        echo (isset($context["heading_coupon"]) ? $context["heading_coupon"] : null);
        echo "</h4>
          </div>
          <div class=\"modal-body color-black\">
            <div class=\"form-group\">
              <input type=\"text\" id=\"coupon-code\" class=\"form-control\" placeholder=\"";
        // line 876
        echo (isset($context["entry_coupon"]) ? $context["entry_coupon"] : null);
        echo "\">
            </div>
            <div class=\"col-sm-12 text-center\" style=\"float: none;\">
              <button type=\"button\" class=\"btn buttons-sp\" id=\"addCouponbtn\">";
        // line 879
        echo (isset($context["button_apply"]) ? $context["button_apply"] : null);
        echo "</button>
              <button type=\"button\" class=\"btn buttons-sp\" id=\"removeCoupon\" style=\"background-color: #ff6666;\">";
        // line 880
        echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
        echo "</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id=\"updatePrice\" class=\"modal fade\" role=\"dialog\" tabindex=\"-1\">
      <div class=\"modal-dialog modal-sm\">
        <div class=\"modal-content\">
          <div class=\"product-option-modal\" data-dismiss=\"modal\"><i class=\"fa fa-times\"></i></div>
          <div class=\"modal-header\">
            <h4 class=\"modal-title\">";
        // line 891
        echo (isset($context["heading_price_update"]) ? $context["heading_price_update"] : null);
        echo "</h4>
          </div>
          <div class=\"modal-body color-black\">
            <div class=\"form-group\">
              <label id=\"update-label\"></label>
              <input type=\"text\" id=\"update-price\" class=\"form-control\" placeholder=\"";
        // line 896
        echo (isset($context["entry_updated_price"]) ? $context["entry_updated_price"] : null);
        echo "\" onkeypress=\"return validate(event, this)\">
              <input type=\"hidden\" id=\"cart-index\">
            </div>
            <div class=\"col-sm-12 text-center\" style=\"float: none;\">
              <button type=\"button\" class=\"btn buttons-sp\" id=\"updatePricebtn\">";
        // line 900
        echo (isset($context["button_apply"]) ? $context["button_apply"] : null);
        echo "</button>
              <button type=\"button\" class=\"btn buttons-sp\" id=\"cancelPriceUp\" style=\"background-color: #ff6666;\">";
        // line 901
        echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
        echo "</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id=\"addProduct\" class=\"modal fade\" role=\"dialog\" tabindex=\"-1\">
      <div class=\"modal-dialog modal-md\">
        <div class=\"modal-content\">
          <div class=\"product-option-modal\" data-dismiss=\"modal\"><i class=\"fa fa-times\"></i></div>
          <div class=\"modal-header\">
            <h4 class=\"modal-title\">";
        // line 912
        echo (isset($context["heading_add_product"]) ? $context["heading_add_product"] : null);
        echo "</h4>
          </div>
          <div class=\"modal-body color-black form-horizontal\">
            <div class=\"form-group\">
              <label class=\"col-sm-3 control-label\">";
        // line 916
        echo (isset($context["entry_product_name"]) ? $context["entry_product_name"] : null);
        echo "</label>
              <div class=\"col-sm-9\">
                <input type=\"text\" name=\"product_name\" class=\"form-control\" placeholder=\"";
        // line 918
        echo (isset($context["entry_product_name"]) ? $context["entry_product_name"] : null);
        echo "\">
              </div>
            </div>
            <div class=\"form-group\">
              <label class=\"col-sm-3 control-label\">";
        // line 922
        echo (isset($context["entry_product_price"]) ? $context["entry_product_price"] : null);
        echo "</label>
              <div class=\"col-sm-9\">
                <input type=\"text\" name=\"product_price\" class=\"form-control\" placeholder=\"";
        // line 924
        echo (isset($context["entry_product_price"]) ? $context["entry_product_price"] : null);
        echo "\" onkeypress=\"return validate(event, this)\">
              </div>
            </div>
            <div class=\"form-group\">
              <label class=\"col-sm-3 control-label\">";
        // line 928
        echo (isset($context["entry_product_quantity"]) ? $context["entry_product_quantity"] : null);
        echo "</label>
              <div class=\"col-sm-9\">
                <input type=\"text\" name=\"product_quantity\" class=\"form-control\" placeholder=\"";
        // line 930
        echo (isset($context["entry_product_quantity"]) ? $context["entry_product_quantity"] : null);
        echo "\" onkeypress=\"return validate(event, this, true)\">
              </div>
            </div>
          </div>
          <div class=\"modal-footer color-black\">
            <div class=\"col-sm-12 text-center\" style=\"float: none;\">
              <button type=\"button\" class=\"btn buttons-sp\" id=\"addProductbtn\">";
        // line 936
        echo (isset($context["button_add"]) ? $context["button_add"] : null);
        echo "</button>
              <button type=\"button\" class=\"btn buttons-sp\" data-dismiss=\"modal\" style=\"background-color: #ff6666;\">";
        // line 937
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class=\"modal\" id=\"loader\">
      <div class=\"cp-spinner cp-eclipse\"></div>
      <div class=\"progress\"><div class=\"progress-bar progress-bar-dan\"></div></div>
      <div id=\"loading-text\"></div>
      <div id=\"error-text\"></div>
    </div>
    <div class=\"posProductBlock\"><img src=\"";
        // line 949
        echo (isset($context["no_image"]) ? $context["no_image"] : null);
        echo "\" width=\"50\" height=\"50\"></div>
    <div id=\"screen-div\" onclick=\"toggleFullScreen();\" title=\"Toggle Screen\" data-toggle=\"tooltip\">
      <img src=\"";
        // line 951
        echo (isset($context["screen_image"]) ? $context["screen_image"] : null);
        echo "\">
    </div>
  </div>
  <div class=\"hide\" id=\"printBill\" style=\"color:#000 !important;\">
    <div class=\"container thermal\" style=\"text-align: center; max-width: 630px; font-weight:400\">
      <div class=\"thermal\" style=\"margin: 0 auto;\">
        ";
        // line 957
        if (((isset($context["show_store_logo"]) ? $context["show_store_logo"] : null) && (isset($context["store_logo"]) ? $context["store_logo"] : null))) {
            // line 958
            echo "        <img src=\"";
            echo (isset($context["store_logo"]) ? $context["store_logo"] : null);
            echo "\" title=\"";
            echo (isset($context["store_name"]) ? $context["store_name"] : null);
            echo "\" alt=\"";
            echo (isset($context["store_name"]) ? $context["store_name"] : null);
            echo "\" class=\"img-responsive\" width=\"200\" style=\"margin: 0 auto;\"><br>
        ";
        }
        // line 960
        echo "        ";
        if ((isset($context["show_store_name"]) ? $context["show_store_name"] : null)) {
            // line 961
            echo "        <span style=\"font-size: 14px;\">";
            echo (isset($context["store_name"]) ? $context["store_name"] : null);
            echo "</span><br>
        ";
        }
        // line 963
        echo "        ";
        if ((isset($context["show_store_address"]) ? $context["show_store_address"] : null)) {
            // line 964
            echo "        <span class=\"font-store\">";
            echo (isset($context["store_address"]) ? $context["store_address"] : null);
            echo "</span><br>
        ";
        }
        // line 966
        echo "        ";
        if ((isset($context["store_detail"]) ? $context["store_detail"] : null)) {
            // line 967
            echo "        <span class=\"font-store\">";
            echo (isset($context["store_detail"]) ? $context["store_detail"] : null);
            echo "</span><br>
        ";
        }
        // line 969
        echo "        ";
        if ((isset($context["show_order_date"]) ? $context["show_order_date"] : null)) {
            // line 970
            echo "        ";
            echo (isset($context["text_date"]) ? $context["text_date"] : null);
            echo " <span class=\"order-date\"></span>
        ";
        }
        // line 972
        echo "        ";
        if ((isset($context["show_order_time"]) ? $context["show_order_time"] : null)) {
            // line 973
            echo "        ";
            echo (isset($context["text_time"]) ? $context["text_time"] : null);
            echo " <span class=\"order-time\"></span><br>
        ";
        }
        // line 975
        echo "        ";
        if ((isset($context["show_order_id"]) ? $context["show_order_id"] : null)) {
            // line 976
            echo "        <span class=\"order-txn\">";
            echo (isset($context["text_order_id"]) ? $context["text_order_id"] : null);
            echo "</span> #<span class=\"oid\"></span>
        ";
        }
        // line 978
        echo "        ";
        if ((isset($context["show_cashier_name"]) ? $context["show_cashier_name"] : null)) {
            // line 979
            echo "        ";
            echo (isset($context["text_cashier"]) ? $context["text_cashier"] : null);
            echo " <span id=\"cashier-name\"></span>
        ";
        }
        // line 981
        echo "        ";
        if ((isset($context["show_customer_name"]) ? $context["show_customer_name"] : null)) {
            // line 982
            echo "        <br />
        ";
            // line 983
            echo (isset($context["text_customer"]) ? $context["text_customer"] : null);
            echo " <span id=\"customer-name-in\"></span>
        ";
        }
        // line 985
        echo "      </div>
      <br>
      <div class=\"thermal\">
        ";
        // line 988
        if ((isset($context["show_shipping_mode"]) ? $context["show_shipping_mode"] : null)) {
            // line 989
            echo "        <span>";
            echo (isset($context["text_shipping_mode"]) ? $context["text_shipping_mode"] : null);
            echo ": </span><span class=\"oshipping\">";
            echo (isset($context["text_pickup"]) ? $context["text_pickup"] : null);
            echo "</span><br>
        ";
        }
        // line 991
        echo "        ";
        if ((isset($context["show_payment_mode"]) ? $context["show_payment_mode"] : null)) {
            // line 992
            echo "        <span>";
            echo (isset($context["text_payment_mode"]) ? $context["text_payment_mode"] : null);
            echo ": </span><span class=\"opayment\"></span>
        ";
        }
        // line 994
        echo "      </div>
      <div class=\"table thermal\">
        <table style=\"border-collapse: separate; border-spacing: 5px; margin: 0 auto; width: 95%;\">
          <thead>
            <tr>
              <td style=\"text-align:left;\">";
        // line 999
        echo (isset($context["column_product"]) ? $context["column_product"] : null);
        echo "</td>
              <td>";
        // line 1000
        echo (isset($context["column_quantity"]) ? $context["column_quantity"] : null);
        echo "</td>
              <td>";
        // line 1001
        echo (isset($context["column_price"]) ? $context["column_price"] : null);
        echo "</td>
              <td style=\"text-align:right;\">";
        // line 1002
        echo (isset($context["column_amount"]) ? $context["column_amount"] : null);
        echo "</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td style=\"text-align:left;\">-------------";
        // line 1007
        if (((isset($context["paper_size"]) ? $context["paper_size"] : null) == "80mm")) {
            echo "-------";
        }
        echo "</td>
              <td>-----";
        // line 1008
        if (((isset($context["paper_size"]) ? $context["paper_size"] : null) == "80mm")) {
            echo "----";
        }
        echo "</td>
              <td>-------------</td>
              <td style=\"text-align:right;\">--------------</td>
            </tr>
          </tbody>
          <tbody id=\"receiptProducts\">
          </tbody>
          <tbody>
            <tr>
              <td colspan=\"4\">----------------------------------------------------";
        // line 1017
        if (((isset($context["paper_size"]) ? $context["paper_size"] : null) == "80mm")) {
            echo "------------------";
        }
        if (((isset($context["paper_size"]) ? $context["paper_size"] : null) == "595px")) {
            echo "-------------------------------------------------------------------------------------------";
        }
        echo "</td>
            </tr>
          </tbody>
          <tbody id=\"print-totals\">
            <tr>
              <td></td>
              <td></td>
              <td>";
        // line 1024
        echo (isset($context["text_subtotal"]) ? $context["text_subtotal"] : null);
        echo "</td>
              <td style=\"text-align:left;\">00.00</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td id=\"total-quantity-text\"></td>
              <td id=\"total-quantity\" style=\"font-weight: bold;\"></td>
              <td>";
        // line 1032
        echo (isset($context["text_grandtotal"]) ? $context["text_grandtotal"] : null);
        echo "</td>
              <td style=\"text-align: right;\"><span class=\"oTotal\">00.00</span></td>
            </tr>
          </tbody>
        </table>
        ";
        // line 1037
        if ((isset($context["show_note"]) ? $context["show_note"] : null)) {
            // line 1038
            echo "        <div class=\"text-left\">
          <strong>";
            // line 1039
            echo (isset($context["text_note"]) ? $context["text_note"] : null);
            echo ": </strong><span class=\"onote\"></span>
        </div>
        ";
        }
        // line 1042
        echo "        <span>";
        echo (isset($context["text_nice_day"]) ? $context["text_nice_day"] : null);
        echo "</span><br>
        <span>";
        // line 1043
        echo (isset($context["text_thank_you"]) ? $context["text_thank_you"] : null);
        echo "</span>
      </div>
    </div>
  </div>

  <noscript>
    <div class=\"no-js\">";
        // line 1049
        echo (isset($context["text_no_js"]) ? $context["text_no_js"] : null);
        echo "</div>
  </noscript>
  <script language=\"JavaScript\">
  var direction = '";
        // line 1052
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "';
    window.onbeforeunload = confirmExit;
    function confirmExit() {
      return \"";
        // line 1055
        echo (isset($context["text_close_warning"]) ? $context["text_close_warning"] : null);
        echo "\";
    }
  </script>
  <script type=\"text/javascript\">
  var currency              = \"";
        // line 1059
        echo (isset($context["currency"]) ? $context["currency"] : null);
        echo "\",
  entry_slip_id            = \"";
        // line 1060
        echo (isset($context["entry_slip_id"]) ? $context["entry_slip_id"] : null);
        echo "\", 
  tax_sort_order            = ";
        // line 1061
        echo (isset($context["tax_sort_order"]) ? $context["tax_sort_order"] : null);
        echo ",
  entry_changed             = '";
        // line 1062
        echo (isset($context["entry_changed"]) ? $context["entry_changed"] : null);
        echo "',
  discount_sort_order       = ";
        // line 1063
        echo (isset($context["discount_sort_order"]) ? $context["discount_sort_order"] : null);
        echo ",
  currency_code             = \"";
        // line 1064
        echo (isset($context["currency_code"]) ? $context["currency_code"] : null);
        echo "\",
  symbol_position           = \"";
        // line 1065
        echo (isset($context["symbol_position"]) ? $context["symbol_position"] : null);
        echo "\",
  user_login                = ";
        // line 1066
        echo (isset($context["user_login"]) ? $context["user_login"] : null);
        echo ",
  tax_status                = ";
        // line 1067
        echo (isset($context["tax_status"]) ? $context["tax_status"] : null);
        echo ",
  message_error_credentials = \"";
        // line 1068
        echo (isset($context["error_credentials"]) ? $context["error_credentials"] : null);
        echo "\",
  entry_price               = \"";
        // line 1069
        echo (isset($context["entry_price"]) ? $context["entry_price"] : null);
        echo "\",
  text_loading_products     = \"";
        // line 1070
        echo (isset($context["text_loading_products"]) ? $context["text_loading_products"] : null);
        echo "\",
  error_load_products       = \"";
        // line 1071
        echo (isset($context["error_load_products"]) ? $context["error_load_products"] : null);
        echo "\",
  text_loading_populars     = \"";
        // line 1072
        echo (isset($context["text_loading_populars"]) ? $context["text_loading_populars"] : null);
        echo "\",
  error_load_populars       = \"";
        // line 1073
        echo (isset($context["error_load_populars"]) ? $context["error_load_populars"] : null);
        echo "\",
  text_loading_orders       = \"";
        // line 1074
        echo (isset($context["text_loading_orders"]) ? $context["text_loading_orders"] : null);
        echo "\",
  error_load_orders         = \"";
        // line 1075
        echo (isset($context["error_load_orders"]) ? $context["error_load_orders"] : null);
        echo "\",
  text_loading_customers    = \"";
        // line 1076
        echo (isset($context["text_loading_customers"]) ? $context["text_loading_customers"] : null);
        echo "\",
  error_load_customers      = \"";
        // line 1077
        echo (isset($context["error_load_customers"]) ? $context["error_load_customers"] : null);
        echo "\",
  text_loading_categories   = \"";
        // line 1078
        echo (isset($context["text_loading_categories"]) ? $context["text_loading_categories"] : null);
        echo "\",
  error_load_categories     = \"";
        // line 1079
        echo (isset($context["error_load_categories"]) ? $context["error_load_categories"] : null);
        echo "\",
  text_select               = \"";
        // line 1080
        echo (isset($context["text_select"]) ? $context["text_select"] : null);
        echo "\",
  text_none                 = \"";
        // line 1081
        echo (isset($context["text_none"]) ? $context["text_none"] : null);
        echo "\",
  text_loading              = \"";
        // line 1082
        echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
        echo "\",
  button_upload             = \"";
        // line 1083
        echo (isset($context["button_upload"]) ? $context["button_upload"] : null);
        echo "\",
  button_cart               = \"";
        // line 1084
        echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
        echo "\",
  text_product_options      = \"";
        // line 1085
        echo (isset($context["text_product_options"]) ? $context["text_product_options"] : null);
        echo "\",
  error_keyword             = \"";
        // line 1086
        echo (isset($context["error_keyword"]) ? $context["error_keyword"] : null);
        echo "\",
  error_products            = \"";
        // line 1087
        echo (isset($context["error_products"]) ? $context["error_products"] : null);
        echo "\",
  text_online_mode          = \"";
        // line 1088
        echo (isset($context["text_online_mode"]) ? $context["text_online_mode"] : null);
        echo "\",
  text_online               = \"";
        // line 1089
        echo (isset($context["text_online"]) ? $context["text_online"] : null);
        echo "\",
  error_enter_online        = \"";
        // line 1090
        echo (isset($context["error_enter_online"]) ? $context["error_enter_online"] : null);
        echo "\",
  text_offline_mode         = \"";
        // line 1091
        echo (isset($context["text_offline_mode"]) ? $context["text_offline_mode"] : null);
        echo "\",
  text_offline              = \"";
        // line 1092
        echo (isset($context["text_offline"]) ? $context["text_offline"] : null);
        echo "\",
  error_no_category_product = \"";
        // line 1093
        echo (isset($context["error_no_category_product"]) ? $context["error_no_category_product"] : null);
        echo "\",
  error_no_customer         = \"";
        // line 1094
        echo (isset($context["error_no_customer"]) ? $context["error_no_customer"] : null);
        echo "\",
  text_select_customer      = \"";
        // line 1095
        echo (isset($context["text_select_customer"]) ? $context["text_select_customer"] : null);
        echo "\",
  error_customer_add        = \"";
        // line 1096
        echo (isset($context["error_add_customer"]) ? $context["error_add_customer"] : null);
        echo "\",
  text_remove_customer      = \"";
        // line 1097
        echo (isset($context["text_remove_customer"]) ? $context["text_remove_customer"] : null);
        echo "\",
  error_checkout            = \"";
        // line 1098
        echo (isset($context["error_checkout"]) ? $context["error_checkout"] : null);
        echo "\",
  text_balance_due          = \"";
        // line 1099
        echo (isset($context["text_balance_due"]) ? $context["text_balance_due"] : null);
        echo "\",
  text_order_success        = \"";
        // line 1100
        echo (isset($context["text_order_success"]) ? $context["text_order_success"] : null);
        echo "\",
  text_customer_select      = \"";
        // line 1101
        echo (isset($context["text_customer_select"]) ? $context["text_customer_select"] : null);
        echo "\",
  guest_name                = \"";
        // line 1102
        echo (isset($context["guest_name"]) ? $context["guest_name"] : null);
        echo "\",
  text_item_detail          = \"";
        // line 1103
        echo (isset($context["text_item_details"]) ? $context["text_item_details"] : null);
        echo "\",
  text_sync_order           = \"";
        // line 1104
        echo (isset($context["text_sync_orders"]) ? $context["text_sync_orders"] : null);
        echo "\",
  text_no_orders            = \"";
        // line 1105
        echo (isset($context["text_no_orders"]) ? $context["text_no_orders"] : null);
        echo "\",
  error_sync_orders         = \"";
        // line 1106
        echo (isset($context["error_sync_orders"]) ? $context["error_sync_orders"] : null);
        echo "\",
  text_another_cart         = \"";
        // line 1107
        echo (isset($context["text_select_cart"]) ? $context["text_select_cart"] : null);
        echo "\",
  text_cart_deleted         = \"";
        // line 1108
        echo (isset($context["text_cart_deleted"]) ? $context["text_cart_deleted"] : null);
        echo "\",
  text_current_deleted      = \"";
        // line 1109
        echo (isset($context["text_current_deleted"]) ? $context["text_current_deleted"] : null);
        echo "\",
  text_cart_empty           = \"";
        // line 1110
        echo (isset($context["text_cart_empty"]) ? $context["text_cart_empty"] : null);
        echo "\",
  text_empty_cart           = \"";
        // line 1111
        echo (isset($context["text_empty_cart"]) ? $context["text_empty_cart"] : null);
        echo "\",
  text_cart_add             = \"";
        // line 1112
        echo (isset($context["text_cart_add"]) ? $context["text_cart_add"] : null);
        echo "\",
  text_option_required      = \"";
        // line 1113
        echo (isset($context["text_option_required"]) ? $context["text_option_required"] : null);
        echo "\",
  text_no_quantity          = \"";
        // line 1114
        echo (isset($context["text_no_quantity"]) ? $context["text_no_quantity"] : null);
        echo "\",
  text_product_added        = \"";
        // line 1115
        echo (isset($context["text_product_added"]) ? $context["text_product_added"] : null);
        echo "\",
  text_product_not_added    = \"";
        // line 1116
        echo (isset($context["text_product_not_added"]) ? $context["text_product_not_added"] : null);
        echo "\",
  text_product_removed      = \"";
        // line 1117
        echo (isset($context["text_product_removed"]) ? $context["text_product_removed"] : null);
        echo "\",
  cash_payment_title        = \"";
        // line 1118
        echo (isset($context["cash_payment_title"]) ? $context["cash_payment_title"] : null);
        echo "\",
  text_order_id             = \"";
        // line 1119
        echo (isset($context["text_order_id"]) ? $context["text_order_id"] : null);
        echo "\",
  text_all_products         = \"";
        // line 1120
        echo (isset($context["text_all_products"]) ? $context["text_all_products"] : null);
        echo "\",
  text_search               = \"";
        // line 1121
        echo (isset($context["text_search"]) ? $context["text_search"] : null);
        echo "\",
  text_option_notifier      = \"";
        // line 1122
        echo (isset($context["text_option_notifier"]) ? $context["text_option_notifier"] : null);
        echo "\",
  show_lowstock_prod        = \"";
        // line 1123
        echo (isset($context["show_lowstock_prod"]) ? $context["show_lowstock_prod"] : null);
        echo "\",
  low_stock                 = \"";
        // line 1124
        echo (isset($context["low_stock"]) ? $context["low_stock"] : null);
        echo "\",
  text_low_stock            = \"";
        // line 1125
        echo (isset($context["text_low_stock"]) ? $context["text_low_stock"] : null);
        echo "\",
  text_special_price        = \"";
        // line 1126
        echo (isset($context["text_special_price"]) ? $context["text_special_price"] : null);
        echo "\",
  text_empty_hold           = \"";
        // line 1127
        echo (isset($context["text_empty_hold"]) ? $context["text_empty_hold"] : null);
        echo "\",
  text_success_logout       = \"";
        // line 1128
        echo (isset($context["text_success_logout"]) ? $context["text_success_logout"] : null);
        echo "\",
  text_cust_add_select      = \"";
        // line 1129
        echo (isset($context["text_cust_add_select"]) ? $context["text_cust_add_select"] : null);
        echo "\",
  text_register             = \"";
        // line 1130
        echo (isset($context["text_register"]) ? $context["text_register"] : null);
        echo "\",
  text_register_select      = \"";
        // line 1131
        echo (isset($context["text_register_select"]) ? $context["text_register_select"] : null);
        echo "\",
  error_save_setting        = \"";
        // line 1132
        echo (isset($context["error_save_setting"]) ? $context["error_save_setting"] : null);
        echo "\",
  base_pos                  = \"";
        // line 1133
        echo (isset($context["base_pos"]) ? $context["base_pos"] : null);
        echo "\",
  text_tendered_confirm     = \"";
        // line 1134
        echo (isset($context["text_tendered_confirm"]) ? $context["text_tendered_confirm"] : null);
        echo "\",
  text_card_confirm         = \"";
        // line 1135
        echo (isset($context["text_card_confirm"]) ? $context["text_card_confirm"] : null);
        echo "\",
  text_product_success      = \"";
        // line 1136
        echo (isset($context["text_product_success"]) ? $context["text_product_success"] : null);
        echo "\",
  error_product_name        = \"";
        // line 1137
        echo (isset($context["error_product_name"]) ? $context["error_product_name"] : null);
        echo "\",
  error_product_price       = \"";
        // line 1138
        echo (isset($context["error_product_price"]) ? $context["error_product_price"] : null);
        echo "\",
  error_product_quant       = \"";
        // line 1139
        echo (isset($context["error_product_quant"]) ? $context["error_product_quant"] : null);
        echo "\",
  error_request_offline     = \"";
        // line 1140
        echo (isset($context["error_request_offline"]) ? $context["error_request_offline"] : null);
        echo "\",
  error_supplier_range      = \"";
        // line 1141
        echo (isset($context["error_supplier_range"]) ? $context["error_supplier_range"] : null);
        echo "\",
  text_no_quantity_added    = \"";
        // line 1142
        echo (isset($context["text_no_quantity_added"]) ? $context["text_no_quantity_added"] : null);
        echo "\",
  error_select_product      = \"";
        // line 1143
        echo (isset($context["error_select_product"]) ? $context["error_select_product"] : null);
        echo "\",
  text_sure                 = \"";
        // line 1144
        echo (isset($context["text_sure"]) ? $context["text_sure"] : null);
        echo "\",
  text_success_price_up     = \"";
        // line 1145
        echo (isset($context["text_success_price_up"]) ? $context["text_success_price_up"] : null);
        echo "\",
  text_price_remove         = \"";
        // line 1146
        echo (isset($context["text_price_remove"]) ? $context["text_price_remove"] : null);
        echo "\",
  text_success              = \"";
        // line 1147
        echo (isset($context["text_success"]) ? $context["text_success"] : null);
        echo "\",
  text_warning              = \"";
        // line 1148
        echo (isset($context["text_warning"]) ? $context["text_warning"] : null);
        echo "\",
  error_warning             = \"";
        // line 1149
        echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
        echo "\",
  text_coupon_remove        = \"";
        // line 1150
        echo (isset($context["text_coupon_remove"]) ? $context["text_coupon_remove"] : null);
        echo "\",
  text_no_product           = \"";
        // line 1151
        echo (isset($context["text_no_product"]) ? $context["text_no_product"] : null);
        echo "\",
  text_success_add_disc     = \"";
        // line 1152
        echo (isset($context["text_success_add_disc"]) ? $context["text_success_add_disc"] : null);
        echo "\",
  text_success_rem_disc     = \"";
        // line 1153
        echo (isset($context["text_success_rem_disc"]) ? $context["text_success_rem_disc"] : null);
        echo "\",
  error_cart_discount       = \"";
        // line 1154
        echo (isset($context["error_cart_discount"]) ? $context["error_cart_discount"] : null);
        echo "\",
  error_coupon_offline      = \"";
        // line 1155
        echo (isset($context["error_coupon_offline"]) ? $context["error_coupon_offline"] : null);
        echo "\",
  error_supplier            = \"";
        // line 1156
        echo (isset($context["error_supplier"]) ? $context["error_supplier"] : null);
        echo "\",
  error_no_supplier         = \"";
        // line 1157
        echo (isset($context["error_no_supplier"]) ? $context["error_no_supplier"] : null);
        echo "\",
  heading_price_update      = \"";
        // line 1158
        echo (isset($context["heading_price_update"]) ? $context["heading_price_update"] : null);
        echo "\",
  button_product_info       = \"";
        // line 1159
        echo (isset($context["button_product_info"]) ? $context["button_product_info"] : null);
        echo "\",
  error_mobile_view         = \"";
        // line 1160
        echo (isset($context["error_mobile_view"]) ? $context["error_mobile_view"] : null);
        echo "\",
  cashier                   = \"";
        // line 1161
        echo (isset($context["name"]) ? $context["name"] : null);
        echo "\",
  error_script              = \"";
        // line 1162
        echo (isset($context["error_script"]) ? $context["error_script"] : null);
        echo "\",
  error_price               = \"";
        // line 1163
        echo (isset($context["error_price"]) ? $context["error_price"] : null);
        echo "\",
  error_option_date         = \"";
        // line 1164
        echo (isset($context["error_option_date"]) ? $context["error_option_date"] : null);
        echo "\",
  // POS Update Code
  home_delivery_title       = \"";
        // line 1166
        echo (isset($context["home_delivery_title"]) ? $context["home_delivery_title"] : null);
        echo "\",
  text_credit_applied       = \"";
        // line 1167
        echo (isset($context["text_credit_applied"]) ? $context["text_credit_applied"] : null);
        echo "\",
  text_credit_removed       = \"";
        // line 1168
        echo (isset($context["text_credit_removed"]) ? $context["text_credit_removed"] : null);
        echo "\",
  text_pickup               = \"";
        // line 1169
        echo (isset($context["text_pickup"]) ? $context["text_pickup"] : null);
        echo "\",
  credit_status             = \"";
        // line 1170
        echo (isset($context["credit_status"]) ? $context["credit_status"] : null);
        echo "\",
  heading_return            = \"";
        // line 1171
        echo (isset($context["heading_return"]) ? $context["heading_return"] : null);
        echo "\",
  delivery_max              = \"";
        // line 1172
        echo (isset($context["delivery_max"]) ? $context["delivery_max"] : null);
        echo "\",
  text_product_info         = \"";
        // line 1173
        echo (isset($context["text_product_info"]) ? $context["text_product_info"] : null);
        echo "\",
  text_order_info           = \"";
        // line 1174
        echo (isset($context["text_order_info"]) ? $context["text_order_info"] : null);
        echo "\",
  text_loading_returns      = \"";
        // line 1175
        echo (isset($context["text_loading_returns"]) ? $context["text_loading_returns"] : null);
        echo "\",
  text_total                = \"";
        // line 1176
        echo (isset($context["text_total"]) ? $context["text_total"] : null);
        echo "\",
  text_return_details       = \"";
        // line 1177
        echo (isset($context["text_return_details"]) ? $context["text_return_details"] : null);
        echo "\",
  text_product_detail       = \"";
        // line 1178
        echo (isset($context["heading_product_detail"]) ? $context["heading_product_detail"] : null);
        echo "\",
  text_return_id            = \"";
        // line 1179
        echo (isset($context["text_return_id"]) ? $context["text_return_id"] : null);
        echo "\",
  text_no                   = \"";
        // line 1180
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "\",
  text_yes                  = \"";
        // line 1181
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "\",
  text_order_date           = \"";
        // line 1182
        echo (isset($context["text_order_date"]) ? $context["text_order_date"] : null);
        echo "\",
  text_no_credit            = \"";
        // line 1183
        echo (isset($context["text_no_credit"]) ? $context["text_no_credit"] : null);
        echo "\",
  text_return_date          = \"";
        // line 1184
        echo (isset($context["text_return_date"]) ? $context["text_return_date"] : null);
        echo "\",
  text_return_status        = \"";
        // line 1185
        echo (isset($context["text_return_status"]) ? $context["text_return_status"] : null);
        echo "\",
  text_comment              = \"";
        // line 1186
        echo (isset($context["text_comment"]) ? $context["text_comment"] : null);
        echo "\",
  text_return_action        = \"";
        // line 1187
        echo (isset($context["text_return_action"]) ? $context["text_return_action"] : null);
        echo "\",
  text_no_order             = \"";
        // line 1188
        echo (isset($context["text_no_order"]) ? $context["text_no_order"] : null);
        echo "\",
  pricelist_status          = \"";
        // line 1189
        echo (isset($context["pricelist_status"]) ? $context["pricelist_status"] : null);
        echo "\",
  text_price_changed        = \"";
        // line 1190
        echo (isset($context["text_price_changed"]) ? $context["text_price_changed"] : null);
        echo "\",
  select_customer_first     = \"";
        // line 1191
        echo (isset($context["select_customer_first"]) ? $context["select_customer_first"] : null);
        echo "\",
  button_credit             = \"";
        // line 1192
        echo (isset($context["button_credit"]) ? $context["button_credit"] : null);
        echo "\",
  entry_date_ordered        = \"";
        // line 1193
        echo (isset($context["entry_date_ordered"]) ? $context["entry_date_ordered"] : null);
        echo "\",
  entry_model               = \"";
        // line 1194
        echo (isset($context["entry_model"]) ? $context["entry_model"] : null);
        echo "\",
  entry_quantity            = \"";
        // line 1195
        echo (isset($context["entry_quantity"]) ? $context["entry_quantity"] : null);
        echo "\",
  entry_reason              = \"";
        // line 1196
        echo (isset($context["entry_reason"]) ? $context["entry_reason"] : null);
        echo "\",
  entry_action              = \"";
        // line 1197
        echo (isset($context["entry_action"]) ? $context["entry_action"] : null);
        echo "\",
  entry_opened              = \"";
        // line 1198
        echo (isset($context["entry_opened"]) ? $context["entry_opened"] : null);
        echo "\",
  entry_fault_detail        = \"";
        // line 1199
        echo (isset($context["entry_fault_detail"]) ? $context["entry_fault_detail"] : null);
        echo "\",
  entry_name                = \"";
        // line 1200
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo "\",
  entry_customer            = \"";
        // line 1201
        echo (isset($context["entry_customer"]) ? $context["entry_customer"] : null);
        echo "\",
  entry_firstname           = \"";
        // line 1202
        echo (isset($context["entry_firstname"]) ? $context["entry_firstname"] : null);
        echo "\",
  entry_lastname            = \"";
        // line 1203
        echo (isset($context["entry_lastname"]) ? $context["entry_lastname"] : null);
        echo "\",
  entry_status              = \"";
        // line 1204
        echo (isset($context["entry_status"]) ? $context["entry_status"] : null);
        echo "\",
  entry_telephone           = \"";
        // line 1205
        echo (isset($context["entry_telephone"]) ? $context["entry_telephone"] : null);
        echo "\",
  entry_email               = \"";
        // line 1206
        echo (isset($context["entry_email"]) ? $context["entry_email"] : null);
        echo "\",
  entry_opened1             = \"";
        // line 1207
        echo (isset($context["entry_opened1"]) ? $context["entry_opened1"] : null);
        echo "\",

  button_exchange           = \"";
        // line 1209
        echo (isset($context["button_exchange"]) ? $context["button_exchange"] : null);
        echo "\",
  button_return             = \"";
        // line 1210
        echo (isset($context["button_return"]) ? $context["button_return"] : null);
        echo "\",
  button_close              = \"";
        // line 1211
        echo (isset($context["button_close"]) ? $context["button_close"] : null);
        echo "\",
  button_submit             = \"";
        // line 1212
        echo (isset($context["button_submit"]) ? $context["button_submit"] : null);
        echo "\",
  button_remove_credit      = \"";
        // line 1213
        echo (isset($context["button_remove_credit"]) ? $context["button_remove_credit"] : null);
        echo "\",
  return_heading            = \"";
        // line 1214
        echo (isset($context["return_heading"]) ? $context["return_heading"] : null);
        echo "\",

  error_return_offline      = \"";
        // line 1216
        echo (isset($context["error_return_offline"]) ? $context["error_return_offline"] : null);
        echo "\",
  error_check_qty           = \"";
        // line 1217
        echo (isset($context["error_check_qty"]) ? $context["error_check_qty"] : null);
        echo "\",
  error_delivery_max        = \"";
        // line 1218
        echo (isset($context["error_delivery_max"]) ? $context["error_delivery_max"] : null);
        echo "\",
  error_return_product      = \"";
        // line 1219
        echo (isset($context["error_return_product"]) ? $context["error_return_product"] : null);
        echo "\",
  error_exchange_product    = \"";
        // line 1220
        echo (isset($context["error_exchange_product"]) ? $context["error_exchange_product"] : null);
        echo "\",
  error_load_returns        = \"";
        // line 1221
        echo (isset($context["error_load_returns"]) ? $context["error_load_returns"] : null);
        echo "\",
  error_quantity_exceed     = \"";
        // line 1222
        echo (isset($context["error_quantity_exceed"]) ? $context["error_quantity_exceed"] : null);
        echo "\",
  error_credit_offline      = \"";
        // line 1223
        echo (isset($context["error_credit_offline"]) ? $context["error_credit_offline"] : null);
        echo "\",
  error_credit_amount       = \"";
        // line 1224
        echo (isset($context["error_credit_amount"]) ? $context["error_credit_amount"] : null);
        echo "\",
  error_offline_in_credit   = \"";
        // line 1225
        echo (isset($context["error_offline_credit"]) ? $context["error_offline_credit"] : null);
        echo "\",
  error_mobile_view_return  = \"";
        // line 1226
        echo (isset($context["error_mobile_return"]) ? $context["error_mobile_return"] : null);
        echo "\",
  error_offline_file        = \"";
        // line 1227
        echo (isset($context["error_offline_file"]) ? $context["error_offline_file"] : null);
        echo "\";
  </script>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "default/template/wkpos/wkpos.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2758 => 1227,  2754 => 1226,  2750 => 1225,  2746 => 1224,  2742 => 1223,  2738 => 1222,  2734 => 1221,  2730 => 1220,  2726 => 1219,  2722 => 1218,  2718 => 1217,  2714 => 1216,  2709 => 1214,  2705 => 1213,  2701 => 1212,  2697 => 1211,  2693 => 1210,  2689 => 1209,  2684 => 1207,  2680 => 1206,  2676 => 1205,  2672 => 1204,  2668 => 1203,  2664 => 1202,  2660 => 1201,  2656 => 1200,  2652 => 1199,  2648 => 1198,  2644 => 1197,  2640 => 1196,  2636 => 1195,  2632 => 1194,  2628 => 1193,  2624 => 1192,  2620 => 1191,  2616 => 1190,  2612 => 1189,  2608 => 1188,  2604 => 1187,  2600 => 1186,  2596 => 1185,  2592 => 1184,  2588 => 1183,  2584 => 1182,  2580 => 1181,  2576 => 1180,  2572 => 1179,  2568 => 1178,  2564 => 1177,  2560 => 1176,  2556 => 1175,  2552 => 1174,  2548 => 1173,  2544 => 1172,  2540 => 1171,  2536 => 1170,  2532 => 1169,  2528 => 1168,  2524 => 1167,  2520 => 1166,  2515 => 1164,  2511 => 1163,  2507 => 1162,  2503 => 1161,  2499 => 1160,  2495 => 1159,  2491 => 1158,  2487 => 1157,  2483 => 1156,  2479 => 1155,  2475 => 1154,  2471 => 1153,  2467 => 1152,  2463 => 1151,  2459 => 1150,  2455 => 1149,  2451 => 1148,  2447 => 1147,  2443 => 1146,  2439 => 1145,  2435 => 1144,  2431 => 1143,  2427 => 1142,  2423 => 1141,  2419 => 1140,  2415 => 1139,  2411 => 1138,  2407 => 1137,  2403 => 1136,  2399 => 1135,  2395 => 1134,  2391 => 1133,  2387 => 1132,  2383 => 1131,  2379 => 1130,  2375 => 1129,  2371 => 1128,  2367 => 1127,  2363 => 1126,  2359 => 1125,  2355 => 1124,  2351 => 1123,  2347 => 1122,  2343 => 1121,  2339 => 1120,  2335 => 1119,  2331 => 1118,  2327 => 1117,  2323 => 1116,  2319 => 1115,  2315 => 1114,  2311 => 1113,  2307 => 1112,  2303 => 1111,  2299 => 1110,  2295 => 1109,  2291 => 1108,  2287 => 1107,  2283 => 1106,  2279 => 1105,  2275 => 1104,  2271 => 1103,  2267 => 1102,  2263 => 1101,  2259 => 1100,  2255 => 1099,  2251 => 1098,  2247 => 1097,  2243 => 1096,  2239 => 1095,  2235 => 1094,  2231 => 1093,  2227 => 1092,  2223 => 1091,  2219 => 1090,  2215 => 1089,  2211 => 1088,  2207 => 1087,  2203 => 1086,  2199 => 1085,  2195 => 1084,  2191 => 1083,  2187 => 1082,  2183 => 1081,  2179 => 1080,  2175 => 1079,  2171 => 1078,  2167 => 1077,  2163 => 1076,  2159 => 1075,  2155 => 1074,  2151 => 1073,  2147 => 1072,  2143 => 1071,  2139 => 1070,  2135 => 1069,  2131 => 1068,  2127 => 1067,  2123 => 1066,  2119 => 1065,  2115 => 1064,  2111 => 1063,  2107 => 1062,  2103 => 1061,  2099 => 1060,  2095 => 1059,  2088 => 1055,  2082 => 1052,  2076 => 1049,  2067 => 1043,  2062 => 1042,  2056 => 1039,  2053 => 1038,  2051 => 1037,  2043 => 1032,  2032 => 1024,  2017 => 1017,  2003 => 1008,  1997 => 1007,  1989 => 1002,  1985 => 1001,  1981 => 1000,  1977 => 999,  1970 => 994,  1964 => 992,  1961 => 991,  1953 => 989,  1951 => 988,  1946 => 985,  1941 => 983,  1938 => 982,  1935 => 981,  1929 => 979,  1926 => 978,  1920 => 976,  1917 => 975,  1911 => 973,  1908 => 972,  1902 => 970,  1899 => 969,  1893 => 967,  1890 => 966,  1884 => 964,  1881 => 963,  1875 => 961,  1872 => 960,  1862 => 958,  1860 => 957,  1851 => 951,  1846 => 949,  1831 => 937,  1827 => 936,  1818 => 930,  1813 => 928,  1806 => 924,  1801 => 922,  1794 => 918,  1789 => 916,  1782 => 912,  1768 => 901,  1764 => 900,  1757 => 896,  1749 => 891,  1735 => 880,  1731 => 879,  1725 => 876,  1718 => 872,  1704 => 861,  1700 => 860,  1690 => 855,  1685 => 853,  1680 => 850,  1674 => 848,  1668 => 846,  1666 => 845,  1663 => 844,  1657 => 842,  1651 => 840,  1649 => 839,  1644 => 837,  1633 => 829,  1628 => 827,  1620 => 821,  1614 => 819,  1608 => 817,  1606 => 816,  1601 => 815,  1595 => 813,  1589 => 811,  1587 => 810,  1577 => 803,  1564 => 793,  1558 => 790,  1552 => 787,  1539 => 777,  1533 => 774,  1527 => 771,  1514 => 761,  1503 => 753,  1496 => 749,  1489 => 745,  1482 => 741,  1471 => 733,  1457 => 722,  1449 => 717,  1435 => 706,  1431 => 705,  1425 => 702,  1419 => 699,  1405 => 688,  1401 => 687,  1397 => 686,  1389 => 681,  1385 => 680,  1375 => 673,  1369 => 669,  1358 => 667,  1354 => 666,  1350 => 665,  1344 => 662,  1337 => 658,  1332 => 656,  1325 => 652,  1320 => 650,  1313 => 646,  1308 => 644,  1303 => 642,  1296 => 638,  1291 => 636,  1284 => 632,  1279 => 630,  1272 => 626,  1267 => 624,  1260 => 620,  1255 => 618,  1246 => 612,  1239 => 608,  1224 => 596,  1215 => 590,  1210 => 588,  1201 => 582,  1191 => 575,  1176 => 563,  1171 => 561,  1167 => 560,  1164 => 559,  1161 => 558,  1155 => 556,  1152 => 555,  1146 => 553,  1143 => 552,  1138 => 550,  1133 => 549,  1131 => 548,  1126 => 546,  1116 => 539,  1105 => 530,  1099 => 527,  1093 => 524,  1089 => 522,  1087 => 521,  1081 => 518,  1078 => 517,  1071 => 513,  1068 => 512,  1061 => 508,  1058 => 507,  1055 => 506,  1052 => 505,  1045 => 501,  1042 => 500,  1040 => 499,  1034 => 496,  1031 => 495,  1029 => 494,  1023 => 491,  1010 => 481,  1000 => 474,  994 => 471,  990 => 470,  978 => 461,  971 => 457,  964 => 453,  954 => 446,  929 => 424,  924 => 422,  920 => 421,  912 => 416,  891 => 404,  880 => 396,  874 => 393,  868 => 390,  863 => 388,  859 => 387,  854 => 385,  836 => 370,  823 => 360,  819 => 359,  815 => 358,  811 => 357,  799 => 348,  795 => 347,  791 => 346,  777 => 335,  766 => 327,  757 => 321,  751 => 318,  747 => 316,  739 => 311,  733 => 308,  730 => 307,  728 => 306,  718 => 299,  712 => 295,  706 => 293,  700 => 291,  698 => 290,  695 => 289,  689 => 287,  683 => 285,  681 => 284,  675 => 281,  666 => 275,  658 => 269,  646 => 267,  643 => 266,  631 => 264,  629 => 263,  624 => 261,  615 => 255,  610 => 252,  604 => 251,  596 => 249,  588 => 247,  585 => 246,  581 => 245,  575 => 242,  569 => 238,  563 => 237,  555 => 235,  547 => 233,  544 => 232,  540 => 231,  534 => 228,  527 => 224,  520 => 220,  515 => 218,  508 => 214,  503 => 212,  496 => 208,  491 => 206,  482 => 202,  477 => 200,  468 => 196,  463 => 194,  454 => 190,  449 => 188,  440 => 184,  435 => 182,  419 => 175,  409 => 174,  400 => 168,  393 => 164,  387 => 160,  375 => 158,  370 => 157,  365 => 155,  356 => 149,  349 => 145,  345 => 144,  341 => 143,  337 => 142,  333 => 141,  329 => 140,  320 => 134,  315 => 132,  308 => 128,  302 => 125,  296 => 121,  292 => 119,  286 => 117,  284 => 116,  276 => 115,  267 => 113,  257 => 106,  253 => 105,  249 => 104,  237 => 95,  233 => 94,  229 => 93,  219 => 85,  213 => 81,  210 => 80,  204 => 76,  201 => 75,  192 => 68,  190 => 67,  185 => 65,  181 => 64,  176 => 61,  167 => 59,  163 => 58,  152 => 56,  148 => 55,  140 => 49,  127 => 47,  123 => 46,  117 => 42,  113 => 40,  109 => 38,  107 => 37,  103 => 35,  99 => 33,  97 => 32,  91 => 28,  87 => 26,  82 => 23,  80 => 22,  77 => 21,  71 => 19,  69 => 18,  63 => 16,  61 => 15,  57 => 14,  53 => 13,  42 => 7,  36 => 6,  29 => 4,  23 => 3,  19 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <!--[if IE]><![endif]-->*/
/* <!--[if IE 8 ]><html dir="{{ direction }}" lang="{{ lang }}" class="ie8"><![endif]-->*/
/* <!--[if IE 9 ]><html dir="{{ direction }}" lang="{{ lang }}" class="ie9"><![endif]-->*/
/* <!--[if (gt IE 9)|!(IE)]><!-->*/
/* <!-- <html dir="{{ direction }}" lang="{{ lang }}" manifest="../manifest.appcache"> -->*/
/* <html dir="{{ direction }}" lang="{{ lang }}">*/
/* <!--<![endif]-->*/
/* <head>*/
/* <meta charset="UTF-8" />*/
/* <meta name="viewport" content="width=device-width, initial-scale=1">*/
/* <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/* <title>{{ title }}</title>*/
/* <base href="{{ base }}" />*/
/* {% if description %}*/
/* <meta name="description" content="{{ description }}" />*/
/* {% endif %}*/
/* {% if keywords %}*/
/* <meta name="keywords" content= "{{ keywords }}" />*/
/* {% endif %}*/
/* <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>*/
/* {% if direction == 'ltr' %}*/
/* <link id="bootstrap-css" href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet"*/
/*  media="screen" />*/
/* {% else %}*/
/*  <link id="bootstrap-css" href="wkpos/css/bootstrap-rtl.css" rel="stylesheet" media="screen" />*/
/* {% endif %}*/
/* <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>*/
/* <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />*/
/* */
/* <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />*/
/* {% if direction == 'ltr' %}*/
/*   <link id="stylesheet" href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">*/
/* {% else %}*/
/*   <link id="stylesheet" href="wkpos/css/stylesheet-rtl.css" rel="stylesheet">*/
/* {% endif %}*/
/* {% if direction == 'ltr' %}*/
/* <link id="wkpos-css" href="wkpos/css/wkpos.css" rel="stylesheet" media="screen">*/
/* {% else %}*/
/* <link id="wkpos-css" href="wkpos/css/wkpos-rtl.css" rel="stylesheet" media="screen">*/
/* {% endif %}*/
/* <script src="catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js" type="text/javascript"></script>*/
/* <script src="catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js" type="text/javascript"></script>*/
/* <script src="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>*/
/* <link href="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />*/
/* {% for style in styles %}*/
/* <link href="{{ style.href }}" type="text/css" rel="{{ style.rel }}" media="{{ style.media }}" />*/
/* {% endfor %}*/
/* <script src="wkpos/js/localforage.js" type="text/javascript"></script>*/
/* <script src="wkpos/js/common.js" type="text/javascript"></script>*/
/* <script src="wkpos/js/wkpos.js" type="text/javascript"></script>*/
/* */
/*         <script src="wkpos/js/wkpos_receipt.js" type="text/javascript"></script>*/
/* <script src="wkpos/js/jQuery.print.min.js" type="text/javascript"></script>      */
/* {% for link in links %}*/
/* <link href="{{ link.href }}" rel="{{ link.rel }}" />*/
/* {% endfor %}*/
/* {% for script in scripts %}*/
/* <script src="{{ script }}" type="text/javascript"></script>*/
/* {% endfor %}*/
/* <style type="text/css">*/
/* */
/*  .thermal {*/
/*    width: {{ paper_size }};*/
/*    font-weight: {{ font_weight }};*/
/*  }*/
/*  {% if paper_size == '595px' %}*/
/*   .thermal {*/
/*     max-width: 630px;*/
/*   }*/
/*   .font-store {*/
/*     font-size: 14px;*/
/*   }*/
/*  {% endif %}*/
/*  {% if paper_size == '58mm' %}*/
/*   .font-store {*/
/*     font-size: 11px;*/
/*   }*/
/*  {% endif %}*/
/*  {% if paper_size == '80mm' %}*/
/*   .font-store {*/
/*     font-size: 13px;*/
/*   }*/
/*  {% endif %}*/
/* </style>*/
/* </head>*/
/* <body>*/
/*   <div id="top-div">*/
/*     <div id="clockin">*/
/*       <div class="modal-dialog modal-sm">*/
/*         <div class="modal-content">*/
/*           <div class="modal-body">*/
/*             <h3><b>{{ text_resume_sess }}</b></h3>*/
/*             <button class="btn buttons-sp" id="resumeSession" tabindex="5"><i class="fa fa-hourglass-half"></i> {{ text_resume }}</button>*/
/*             <button class="btn buttons-sp" id="startSession" tabindex="6"><i class="fa fa-hourglass-start"></i> {{ text_start_sess }}</button>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*     <div id="postorder">*/
/*       <div class="modal-dialog modal-sm">*/
/*         <div class="modal-content">*/
/*           <div class="modal-body">*/
/*             <h3 style="margin-top: 0;"><b>{{ heading_invoice }}</b></h3>*/
/*             <button class="btn buttons-sp" onclick="printInvoice();"><i class="fa fa-print"></i> {{ button_invoice }}</button>*/
/*             <button class="btn buttons-sp pull-right" onclick="$('#postorder, #loader').css('display', 'none');">{{ button_skip }} <i class="fa fa-arrow-right"></i></button>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*     <div class="modal" id="loginModalParent">*/
/*       <div class="col-sm-7 col-xs-12 home-div">*/
/*         <h2><b>{% if pos_heading1 %}{{ pos_heading1 }}{% else %}A Complete Retail Management Solution{% endif %}</b></h2>*/
/*         <div>*/
/*       <h3><b>{% if pos_heading2 %}{{ pos_heading2 }}{% else %}Branding{% endif %}</b></h3>*/
/*           {% if pos_content %}*/
/*             {{ pos_content }}*/
/*           {% else %}*/
/*           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.*/
/*           {% endif %}*/
/*         </div>*/
/*       </div>*/
/*       <div class="col-sm-5 col-xs-12 text-center login-div">*/
/*         <div class="form-horizontal">*/
/*           <h3>{{ heading_login }}</h3>*/
/*           <div class="form-group input-group">*/
/*             <span class="input-group-addon"><i class="fa fa-envelope"></i></span>*/
/*             <input type="text" class="form-control" id="input-username" placeholder="{{ entry_username }}" value="" name="username" tabindex="2">*/
/*           </div>*/
/*           <div class="form-group input-group">*/
/*             <span class="input-group-addon"><i class="fa fa-key"></i></span>*/
/*             <input type="password" class="form-control" id="input-password" placeholder="{{ entry_password }}" value="" name="password" tabindex="3">*/
/*           </div>*/
/*           <button class="buttons-sp" id="login-submit" onclick="loginUser();" tabindex="4">{{ button_signin }}</button>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*     <div class="col-xs-1" id="pos-side-panel">*/
/*       <div class="col-xs-12" id="left-panel">*/
/*         <div class="wksidepanel button-payment" id="button-payment"><i class="fa fa-credit-card"></i><br><span class="hidden-xs">{{ text_checkout }}</span></div>*/
/*         <div class="wksidepanel" id="button-order"><i class="fa fa-circle-o"></i><br><span class="hidden-xs">{{ text_orders }}</span></div>*/
/*         <div class="wksidepanel" id="button-return"><i class="fa fa-reply"></i><br><span class="hidden-xs">{{ text_returns }}</span></div>*/
/*         <div class="wksidepanel" id="button-account"><i class="fa fa-wrench"></i><br><span class="hidden-xs">{{ text_settings }}</span></div>*/
/*         <div class="wksidepanel" id="button-other"><i class="fa fa-gears"></i><br><span class="hidden-xs">{{ text_others }}</span></div>*/
/*         <div class="wksidepanel" id="button-report"><i class="fa fa-list"></i><br><span class="hidden-xs">{{ text_reports }}</span></div>*/
/*         */
/*       </div>*/
/*       <div class="mode-div">*/
/*         <button class="btn label label-success" id="mode"><i class="fa fa-toggle-on"></i> <span class="hidden-xs">{{ text_online }}</span></button>*/
/*       </div>*/
/*     </div>*/
/*     <div class="col-xs-8 category-div">*/
/*       <div class="col-xs-12 upper-category">*/
/*         <div class="col-xs-9 col-md-8 lower-category">*/
/*           <div class="wkcategory categoryProduct" category-id="0" style="color: #0c8c00;"><span class="hidden-xs">{{ text_populars }}</span><span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-fire font-22"></i></span></div>*/
/*           <span class="hidden-xs">*/
/*           {% for key, category in categories if key < 2 %}*/
/*             <div class="wkcategory categoryProduct" category-id="{{ category.category_id }}">{{ category.name }}</div>*/
/*           {% endfor %}*/
/*           </span>*/
/*           <span data-toggle="modal" data-target="#categoryList" class="wkcategory"><i class="fa fa-list font-22"></i></span>*/
/*         </div>*/
/*         <div class="col-xs-3 col-md-4" id="search-div">*/
/*           <input type="text" name="search" id="search" class="form-control" placeholder="{{ text_search }}">*/
/*         </div>*/
/*       </div>*/
/*       <div class="col-xs-12 text-center" style="padding: 0;">*/
/*         <h4>{{ text_all_products }}</h4>*/
/*         <div class="col-xs-12" id="product-panel">*/
/*         </div>*/
/*       </div>*/
/*       <div class="parents account-parent">*/
/*         <div class="payment-child" style="width: 35%;">*/
/*           <div class="wkpaymentmethod wkaccounts" type="basic"><i class="fa fa-wrench"></i> <span class="margin-10">{{ text_basic_settings }}</span><i class="fa {% if direction == 'ltr' %}fa-chevron-right{% else %}fa-chevron-left{% endif %} hide"></i></div>*/
/*           <div class="wkpaymentmethod wkaccounts" type="other"><i class="fa fa-cogs"></i> <span class="margin-10">{{ text_other_settings }}</span><i class="fa {% if direction == 'ltr' %}fa-chevron-right{% else %}fa-chevron-left{% endif %} hide"></i></div>*/
/*         </div>*/
/*         <div class="payment-child2 form-horizontal" style="width: 65%;">*/
/*           <span class="pull-right cursor color-black close-it"><i class="fa fa-times font-25"></i></span>*/
/*           <div class="basic-account setting-account hide">*/
/*             <div class="scroll-account">*/
/*               <div class="form-group required">*/
/*                 <label class="col-sm-4 control-label" for="first-name">{{ text_firstname }}</label>*/
/*                 <div class="col-sm-8">*/
/*                   <input class="form-control" id="first-name" name="firstname" placeholder="{{ text_firstname }}" type="text" value="{{ firstname }}">*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group required">*/
/*                 <label class="col-sm-4 control-label" for="last-name">{{ text_lastname }}</label>*/
/*                 <div class="col-sm-8">*/
/*                   <input class="form-control" id="last-name" name="lastname" placeholder="{{ text_lastname }}" type="text" value="{{ lastname }}">*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group required">*/
/*                 <label class="col-sm-4 control-label" for="user-name">{{ text_username }}</label>*/
/*                 <div class="col-sm-8">*/
/*                   <input class="form-control" id="user-name" name="username" placeholder="{{ text_username }}" type="text" value="{{ username }}">*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group required">*/
/*                 <label class="col-sm-4 control-label" for="account-email">{{ text_account_email }}</label>*/
/*                 <div class="col-sm-8">*/
/*                   <input class="form-control" id="account-email" name="account_email" placeholder="{{ text_account_email }}" type="text" value="{{ email }}">*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group required">*/
/*                 <label class="col-sm-4 control-label" for="account-ppwd">{{ text_ppassword }}</label>*/
/*                 <div class="col-sm-8">*/
/*                   <input class="form-control" id="account-ppwd" name="account_ppwd" placeholder="{{ text_ppassword }}" type="password" value="">*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group required">*/
/*                 <label class="col-sm-4 control-label" for="account-npwd">{{ text_npassword }}</label>*/
/*                 <div class="col-sm-8">*/
/*                   <input class="form-control" id="account-npwd" name="account_npwd" placeholder="{{ text_npassword }}" type="password" value="">*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group required">*/
/*                 <label class="col-sm-4 control-label" for="account-cpwd">{{ text_cpassword }}</label>*/
/*                 <div class="col-sm-8">*/
/*                   <input class="form-control" id="account-cpwd" name="account_cpwd" placeholder="{{ text_cpassword }}" type="password" value="">*/
/*                 </div>*/
/*               </div>*/
/*             </div>*/
/*             <button class="col-xs-12 btn buttons-sp button-accounts" stype="basic"><strong>{{ text_save_details }}</strong></button>*/
/*           </div>*/
/*           <div class="other-account setting-account hide">*/
/*             <div class="form-group">*/
/*               <label class="col-sm-4 control-label" for="input-langauge">{{ text_language }}</label>*/
/*               <div class="col-sm-8">*/
/*                 <select class="form-control" id="input-language">*/
/*                   {% for language_value in languages %}*/
/*                   {% if language == language_value.code %}*/
/*                   <option value="{{ language_value.code }}" selected="selected">{{ language_value.name }}</option>*/
/*                   {% else %}*/
/*                   <option value="{{ language_value.code }}">{{ language_value.name }}</option>*/
/*                   {% endif %}*/
/*                   {% endfor %}*/
/*                 </select>*/
/*               </div>*/
/*             </div>*/
/*             <div class="form-group">*/
/*               <label class="col-sm-4 control-label" for="input-currency">{{ text_currency }}</label>*/
/*               <div class="col-sm-8">*/
/*                 <select class="form-control" id="input-currency">*/
/*                   {% for currency_value in currencies %}*/
/*                   {% if currency == currency_value.code %}*/
/*                     <option value="{{ currency_value.code }}" selected="selected">{{ currency_value.title }}</option>*/
/*                   {% else %}*/
/*                     <option value="{{ currency_value.code }}">{{ currency_value.title }}</option>*/
/*                   {% endif %}*/
/*                   {% endfor %}*/
/*                 </select>*/
/*               </div>*/
/*             </div>*/
/*             <button class="col-xs-12 btn buttons-sp button-accounts" stype="other"><strong>{{ text_save }}</strong></button>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*       <div class="parents payment-parent">*/
/*         <div class="payment-child">*/
/*           <h3 class="payment-heading"><strong>{{ heading_payments }}:</strong></h3>*/
/*           <div id="payment-methods">*/
/*             {% if cash_payment_status %}*/
/*             <div class="wkpaymentmethod" type="cash-payment"><i class="fa fa-money"></i> <span class="margin-10">{{ cash_payment_title }}</span><i class="fa {% if direction == 'ltr' %}fa-chevron-right{% else %}fa-chevron-left{% endif %}t hide"></i></div>*/
/*             {% endif %}*/
/*             {% if card_payment_status %}*/
/*             <div class="wkpaymentmethod" type="card-payment"><i class="fa fa-credit-card"></i> <span class="margin-10">{{ card_payment_title }}</span><i class="fa {% if direction == 'ltr' %}fa-chevron-right{% else %}fa-chevron-left{% endif %} hide"></i></div>*/
/*             {% endif %}*/
/*           </div>*/
/*         </div>*/
/*         <div class="payment-child2 form-horizontal">*/
/*           <span class="pull-right cursor color-black close-it"><i class="fa fa-times font-25"></i></span>*/
/*           <div class="cash-payment hide">*/
/*             <div class="form-group">*/
/*               <label class="col-sm-4 control-label">{{ text_balance_due }}</label>*/
/*               <div class="col-sm-8">*/
/*                 <div class="form-control" id="balance-due"></div>*/
/*               </div>*/
/*             </div>*/
/*             <div class="form-group">*/
/*               <label class="col-sm-4 control-label" for="amount-tendered">{{ text_tendered }}</label>*/
/*               <div class="col-sm-8">*/
/*                 <div class="input-group input-group1">*/
/*                   {% if symbol_position == 'L' %}*/
/*                   <span class="input-group-addon currency"><strong>{{ currency_code }}</strong></span>*/
/*                   {% else %}*/
/*                   <span class="input-group-addon currency" style="display: none;">{{ currency_code }}</span>*/
/*                   {% endif %}*/
/*                   <input class="form-control" id="amount-tendered" placeholder="" type="text" onkeypress="return validate(event, this)">*/
/*                   {% if symbol_position == 'R' %}*/
/*                   <span class="input-group-addon currency"><strong>{{ currency_code }}</strong></span>*/
/*                   {% else %}*/
/*                   <span class="input-group-addon currency" style="display: none;">{{ currency_code }}</span>*/
/*                   {% endif %}*/
/*                 </div>*/
/*               </div>*/
/*             </div>*/
/*             <div class="form-group">*/
/*               <label class="col-sm-4 control-label">{{ text_change }}</label>*/
/*               <div class="col-sm-8">*/
/*                 <div class="form-control" id="change"></div>*/
/*               </div>*/
/*             </div>*/
/*           </div>*/
/*           <div class="all-payment hide">*/
/*             {% if credit_status %}*/
/*             <div class="form-group">*/
/*               <label class="col-sm-4 control-label">{{ text_balance_credit }}</label>*/
/*               <div class="col-sm-8">*/
/*                 <div class="form-control input-group" id="balance-credit">*/
/*                   {{ select_customer_first }}*/
/*                 </div>*/
/*               </div>*/
/*             </div>*/
/*             {% endif %}*/
/*             <div class="form-group">*/
/*               <div class="col-sm-12">*/
/*                 <textarea class="form-control" id="orderNote" placeholder="{{ text_add_order_note }}"></textarea>*/
/*               </div>*/
/*             </div>*/
/*             <button class="col-xs-12 btn buttons-sp accept-payment" ptype=""><strong>{{ button_payment }}</strong></button>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*       <!-- POS Update Code for Return  -->*/
/*       <div class="parents return-parent">*/
/*         <!-- <h3 class="pull-left" id="return-heading">{{ text_returned_product }}</h3> -->*/
/*           <span class="pull-right cursor color-black close-it" onclick="$('#returns').html('');"><i class="fa fa-times font-25"></i></span>*/
/*         <div class="col-sm-12" id="return-container">*/
/*           <div class="row" id="returns"></div>*/
/*         </div>*/
/*       </div>*/
/*       <div class="parents return-form-parent">*/
/*         <div class="col-xs-12">*/
/*           <h3 class="text-center">{{ return_heading }}<span class="pull-right cursor color-black close-it"><i class="fa fa-times font-25"></i></span></h3>*/
/*         </div>*/
/*         <div class="col-sm-12" style="overflow-y:auto;">*/
/*           <div class="row"  id="return-form">*/
/* */
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*       <!-- POS Update Code for Return  -->*/
/*       <div class="parents order-parent">*/
/*         <div class="col-xs-12 order-child">*/
/*           <div class="wkcategory wkorder" otype="1">{{ text_previous }}</div>*/
/*           <div class="wkcategory wkorder" otype="2">{{ text_onhold }}</div>*/
/*           <div class="wkcategory wkorder" otype="3">{{ text_offline }}</div>*/
/*           <span class="pull-right cursor color-black close-it"><i class="fa fa-times font-25"></i></span>*/
/*         </div>*/
/*         <div class="col-sm-12" id="order-container">*/
/*           <div class="row" id="orders"></div>*/
/*         </div>*/
/*       </div>*/
/*       <div class="parents other-parent">*/
/*         <div class="col-xs-12 other-child">*/
/*           <div class="wkcategory wkother" otype="1">{{ text_low_stock }}</div>*/
/*           <div class="wkcategory wkother" otype="2">{{ text_request }}</div>*/
/*           <div class="wkcategory wkother" otype="3">{{ text_request_history }}</div>*/
/*           <!-- <div class="wkcategory wkother" otype="4">{{ text_inventory_report }}</div> -->*/
/*           <span class="pull-right cursor color-black close-it"><i class="fa fa-times font-25"></i></span>*/
/*         </div>*/
/*         <div class="col-sm-12" id="other-container">*/
/*           <div class="row" id="others"></div>*/
/*         </div>*/
/*       </div>*/
/*       <!-- New tab start-->*/
/*       <div class="parents report-parent">*/
/*         <div class="col-xs-12 report-child">*/
/*           <div class="wkcategory wkreport" otype="1">{{ text_inventory_report }}</div>*/
/*           <span class="pull-right cursor color-black close-it"><i class="fa fa-times font-25"></i></span>*/
/*         </div>*/
/*         <div class="col-sm-12" id="report-container">*/
/*           <div class="row">*/
/*             */
/*             <div class="col-sm-8" id="reports"></div>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*       <!-- New tab end-->*/
/*     </div>*/
/*     <div class="col-xs-3" id="cart-panel">*/
/*       <div id="logger-detail">*/
/*         <div class="logger-div pull-left">*/
/*           <img src="{{ image }}" class="img-responsive logger-img pull-left" width="40" height="40">*/
/*           <div class="pull-left" style="margin-left: 5px;">*/
/*             <span class="logger-name">{{ name }}</span><br>*/
/*             <span class="logger-post hidden-xs">({{ group_name }})</span><br>*/
/*             <div class="dropdown">*/
/*               <button class="btn label label-info dropdown-toggle" data-toggle="dropdown">{{ text_more }} <i class="caret"></i></button>*/
/*               <ul class="dropdown-menu dropdown-menu-left cursor">*/
/*                 <li>*/
/*                   <a onclick="accountSettings(this);">{{ text_account_settings }}</a>*/
/*                 </li>*/
/*                 <li>*/
/*                   <a onclick="logout();">{{ text_logout }}</a>*/
/*                 </li>*/
/*               </ul>*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*       <div class="sidepanel color-black" style="overflow-y: auto;">*/
/*         <div class="upper-category"><i class="fa {% if direction == 'ltr' %}fa-chevron-right{% else %}fa-chevron-left{% endif %} pull-right close-it cursor"></i><span class="order-txn">{{ text_order_id }}</span> #<span class="oid"></span></div>*/
/*         <div class="table-responsive hide" id="return-section">*/
/* */
/*         </div>*/
/*         <div class="col-xs-12 hide" id="return-details">*/
/* */
/*         </div>*/
/*         <div id="sidepanel-inner" style="display: none;">*/
/*           <div class="col-xs-12" id="order-upper-div">*/
/*             <div class="pull-left">*/
/*               <span class="order-date"></span><br/>*/
/*               <span class="order-time"></span><br/>*/
/*               <span>{{ text_purchase }}</span>*/
/*             </div>*/
/*             <div class="pull-right" id="order-address"></div>*/
/*           </div>*/
/*           <div class="col-xs-12" style="margin-top: 10px;">*/
/*             <span class="pull-left font-bold">{{ text_shipping_mode }}:&nbsp;</span>*/
/*             <span class="oshipping">{{ text_pickup }}</span>*/
/*           </div>*/
/*           <h5 class="col-xs-12 font-bold">{{ text_order_details }}</h5>*/
/*           <div class="width-100 order-scroll">*/
/*             <div class="col-xs-12 table-responsive">*/
/*               <table class="table table-hover margin-bottom-0" id="oitem-table">*/
/*                 <tbody id="oitem-body">*/
/*                 </tbody>*/
/*               </table>*/
/*             </div>*/
/*             <hr class="hr-tag">*/
/*             <hr class="hr-tag">*/
/*             <div class="col-xs-12 table-responsive">*/
/*               <table class="table table-hover margin-bottom-0">*/
/*                 <tbody id="oTotals">*/
/*                 </tbody>*/
/*               </table>*/
/*             </div>*/
/*           </div>*/
/*           <hr class="hr-tag">*/
/*           <div class="col-xs-12 table-responsive table-total" style="margin: 0; width: 100%;">*/
/*             <table class="table table-hover margin-bottom-0">*/
/*               <tbody>*/
/*                 <tr>*/
/*                   <td class="text-left">{{ text_total }}</td>*/
/*                   <td class="text-right oTotal">00.00</td>*/
/*                 </tr>*/
/*               </tbody>*/
/*             </table>*/
/*           </div>*/
/*           <div class="col-xs-12" style="margin-top: 5px;">*/
/*             <span class="pull-left font-bold">{{ text_payment_mode }}:&nbsp;</span>*/
/*             <span class="opayment"></span>*/
/*           </div>*/
/*           <div class="col-xs-12" style="margin-top: 5px;">*/
/*             <span class="pull-left font-bold">{{ text_note }}:&nbsp;</span>*/
/*             <span class="onote"></span>*/
/*           </div>*/
/*           <div class="col-xs-12" style="margin-top: 5px;">*/
/*             <button onclick="printBill();" class="buttons-sp width-100">{{ text_print }}</button>*/
/*           </div>*/
/*         </div>*/
/*         <div class="order-loader hide">*/
/*           <div class="cp-spinner cp-round"></div>*/
/*         </div>*/
/*       </div>*/
/*       <div class="col-xs-12 upper-cart-detail">*/
/*         <div class="col-xs-12 lower-cart-detail">*/
/*           <div class="pull-left"><i class="fa fa-trash cursor font-22" title="{{ text_delete_cart }}" data-toggle="tooltip" onclick="cart_list.delete(1)"></i></div>*/
/*           <b class="cursor" id="more-carts"><span class="">{{ text_cart_details }}</span> (<span class="cart-total">0</span>) <span class="caret"> </span></b>*/
/*           <div id="upper-cart">*/
/*           </div>*/
/*           <div class="pull-right"><i class="fa fa-plus-circle cursor font-22" title="{{ text_add_product }}" data-toggle="modal" data-target="#addProduct"></i></div>*/
/*         </div>*/
/*         <br>*/
/*         <div id="cart-detail">*/
/*           <div class="table-responsive">*/
/*             <table class="table table-hover margin-bottom-0" id="item-table">*/
/*               <tbody id="item-body">*/
/*                 <tr><td class="text-center">{{ text_empty_cart }}</td></tr>*/
/*               </tbody>*/
/*             </table>*/
/*           </div>*/
/*           <hr class="hr-tag">*/
/*           <hr class="hr-tag">*/
/*           <div class="table-responsive div-totals">*/
/*             <table class="table table-hover margin-bottom-0">*/
/*               <tbody>*/
/*                 <tr>*/
/*                   <td class="text-left">{{ text_subtotal }}</td>*/
/*                   <td class="text-right" id="subtotal">0.00</td>*/
/*                 </tr>*/
/*                 {% if discount_sort_order < tax_sort_order %}*/
/*                 <tr id="couprow" style="display: none;">*/
/*                   <td class="text-left">{{ button_coupon }}</td>*/
/*                   <td class="text-right" id="coupondisc">$0.00</td>*/
/*                 </tr>*/
/*                   {% if tax_status %}*/
/*                   <tr>*/
/*                     <td class="text-left">{{ button_tax }}</td>*/
/*                     <td class="text-right" id="tax">$0.00</td>*/
/*                   </tr>*/
/*                   {% endif %}*/
/*                 {% else %}*/
/*                   {% if tax_status %}*/
/*                   <tr>*/
/*                     <td class="text-left">{{ button_tax }}</td>*/
/*                     <td class="text-right" id="tax">$0.00</td>*/
/*                   </tr>*/
/*                   {% endif %}*/
/*                   <tr id="couprow" style="display: none;">*/
/*                     <td class="text-left">{{ button_coupon }}</td>*/
/*                     <td class="text-right" id="coupondisc">$0.00</td>*/
/*                   </tr>*/
/*                 {% endif %}*/
/*                   <tr id="discrow" style="display: none;">*/
/*                     <td class="text-left" id="discname">{{ button_discount }}</td>*/
/*                     <td class="text-right" id="discount">$0.00</td>*/
/*                   </tr>*/
/*                 {% if home_delivery_status %}*/
/*                 <tr>*/
/*                   <td>*/
/*                     <div class="checkbox checkbox-primary"><input id="checkbox-home-delivery" type="checkbox"><label for="checkbox-home-delivery">{{ home_delivery_title }}</label>*/
/*                     </div>*/
/*                   </td>*/
/*                   <td><input type="number" class="form-control pull-right" placeholder="{{ entry_delivery_charge }}" id="delivery-charge" min="0" onkeypress="return validate(event, this)"></td>*/
/*                 </tr>*/
/*                 {% endif %}*/
/*               </tbody>*/
/*             </table>*/
/*           </div>*/
/*         </div>*/
/*         <hr class="hr-tag">*/
/*         <div class="table-responsive table-total">*/
/*           <table class="table table-hover margin-bottom-0">*/
/*             <tbody>*/
/*               <tr>*/
/*                 <td class="text-left">{{ text_total }}</td>*/
/*                 <td class="text-right" id="cartTotal">00.00</td>*/
/*               </tr>*/
/*             </tbody>*/
/*           </table>*/
/*         </div>*/
/* */
/*         <button class="col-xs-12 btn" id="button-customer" data-toggle="modal" data-target="#customerSearch"><i class="fa fa-user font-22"></i> <strong class="" id="customer-name">{{ button_customer }}</strong></button>*/
/*         <div class="btn-group" style="width: 100%;">*/
/*           {% if discount_status and coupon_status %}*/
/*           <button class="col-xs-6 btn buttons-sp" data-toggle="modal" data-target="#addDiscount" id="button-discount"><i class="fa fa-minus-square"></i> <strong class="">{{ button_discount }}</strong></button>*/
/*           <button class="col-xs-6 btn buttons-sp" data-toggle="modal" data-target="#addCoupon" id="button-coupon"><i class="fa fa-ticket"></i> <strong class="">{{ button_coupon }}</strong></button>*/
/*           {% else %}*/
/*           {% if discount_status %}*/
/*           <button class="col-xs-12 btn buttons-sp" data-toggle="modal" data-target="#addDiscount" id="button-discount"><i class="fa fa-minus-square"></i> <strong class="">{{ button_discount }}</strong></button>*/
/*           {% endif %}*/
/*           {% if coupon_status %}*/
/*           <button class="col-xs-12 btn buttons-sp" data-toggle="modal" data-target="#addCoupon" id="button-coupon"><i class="fa fa-ticket"></i> <strong class="">{{ button_coupon }}</strong></button>*/
/*           {% endif %}*/
/*           {% endif %}*/
/*         </div>*/
/*         <button class="col-xs-12 btn buttons-sp button-payment btn-checkout"><i class="fa fa-money"></i> <strong class="">{{ text_checkout }}</strong></button>*/
/*         <button class="col-xs-12 btn buttons-sp btn-checkout" data-toggle="modal" data-target="#holdOrder" style="background-color: #f1b40f;"><i class="fa fa-pause"></i> <strong class="">{{ text_hold_order }}</strong></button>*/
/*         <!-- POS Update Code -->*/
/*         <button class="col-xs-12 btn buttons-sp btn-return" style="background-color: #5bc0de;"><i class="fa fa-reply"></i> <strong>{{ button_return }}</strong></button>*/
/*       </div>*/
/*     </div>*/
/*     <div id="return-order" class="modal fade" role="dialog" tabindex="-1">*/
/*       <div class="modal-dialog modal-sm">*/
/*         <div class="modal-content">*/
/*           <div class="product-option-modal" data-dismiss="modal"><i class="fa fa-times"></i></div>*/
/*           <div class="modal-header">*/
/*             <h4 class="modal-title"></h4>*/
/*           </div>*/
/*           <div class="modal-body color-black">*/
/*             <div class="form-group">*/
/*               <input type="text" id="search-order-id" class="form-control" placeholder="{{ placeholder_order_id }}" onkeypress="return isNumber(event);" ondrop="return false;">*/
/*             </div>*/
/*           </div>*/
/*           <div class="table" id="search-order-result">*/
/* */
/*           </div>*/
/*           <div class="modal-footer">*/
/*             <button type="button" class="btn btn-info" data-dismiss="modal">{{ button_close }}</button>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*     <div class="pull-right color-white" id="fixed-div">*/
/*       <button class="btn label label-info barcode-scan" data-toggle="modal" data-target="#barcodeScan" title="{{ "Barcode Scan" }}" data-toggle="tooltip"><i class="fa fa-barcode"></i></button>*/
/*       <span id="hold-carts">*/
/*         <button class="btn label label-info" title="{{ text_view_hold }}" data-toggle="tooltip">*/
/*           <span class="fa fa-pause"></span>*/
/*         </button>*/
/*         <div class="label label-danger cart-hold fix-label">0</div>*/
/*       </span>*/
/*       <span id="show-cart">*/
/*         <div class="btn label label-info" title="{{ text_cart_items }}" data-toggle="tooltip">*/
/*           <span class="fa fa-shopping-cart"></span>*/
/*         </div>*/
/*         <div class="label label-danger cart-total fix-label">0</div>*/
/*       </span>*/
/*     </div>*/
/* */
/*     <div id="customerSearch" class="modal fade" role="dialog" tabindex="-1">*/
/*       <div class="modal-dialog modal-sm">*/
/*         <div class="modal-content">*/
/*           <div class="product-option-modal" data-dismiss="modal"><i class="fa fa-times"></i></div>*/
/*           <div class="modal-header">*/
/*             <h4 class="modal-title">{{ text_customer_details }}</h4>*/
/*           </div>*/
/*           <div class="modal-body">*/
/*             <div class="searchCustomer">*/
/*               <input type="text" id="searchCustomer" placeholder="{{ entry_search_customer }}" class="form-control" autocomplete="off">*/
/*               <div id="putCustomer">*/
/*               </div>*/
/*             </div>*/
/*             <div class="addCustomer form-horizontal hide">*/
/*               <div class="form-group required">*/
/*                 <label class="col-sm-2 control-label" for="input-customer-firstname">{{ entry_firstname }}</label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="text" name="firstname" value="" placeholder="{{ entry_firstname }}" id="input-customer-firstname" class="form-control" autocomplete="off" />*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group required">*/
/*                 <label class="col-sm-2 control-label" for="input-customer-lastname">{{ entry_lastname }}</label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="text" name="lastname" value="" placeholder="{{ entry_lastname }}" id="input-customer-lastname" class="form-control" autocomplete="off" />*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group required">*/
/*                 <label class="col-sm-2 control-label" for="input-telephone">{{ entry_telephone }}</label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="tel" name="telephone" value="" placeholder="{{ entry_telephone }}" id="input-telephone" class="form-control" autocomplete="off" />*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-email">{{ entry_email }}</label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="email" name="email" value="" placeholder="{{ entry_email }}" id="input-email" class="form-control" autocomplete="off" />*/
/*                 </div>*/
/*               </div>*/
/*               */
/*               <legend>{{ text_address }}</legend>*/
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-address-1">{{ entry_address_1 }}</label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="text" name="address_1" value="" placeholder="{{ entry_address_1 }}" id="input-address-1" class="form-control" autocomplete="off" />*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-city">{{ entry_city }}</label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="text" name="city" value="" placeholder="{{ entry_city }}" id="input-city" class="form-control" autocomplete="off" />*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-postcode">{{ entry_postcode }}</label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="text" name="postcode" value="" placeholder="{{ entry_postcode }}" id="input-postcode" class="form-control" autocomplete="off" />*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-country">{{ entry_country }}</label>*/
/*                 <div class="col-sm-10">*/
/*                   <select name="country_id" id="input-country" class="form-control" autocomplete="off">*/
/*                     <option value="">{{ text_select }}</option>*/
/*                     {% for country in countries %}*/
/*                     <option value="{{ country.country_id }}">{{ country.name }}</option>*/
/*                     {% endfor %}*/
/*                   </select>*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-zone">{{ entry_zone }}</label>*/
/*                 <div class="col-sm-10">*/
/*                   <select name="zone_id" id="input-zone" class="form-control" autocomplete="off">*/
/*                   </select>*/
/*                 </div>*/
/*               </div>*/
/*               <div class="text-center">*/
/*                 <button class="btn btn-default" onclick="registerCustomer($(this));"><strong>{{ text_register }}</strong></button>*/
/*                 <button class="btn btn-default" onclick="registerCustomer($(this), true);"><strong>{{ text_register_select }}</strong></button>*/
/*               </div>*/
/*             </div>*/
/*           </div>*/
/*           <div class="modal-footer">*/
/*             <button type="button" class="btn btn-success pull-left" id="addCustomer">{{ text_add_customer }}</button>*/
/*             <button type="button" class="btn btn-danger pull-left" id="removeCustomer">{{ text_remove }}</button>*/
/*             <button type="button" class="btn btn-info" data-dismiss="modal">{{ button_close }}</button>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/* */
/*     <div id="holdOrder" class="modal fade" role="dialog" tabindex="-1">*/
/*       <div class="modal-dialog modal-sm">*/
/*         <div class="modal-content">*/
/*           <div class="product-option-modal" data-dismiss="modal"><i class="fa fa-times"></i></div>*/
/*           <div class="modal-header">*/
/*             <h4 class="modal-title">{{ heading_hold_note }}</h4>*/
/*           </div>*/
/*           <div class="modal-body">*/
/*             <textarea class="form-control" placeholder="{{ entry_hold_note }}" id="holdNote" rows="4"></textarea>*/
/*           </div>*/
/*           <div class="modal-footer">*/
/*             <button type="button" class="btn btn-success pull-left" onclick="holdOrder();">{{ button_hold_order }}</button>*/
/*             <button type="button" class="btn btn-info" data-dismiss="modal">{{ button_cancel }}</button>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*     <span data-toggle="modal" data-target="#productOptions" id="buttonModal"></span>*/
/*     <div id="productOptions" class="modal fade" role="dialog" tabindex="-1">*/
/*       <div class="modal-dialog">*/
/*         <div class="modal-content">*/
/*           <div class="product-option-modal" data-dismiss="modal"><i class="fa fa-times"></i></div>*/
/*           <div class="modal-header">*/
/*             <h4 class="modal-title" id="global-modal-title">{{ text_product_options }}</h4>*/
/*           </div>*/
/*           <div class="modal-body color-black" id="posProductOptions">*/
/*           </div>*/
/*           <div class="modal-footer">*/
/*             <button type="button" class="btn btn-info" data-dismiss="modal">{{ button_close }}</button>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*     <span data-toggle="modal" data-target="#productDetails" id="buttonProductDetails"></span>*/
/*     <div id="productDetails" class="modal fade" role="dialog" tabindex="-1">*/
/*       <div class="modal-dialog modal-sm">*/
/*         <div class="modal-content">*/
/*           <div class="product-option-modal" data-dismiss="modal"><i class="fa fa-times"></i></div>*/
/*           <div class="modal-header">*/
/*             <h4 class="modal-title">{{ heading_product_detail }}</h4>*/
/*           </div>*/
/*           <div class="modal-body color-black">*/
/*             <img src="" class="img-responsive" style="margin: 0 auto" id="detail-image">*/
/*             <div class="table-responsive">*/
/*               <table class="table table-hover margin-bottom-0">*/
/*                 <tbody>*/
/*                   <tr>*/
/*                     <td class="text-right">{{ entry_name }}</td>*/
/*                     <td class="text-left" id="productName"></td>*/
/*                   </tr>*/
/*                   <tr>*/
/*                     <td class="text-right">{{ entry_price }}</td>*/
/*                     <td class="text-left" id="productPrice"></td>*/
/*                   </tr>*/
/*                   <tr>*/
/*                     <td class="text-right">{{ entry_item_left }}</td>*/
/*                     <td class="text-left" id="productItem"></td>*/
/*                   </tr>*/
/*                   <tr id="supplier-info">*/
/*                     <td class="text-right">{{ entry_suppliers }}:</td>*/
/*                     <td class="text-left" id="productSuppliers"></td>*/
/*                   </tr>*/
/*                 </tbody>*/
/*               </table>*/
/*             </div>*/
/*           </div>*/
/*           <div class="modal-footer">*/
/*             <button type="button" class="btn btn-info" data-dismiss="modal">{{ button_close }}</button>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*     <div id="categoryList" class="modal fade" role="dialog" tabindex="-1">*/
/*       <div class="modal-dialog modal-sm">*/
/*         <div class="modal-content">*/
/*           <div class="product-option-modal" data-dismiss="modal"><i class="fa fa-times"></i></div>*/
/*           <div class="modal-header">*/
/*             <h4 class="modal-title">{{ heading_category }}</h4>*/
/*           </div>*/
/*           <div class="modal-body color-black">*/
/*             <button class="btn btn-default" onclick="showAllProducts()">{{ button_show_product }}</button>*/
/*           </div>*/
/*           <div class="modal-footer">*/
/*             <button type="button" class="btn btn-info" data-dismiss="modal">{{ button_close }}</button>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*     <div id="barcodeScan" class="modal fade" role="dialog" tabindex="-1">*/
/*       <div class="modal-dialog modal-sm">*/
/*         <div class="modal-content">*/
/*           <div class="product-option-modal" data-dismiss="modal"><i class="fa fa-times"></i></div>*/
/*           <div class="modal-header">*/
/*             <h4 class="modal-title">{{ heading_scan_barcode }}</h4>*/
/*           </div>*/
/*           <div class="modal-body color-black">*/
/*             <input type="text" id="bar-code" placeholder="{{ entry_barcode }}" class="form-control">*/
/*           </div>*/
/*           <div class="modal-footer">*/
/*             <button type="button" class="btn btn-info" data-dismiss="modal">{{ button_close }}</button>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*     <div id="addDiscount" class="modal fade" role="dialog" tabindex="-1">*/
/*       <div class="modal-dialog modal-md">*/
/*         <div class="modal-content">*/
/*           <div class="product-option-modal" data-dismiss="modal"><i class="fa fa-times"></i></div>*/
/*           <div class="modal-header">*/
/*             <h4 class="modal-title">{{ heading_discount }}</h4>*/
/*           </div>*/
/*           <div class="modal-body color-black">*/
/*             <div class="row">*/
/*               <div class="col-sm-3 text-center">*/
/*                 <strong>Fixed</strong>*/
/*                 <div class="input-group input-group2">*/
/*                   {% if symbol_position == 'L' %}*/
/*                   <span class="input-group-addon currency"><strong>{{ currency_code }}</strong></span>*/
/*                   {% else %}*/
/*                   <span class="input-group-addon currency" style="display: none;"><strong>{{ currency_code }}</strong></span>*/
/*                   {% endif %}*/
/*                   <input type="text" id="fixedDisc" value="0" placeholder="{{ entry_fixed }}" class="form-control" onkeypress="return validate(event, this)">*/
/*                   {% if symbol_position == 'R' %}*/
/*                   <span class="input-group-addon currency"><strong>{{ currency_code }}</strong></span>*/
/*                   {% else %}*/
/*                   <span class="input-group-addon currency" style="display: none;"><strong>{{ currency_code }}</strong></span>*/
/*                   {% endif %}*/
/*                 </div>*/
/*               </div>*/
/*               <div class="col-sm-1 text-center" style="margin: 28px 0;">*/
/*                 <i class="fa fa-plus"></i>*/
/*               </div>*/
/*               <div class="col-sm-3 text-center">*/
/*                 <strong>{{ entry_percent }}</strong>*/
/*                 <div class="input-group">*/
/*                   <input type="text" id="percentDisc" value="0" placeholder="{{ entry_percent }}" class="form-control" onkeypress="return validate(event, this)" style="border-radius: 0;">*/
/*                   <span class="input-group-addon"><strong>%</strong></span>*/
/*                 </div>*/
/*               </div>*/
/*               <div class="col-sm-1 text-center" style="margin: 28px 0; font-size: 20px;">*/
/*                 <strong>=</strong>*/
/*               </div>*/
/*               <div class="col-sm-4 text-center">*/
/*                 <strong>{{ text_total }}</strong>*/
/*                 <div class="input-group input-group3" style="margin-top: 3px;">*/
/*                   {% if symbol_position == 'L' %}*/
/*                   <span class="input-group-addon currency"><strong>{{ currency_code }}</strong></span>*/
/*                   {% else %}*/
/*                   <span class="input-group-addon currency" style="display: none;"><strong>{{ currency_code }}</strong></span>*/
/*                   {% endif %}*/
/*                   <span class="input-group-addon" style="max-width: 140px; overflow: hidden;"><strong id="total-discount">0.00</strong></span>*/
/*                   {% if symbol_position == 'R' %}*/
/*                   <span class="input-group-addon currency"><strong>{{ currency_code }}</strong></span>*/
/*                   {% else %}*/
/*                   <span class="input-group-addon currency" style="display: none;"><strong>{{ currency_code }}</strong></span>*/
/*                   {% endif %}*/
/*                 </div>*/
/*               </div>*/
/*               <div class="col-sm-12">*/
/*                 <label class="col-sm-3" for="input-discname">{{ entry_discount }}</label>*/
/*                 <div class="col-sm-9">*/
/*                   <input type="text" class="form-control" id="input-discname" placeholder="{{ entry_discount }}" value="{{ button_discount }}">*/
/*                 </div>*/
/*               </div>*/
/*             </div>*/
/*             <div class="col-sm-12 text-center" style="float: none;">*/
/*               <button type="button" class="btn buttons-sp" id="addDiscountbtn">{{ button_apply }}</button>*/
/*               <button type="button" class="btn buttons-sp" id="removeDiscount" style="background-color: #ff6666;">{{ button_remove }}</button>*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*     <div id="addCoupon" class="modal fade" role="dialog" tabindex="-1">*/
/*       <div class="modal-dialog modal-sm">*/
/*         <div class="modal-content">*/
/*           <div class="product-option-modal" data-dismiss="modal"><i class="fa fa-times"></i></div>*/
/*           <div class="modal-header">*/
/*             <h4 class="modal-title">{{ heading_coupon }}</h4>*/
/*           </div>*/
/*           <div class="modal-body color-black">*/
/*             <div class="form-group">*/
/*               <input type="text" id="coupon-code" class="form-control" placeholder="{{ entry_coupon }}">*/
/*             </div>*/
/*             <div class="col-sm-12 text-center" style="float: none;">*/
/*               <button type="button" class="btn buttons-sp" id="addCouponbtn">{{ button_apply }}</button>*/
/*               <button type="button" class="btn buttons-sp" id="removeCoupon" style="background-color: #ff6666;">{{ button_remove }}</button>*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*     <div id="updatePrice" class="modal fade" role="dialog" tabindex="-1">*/
/*       <div class="modal-dialog modal-sm">*/
/*         <div class="modal-content">*/
/*           <div class="product-option-modal" data-dismiss="modal"><i class="fa fa-times"></i></div>*/
/*           <div class="modal-header">*/
/*             <h4 class="modal-title">{{ heading_price_update }}</h4>*/
/*           </div>*/
/*           <div class="modal-body color-black">*/
/*             <div class="form-group">*/
/*               <label id="update-label"></label>*/
/*               <input type="text" id="update-price" class="form-control" placeholder="{{ entry_updated_price }}" onkeypress="return validate(event, this)">*/
/*               <input type="hidden" id="cart-index">*/
/*             </div>*/
/*             <div class="col-sm-12 text-center" style="float: none;">*/
/*               <button type="button" class="btn buttons-sp" id="updatePricebtn">{{ button_apply }}</button>*/
/*               <button type="button" class="btn buttons-sp" id="cancelPriceUp" style="background-color: #ff6666;">{{ button_remove }}</button>*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*     <div id="addProduct" class="modal fade" role="dialog" tabindex="-1">*/
/*       <div class="modal-dialog modal-md">*/
/*         <div class="modal-content">*/
/*           <div class="product-option-modal" data-dismiss="modal"><i class="fa fa-times"></i></div>*/
/*           <div class="modal-header">*/
/*             <h4 class="modal-title">{{ heading_add_product }}</h4>*/
/*           </div>*/
/*           <div class="modal-body color-black form-horizontal">*/
/*             <div class="form-group">*/
/*               <label class="col-sm-3 control-label">{{ entry_product_name }}</label>*/
/*               <div class="col-sm-9">*/
/*                 <input type="text" name="product_name" class="form-control" placeholder="{{ entry_product_name }}">*/
/*               </div>*/
/*             </div>*/
/*             <div class="form-group">*/
/*               <label class="col-sm-3 control-label">{{ entry_product_price }}</label>*/
/*               <div class="col-sm-9">*/
/*                 <input type="text" name="product_price" class="form-control" placeholder="{{ entry_product_price }}" onkeypress="return validate(event, this)">*/
/*               </div>*/
/*             </div>*/
/*             <div class="form-group">*/
/*               <label class="col-sm-3 control-label">{{ entry_product_quantity }}</label>*/
/*               <div class="col-sm-9">*/
/*                 <input type="text" name="product_quantity" class="form-control" placeholder="{{ entry_product_quantity }}" onkeypress="return validate(event, this, true)">*/
/*               </div>*/
/*             </div>*/
/*           </div>*/
/*           <div class="modal-footer color-black">*/
/*             <div class="col-sm-12 text-center" style="float: none;">*/
/*               <button type="button" class="btn buttons-sp" id="addProductbtn">{{ button_add }}</button>*/
/*               <button type="button" class="btn buttons-sp" data-dismiss="modal" style="background-color: #ff6666;">{{ button_cancel }}</button>*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*     <div class="modal" id="loader">*/
/*       <div class="cp-spinner cp-eclipse"></div>*/
/*       <div class="progress"><div class="progress-bar progress-bar-dan"></div></div>*/
/*       <div id="loading-text"></div>*/
/*       <div id="error-text"></div>*/
/*     </div>*/
/*     <div class="posProductBlock"><img src="{{ no_image }}" width="50" height="50"></div>*/
/*     <div id="screen-div" onclick="toggleFullScreen();" title="Toggle Screen" data-toggle="tooltip">*/
/*       <img src="{{ screen_image }}">*/
/*     </div>*/
/*   </div>*/
/*   <div class="hide" id="printBill" style="color:#000 !important;">*/
/*     <div class="container thermal" style="text-align: center; max-width: 630px; font-weight:400">*/
/*       <div class="thermal" style="margin: 0 auto;">*/
/*         {% if show_store_logo and store_logo %}*/
/*         <img src="{{ store_logo }}" title="{{ store_name }}" alt="{{ store_name }}" class="img-responsive" width="200" style="margin: 0 auto;"><br>*/
/*         {% endif %}*/
/*         {% if show_store_name %}*/
/*         <span style="font-size: 14px;">{{ store_name }}</span><br>*/
/*         {% endif %}*/
/*         {% if show_store_address %}*/
/*         <span class="font-store">{{ store_address }}</span><br>*/
/*         {% endif %}*/
/*         {% if store_detail %}*/
/*         <span class="font-store">{{ store_detail }}</span><br>*/
/*         {% endif %}*/
/*         {% if show_order_date %}*/
/*         {{ text_date }} <span class="order-date"></span>*/
/*         {% endif %}*/
/*         {% if show_order_time %}*/
/*         {{ text_time }} <span class="order-time"></span><br>*/
/*         {% endif %}*/
/*         {% if show_order_id %}*/
/*         <span class="order-txn">{{ text_order_id }}</span> #<span class="oid"></span>*/
/*         {% endif %}*/
/*         {% if show_cashier_name %}*/
/*         {{ text_cashier }} <span id="cashier-name"></span>*/
/*         {% endif %}*/
/*         {% if show_customer_name %}*/
/*         <br />*/
/*         {{ text_customer }} <span id="customer-name-in"></span>*/
/*         {% endif %}*/
/*       </div>*/
/*       <br>*/
/*       <div class="thermal">*/
/*         {% if show_shipping_mode %}*/
/*         <span>{{ text_shipping_mode }}: </span><span class="oshipping">{{ text_pickup }}</span><br>*/
/*         {% endif %}*/
/*         {% if show_payment_mode %}*/
/*         <span>{{ text_payment_mode }}: </span><span class="opayment"></span>*/
/*         {% endif %}*/
/*       </div>*/
/*       <div class="table thermal">*/
/*         <table style="border-collapse: separate; border-spacing: 5px; margin: 0 auto; width: 95%;">*/
/*           <thead>*/
/*             <tr>*/
/*               <td style="text-align:left;">{{ column_product }}</td>*/
/*               <td>{{ column_quantity }}</td>*/
/*               <td>{{ column_price }}</td>*/
/*               <td style="text-align:right;">{{ column_amount }}</td>*/
/*             </tr>*/
/*           </thead>*/
/*           <tbody>*/
/*             <tr>*/
/*               <td style="text-align:left;">-------------{% if paper_size == '80mm' %}{{ '-------' }}{% endif %}</td>*/
/*               <td>-----{% if paper_size == '80mm' %}{{ '----' }}{% endif %}</td>*/
/*               <td>-------------</td>*/
/*               <td style="text-align:right;">--------------</td>*/
/*             </tr>*/
/*           </tbody>*/
/*           <tbody id="receiptProducts">*/
/*           </tbody>*/
/*           <tbody>*/
/*             <tr>*/
/*               <td colspan="4">----------------------------------------------------{% if paper_size == '80mm' %}{{ '------------------' }}{% endif %}{% if paper_size == '595px' %}{{ '-------------------------------------------------------------------------------------------' }}{% endif %}</td>*/
/*             </tr>*/
/*           </tbody>*/
/*           <tbody id="print-totals">*/
/*             <tr>*/
/*               <td></td>*/
/*               <td></td>*/
/*               <td>{{ text_subtotal }}</td>*/
/*               <td style="text-align:left;">00.00</td>*/
/*             </tr>*/
/*           </tbody>*/
/*           <tbody>*/
/*             <tr>*/
/*               <td id="total-quantity-text"></td>*/
/*               <td id="total-quantity" style="font-weight: bold;"></td>*/
/*               <td>{{ text_grandtotal }}</td>*/
/*               <td style="text-align: right;"><span class="oTotal">00.00</span></td>*/
/*             </tr>*/
/*           </tbody>*/
/*         </table>*/
/*         {% if show_note %}*/
/*         <div class="text-left">*/
/*           <strong>{{ text_note }}: </strong><span class="onote"></span>*/
/*         </div>*/
/*         {% endif %}*/
/*         <span>{{ text_nice_day }}</span><br>*/
/*         <span>{{ text_thank_you }}</span>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* */
/*   <noscript>*/
/*     <div class="no-js">{{ text_no_js }}</div>*/
/*   </noscript>*/
/*   <script language="JavaScript">*/
/*   var direction = '{{ direction }}';*/
/*     window.onbeforeunload = confirmExit;*/
/*     function confirmExit() {*/
/*       return "{{ text_close_warning }}";*/
/*     }*/
/*   </script>*/
/*   <script type="text/javascript">*/
/*   var currency              = "{{ currency }}",*/
/*   entry_slip_id            = "{{ entry_slip_id }}", */
/*   tax_sort_order            = {{ tax_sort_order }},*/
/*   entry_changed             = '{{ entry_changed }}',*/
/*   discount_sort_order       = {{ discount_sort_order }},*/
/*   currency_code             = "{{ currency_code }}",*/
/*   symbol_position           = "{{ symbol_position }}",*/
/*   user_login                = {{ user_login }},*/
/*   tax_status                = {{ tax_status }},*/
/*   message_error_credentials = "{{ error_credentials }}",*/
/*   entry_price               = "{{ entry_price }}",*/
/*   text_loading_products     = "{{ text_loading_products }}",*/
/*   error_load_products       = "{{ error_load_products }}",*/
/*   text_loading_populars     = "{{ text_loading_populars }}",*/
/*   error_load_populars       = "{{ error_load_populars }}",*/
/*   text_loading_orders       = "{{ text_loading_orders }}",*/
/*   error_load_orders         = "{{ error_load_orders }}",*/
/*   text_loading_customers    = "{{ text_loading_customers }}",*/
/*   error_load_customers      = "{{ error_load_customers }}",*/
/*   text_loading_categories   = "{{ text_loading_categories }}",*/
/*   error_load_categories     = "{{ error_load_categories }}",*/
/*   text_select               = "{{ text_select }}",*/
/*   text_none                 = "{{ text_none }}",*/
/*   text_loading              = "{{ text_loading }}",*/
/*   button_upload             = "{{ button_upload }}",*/
/*   button_cart               = "{{ button_cart }}",*/
/*   text_product_options      = "{{ text_product_options }}",*/
/*   error_keyword             = "{{ error_keyword }}",*/
/*   error_products            = "{{ error_products }}",*/
/*   text_online_mode          = "{{ text_online_mode }}",*/
/*   text_online               = "{{ text_online }}",*/
/*   error_enter_online        = "{{ error_enter_online }}",*/
/*   text_offline_mode         = "{{ text_offline_mode }}",*/
/*   text_offline              = "{{ text_offline }}",*/
/*   error_no_category_product = "{{ error_no_category_product }}",*/
/*   error_no_customer         = "{{ error_no_customer }}",*/
/*   text_select_customer      = "{{ text_select_customer }}",*/
/*   error_customer_add        = "{{ error_add_customer }}",*/
/*   text_remove_customer      = "{{ text_remove_customer }}",*/
/*   error_checkout            = "{{ error_checkout }}",*/
/*   text_balance_due          = "{{ text_balance_due }}",*/
/*   text_order_success        = "{{ text_order_success }}",*/
/*   text_customer_select      = "{{ text_customer_select }}",*/
/*   guest_name                = "{{ guest_name }}",*/
/*   text_item_detail          = "{{ text_item_details }}",*/
/*   text_sync_order           = "{{ text_sync_orders }}",*/
/*   text_no_orders            = "{{ text_no_orders }}",*/
/*   error_sync_orders         = "{{ error_sync_orders }}",*/
/*   text_another_cart         = "{{ text_select_cart }}",*/
/*   text_cart_deleted         = "{{ text_cart_deleted }}",*/
/*   text_current_deleted      = "{{ text_current_deleted }}",*/
/*   text_cart_empty           = "{{ text_cart_empty }}",*/
/*   text_empty_cart           = "{{ text_empty_cart }}",*/
/*   text_cart_add             = "{{ text_cart_add }}",*/
/*   text_option_required      = "{{ text_option_required }}",*/
/*   text_no_quantity          = "{{ text_no_quantity }}",*/
/*   text_product_added        = "{{ text_product_added }}",*/
/*   text_product_not_added    = "{{ text_product_not_added }}",*/
/*   text_product_removed      = "{{ text_product_removed }}",*/
/*   cash_payment_title        = "{{ cash_payment_title }}",*/
/*   text_order_id             = "{{ text_order_id }}",*/
/*   text_all_products         = "{{ text_all_products }}",*/
/*   text_search               = "{{ text_search }}",*/
/*   text_option_notifier      = "{{ text_option_notifier }}",*/
/*   show_lowstock_prod        = "{{ show_lowstock_prod }}",*/
/*   low_stock                 = "{{ low_stock }}",*/
/*   text_low_stock            = "{{ text_low_stock }}",*/
/*   text_special_price        = "{{ text_special_price }}",*/
/*   text_empty_hold           = "{{ text_empty_hold }}",*/
/*   text_success_logout       = "{{ text_success_logout }}",*/
/*   text_cust_add_select      = "{{ text_cust_add_select }}",*/
/*   text_register             = "{{ text_register }}",*/
/*   text_register_select      = "{{ text_register_select }}",*/
/*   error_save_setting        = "{{ error_save_setting }}",*/
/*   base_pos                  = "{{ base_pos }}",*/
/*   text_tendered_confirm     = "{{ text_tendered_confirm }}",*/
/*   text_card_confirm         = "{{ text_card_confirm }}",*/
/*   text_product_success      = "{{ text_product_success }}",*/
/*   error_product_name        = "{{ error_product_name }}",*/
/*   error_product_price       = "{{ error_product_price }}",*/
/*   error_product_quant       = "{{ error_product_quant }}",*/
/*   error_request_offline     = "{{ error_request_offline }}",*/
/*   error_supplier_range      = "{{ error_supplier_range }}",*/
/*   text_no_quantity_added    = "{{ text_no_quantity_added }}",*/
/*   error_select_product      = "{{ error_select_product }}",*/
/*   text_sure                 = "{{ text_sure }}",*/
/*   text_success_price_up     = "{{ text_success_price_up }}",*/
/*   text_price_remove         = "{{ text_price_remove }}",*/
/*   text_success              = "{{ text_success }}",*/
/*   text_warning              = "{{ text_warning }}",*/
/*   error_warning             = "{{ error_warning }}",*/
/*   text_coupon_remove        = "{{ text_coupon_remove }}",*/
/*   text_no_product           = "{{ text_no_product }}",*/
/*   text_success_add_disc     = "{{ text_success_add_disc }}",*/
/*   text_success_rem_disc     = "{{ text_success_rem_disc }}",*/
/*   error_cart_discount       = "{{ error_cart_discount }}",*/
/*   error_coupon_offline      = "{{ error_coupon_offline }}",*/
/*   error_supplier            = "{{ error_supplier }}",*/
/*   error_no_supplier         = "{{ error_no_supplier }}",*/
/*   heading_price_update      = "{{ heading_price_update }}",*/
/*   button_product_info       = "{{ button_product_info }}",*/
/*   error_mobile_view         = "{{ error_mobile_view }}",*/
/*   cashier                   = "{{ name }}",*/
/*   error_script              = "{{ error_script }}",*/
/*   error_price               = "{{ error_price }}",*/
/*   error_option_date         = "{{ error_option_date }}",*/
/*   // POS Update Code*/
/*   home_delivery_title       = "{{ home_delivery_title }}",*/
/*   text_credit_applied       = "{{ text_credit_applied }}",*/
/*   text_credit_removed       = "{{ text_credit_removed }}",*/
/*   text_pickup               = "{{ text_pickup }}",*/
/*   credit_status             = "{{ credit_status }}",*/
/*   heading_return            = "{{ heading_return }}",*/
/*   delivery_max              = "{{ delivery_max }}",*/
/*   text_product_info         = "{{ text_product_info }}",*/
/*   text_order_info           = "{{ text_order_info }}",*/
/*   text_loading_returns      = "{{ text_loading_returns }}",*/
/*   text_total                = "{{ text_total }}",*/
/*   text_return_details       = "{{ text_return_details }}",*/
/*   text_product_detail       = "{{ heading_product_detail }}",*/
/*   text_return_id            = "{{ text_return_id }}",*/
/*   text_no                   = "{{ text_no }}",*/
/*   text_yes                  = "{{ text_yes }}",*/
/*   text_order_date           = "{{ text_order_date }}",*/
/*   text_no_credit            = "{{ text_no_credit }}",*/
/*   text_return_date          = "{{ text_return_date }}",*/
/*   text_return_status        = "{{ text_return_status }}",*/
/*   text_comment              = "{{ text_comment }}",*/
/*   text_return_action        = "{{ text_return_action }}",*/
/*   text_no_order             = "{{ text_no_order }}",*/
/*   pricelist_status          = "{{ pricelist_status }}",*/
/*   text_price_changed        = "{{ text_price_changed }}",*/
/*   select_customer_first     = "{{ select_customer_first }}",*/
/*   button_credit             = "{{ button_credit }}",*/
/*   entry_date_ordered        = "{{ entry_date_ordered }}",*/
/*   entry_model               = "{{ entry_model }}",*/
/*   entry_quantity            = "{{ entry_quantity }}",*/
/*   entry_reason              = "{{ entry_reason }}",*/
/*   entry_action              = "{{ entry_action }}",*/
/*   entry_opened              = "{{ entry_opened }}",*/
/*   entry_fault_detail        = "{{ entry_fault_detail }}",*/
/*   entry_name                = "{{ entry_name }}",*/
/*   entry_customer            = "{{ entry_customer }}",*/
/*   entry_firstname           = "{{ entry_firstname }}",*/
/*   entry_lastname            = "{{ entry_lastname }}",*/
/*   entry_status              = "{{ entry_status }}",*/
/*   entry_telephone           = "{{ entry_telephone }}",*/
/*   entry_email               = "{{ entry_email }}",*/
/*   entry_opened1             = "{{ entry_opened1 }}",*/
/* */
/*   button_exchange           = "{{ button_exchange }}",*/
/*   button_return             = "{{ button_return }}",*/
/*   button_close              = "{{ button_close }}",*/
/*   button_submit             = "{{ button_submit }}",*/
/*   button_remove_credit      = "{{ button_remove_credit }}",*/
/*   return_heading            = "{{ return_heading }}",*/
/* */
/*   error_return_offline      = "{{ error_return_offline }}",*/
/*   error_check_qty           = "{{ error_check_qty }}",*/
/*   error_delivery_max        = "{{ error_delivery_max }}",*/
/*   error_return_product      = "{{ error_return_product }}",*/
/*   error_exchange_product    = "{{ error_exchange_product }}",*/
/*   error_load_returns        = "{{ error_load_returns }}",*/
/*   error_quantity_exceed     = "{{ error_quantity_exceed }}",*/
/*   error_credit_offline      = "{{ error_credit_offline }}",*/
/*   error_credit_amount       = "{{ error_credit_amount }}",*/
/*   error_offline_in_credit   = "{{ error_offline_credit }}",*/
/*   error_mobile_view_return  = "{{ error_mobile_return }}",*/
/*   error_offline_file        = "{{ error_offline_file }}";*/
/*   </script>*/
/* </body>*/
/* </html>*/
/* */
