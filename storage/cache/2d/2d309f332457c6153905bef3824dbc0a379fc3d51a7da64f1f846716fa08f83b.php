<?php

/* default/template/wkpos/wkpos_receipt.twig */
class __TwigTemplate_6f5750122a96e079b95c385c1273aef9fa73d4d283a4647fde937f088cab6bfc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<style type=\"text/css\">
 .thermal {
   width: ";
        // line 3
        echo (isset($context["paper_size"]) ? $context["paper_size"] : null);
        echo ";
   font-weight: ";
        // line 4
        echo (isset($context["font_weight"]) ? $context["font_weight"] : null);
        echo ";
   font-size: 12 px;
 
 }
 ";
        // line 8
        if (((isset($context["paper_size"]) ? $context["paper_size"] : null) == "595px")) {
            // line 9
            echo "  .thermal {
    max-width: 630px;
  }
  .font-store {
    font-size: 12px;
   
  }
 ";
        }
        // line 17
        echo " ";
        if (((isset($context["paper_size"]) ? $context["paper_size"] : null) == "58mm")) {
            // line 18
            echo "  .font-store {
    font-size: 11px;
  }
 ";
        }
        // line 22
        echo " ";
        if (((isset($context["paper_size"]) ? $context["paper_size"] : null) == "80mm")) {
            // line 23
            echo "  .font-store {
    font-size: 13px;
  }
 ";
        }
        // line 27
        echo "</style>
<div class=\"container thermal\" style=\" text-align:center; max-width: 630px; font-weight:400\">
  <div class=\"thermal\" style=\"margin: 0 auto;\">
    ";
        // line 30
        if ((isset($context["show_store_logo"]) ? $context["show_store_logo"] : null)) {
            // line 31
            echo "    <img src=\"";
            echo (isset($context["store_logo"]) ? $context["store_logo"] : null);
            echo "\" title=\"";
            echo (isset($context["store_name"]) ? $context["store_name"] : null);
            echo "\" alt=\"";
            echo (isset($context["store_name"]) ? $context["store_name"] : null);
            echo "\" class=\"img-responsive\" width=\"300\" style=\"margin: 0 auto;\"><br>
    ";
        }
        // line 33
        echo "
    ";
        // line 34
        if ((isset($context["show_header"]) ? $context["show_header"] : null)) {
            // line 35
            echo "      <span>";
            echo (isset($context["text_header"]) ? $context["text_header"] : null);
            echo "</span><br/>
    ";
        }
        // line 37
        echo "  
    ";
        // line 38
        if ((isset($context["show_store_name"]) ? $context["show_store_name"] : null)) {
            // line 39
            echo "    <span style=\"font-size: 14px; color: red; \">";
            echo (isset($context["store_name"]) ? $context["store_name"] : null);
            echo "</span><br>
    ";
        }
        // line 41
        echo "    ";
        if ((isset($context["show_store_address"]) ? $context["show_store_address"] : null)) {
            // line 42
            echo "    <span class=\"font-store\">";
            echo (isset($context["store_address"]) ? $context["store_address"] : null);
            echo "</span><br>
    ";
        }
        // line 44
        echo "    <div>
    ";
        // line 45
        if ((isset($context["store_detail"]) ? $context["store_detail"] : null)) {
            // line 46
            echo "    <span class=\"font-store\">";
            echo (isset($context["store_detail"]) ? $context["store_detail"] : null);
            echo " </span>  
    ";
        }
        // line 48
        echo "    ";
        if ((isset($context["show_order_date"]) ? $context["show_order_date"] : null)) {
            // line 49
            echo "    ";
            echo (isset($context["text_date"]) ? $context["text_date"] : null);
            echo " : <span class=\"order-date\"></span>
    ";
        }
        // line 50
        echo "    
    ";
        // line 51
        if ((isset($context["show_order_time"]) ? $context["show_order_time"] : null)) {
            // line 52
            echo "    ";
            echo (isset($context["text_time"]) ? $context["text_time"] : null);
            echo " <span class=\"order-time\"></span><br>
    ";
        }
        // line 54
        echo "    </div>
    <div style=\"text-align: left\">
    ";
        // line 56
        if ((isset($context["show_order_id"]) ? $context["show_order_id"] : null)) {
            echo " &nbsp; &nbsp; 
    <span class=\"order-txn\" Style=\"text-align : left; \">";
            // line 57
            echo (isset($context["text_order_id"]) ? $context["text_order_id"] : null);
            echo "</span> #<span class=\"oid\"></span>  &nbsp; &nbsp; &nbsp; 
    ";
        }
        // line 59
        echo "    ";
        if ((isset($context["show_cashier_name"]) ? $context["show_cashier_name"] : null)) {
            // line 60
            echo "    ";
            echo (isset($context["text_cashier"]) ? $context["text_cashier"] : null);
            echo " : <span id=\"cashier-name\"></span>
    ";
        }
        // line 62
        echo "    </div>  
    ";
        // line 63
        if ((isset($context["show_customer_name"]) ? $context["show_customer_name"] : null)) {
            // line 64
            echo "    <br />
    ";
            // line 65
            echo (isset($context["text_customer"]) ? $context["text_customer"] : null);
            echo " : <span id=\"customer-name-in\"></span>
    ";
        }
        // line 67
        echo "  </div>
  
  <div class=\"thermal\">
    ";
        // line 70
        if ((isset($context["show_shipping_mode"]) ? $context["show_shipping_mode"] : null)) {
            // line 71
            echo "    <span>";
            echo (isset($context["text_shipping_mode"]) ? $context["text_shipping_mode"] : null);
            echo ": </span><span class=\"oshipping\">";
            echo (isset($context["text_pickup"]) ? $context["text_pickup"] : null);
            echo "</span>
    ";
        }
        // line 73
        echo "    ";
        if ((isset($context["show_payment_mode"]) ? $context["show_payment_mode"] : null)) {
            // line 74
            echo "    <span>";
            echo (isset($context["text_payment_mode"]) ? $context["text_payment_mode"] : null);
            echo "</span><span class=\"opayment\"></span>
    ";
        }
        // line 76
        echo "  </div>
  <div class=\"table thermal\">
    <table style=\"border-collapse: collapse; border-spacing: 0px; margin: 0 auto; width: 95%;  \">
    
      <thead  style=\"border-top: 1px dotted ; border-bottom: 1px dotted ;\">
      
        <tr>
          <td style=\"text-align:left; max-width: 250px; \">";
        // line 83
        echo (isset($context["column_product"]) ? $context["column_product"] : null);
        echo "</td>
          <td>";
        // line 84
        echo (isset($context["column_quantity"]) ? $context["column_quantity"] : null);
        echo "</td>
          <td>";
        // line 85
        echo (isset($context["column_price"]) ? $context["column_price"] : null);
        echo "</td>
          <td style=\"text-align:right;\">";
        // line 86
        echo (isset($context["column_amount"]) ? $context["column_amount"] : null);
        echo "</td>
        </tr>
      </thead>

      
      <tbody id=\"receiptProducts\" >
      </tbody>

 <tbody id=\"print-totals\"  style=\"border-top: 1px dotted ; border-bottom: 1px dotted ;\">

            <tr>
              <td style=\"text-align: left; \" colspan=\"3\";>";
        // line 97
        echo (isset($context["text_subtotal"]) ? $context["text_subtotal"] : null);
        echo "</td>
              <td style=\"text-align:left;\">00.00</td>
            </tr >
          </tbody>
\t\t  
\t<tbody Style=\"border-top: 1px dotted ; border-bottom: 1px dotted ;\">
            <tr id=\"grandtotal-tr\">
              <!--td id=\"total-quantity-text\"></td>
              <td id=\"total-quantity\" style=\"font-weight: bold;\"></td -->
              
              <td style=\"text-align: left; \" colspan=\"3\"; >";
        // line 107
        echo (isset($context["text_grandtotal"]) ? $context["text_grandtotal"] : null);
        echo "</td>
              <td style=\"text-align: right;  \"><span class=\"oTotal\">00.00</span></td>
            </tr>
          </tbody>
      

    </table>
    ";
        // line 114
        if ((isset($context["show_note"]) ? $context["show_note"] : null)) {
            // line 115
            echo "    <div class=\"text-left\">
      <strong>";
            // line 116
            echo (isset($context["text_note"]) ? $context["text_note"] : null);
            echo ": </strong><span class=\"onote\"></span>
    </div>
    ";
        }
        // line 119
        echo "
    ";
        // line 120
        if ((isset($context["show_footer"]) ? $context["show_footer"] : null)) {
            // line 121
            echo "      <span>";
            echo (isset($context["text_footer"]) ? $context["text_footer"] : null);
            echo "</span>
    ";
        } else {
            // line 123
            echo "      <span>";
            echo (isset($context["text_nice_day"]) ? $context["text_nice_day"] : null);
            echo "</span><br>
      <span>";
            // line 124
            echo (isset($context["text_thank_you"]) ? $context["text_thank_you"] : null);
            echo "</span>
    ";
        }
        // line 126
        echo "
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "default/template/wkpos/wkpos_receipt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  292 => 126,  287 => 124,  282 => 123,  276 => 121,  274 => 120,  271 => 119,  265 => 116,  262 => 115,  260 => 114,  250 => 107,  237 => 97,  223 => 86,  219 => 85,  215 => 84,  211 => 83,  202 => 76,  196 => 74,  193 => 73,  185 => 71,  183 => 70,  178 => 67,  173 => 65,  170 => 64,  168 => 63,  165 => 62,  159 => 60,  156 => 59,  151 => 57,  147 => 56,  143 => 54,  137 => 52,  135 => 51,  132 => 50,  126 => 49,  123 => 48,  117 => 46,  115 => 45,  112 => 44,  106 => 42,  103 => 41,  97 => 39,  95 => 38,  92 => 37,  86 => 35,  84 => 34,  81 => 33,  71 => 31,  69 => 30,  64 => 27,  58 => 23,  55 => 22,  49 => 18,  46 => 17,  36 => 9,  34 => 8,  27 => 4,  23 => 3,  19 => 1,);
    }
}
/* <style type="text/css">*/
/*  .thermal {*/
/*    width: {{ paper_size }};*/
/*    font-weight: {{ font_weight }};*/
/*    font-size: 12 px;*/
/*  */
/*  }*/
/*  {% if paper_size == '595px' %}*/
/*   .thermal {*/
/*     max-width: 630px;*/
/*   }*/
/*   .font-store {*/
/*     font-size: 12px;*/
/*    */
/*   }*/
/*  {% endif %}*/
/*  {% if paper_size == '58mm' %}*/
/*   .font-store {*/
/*     font-size: 11px;*/
/*   }*/
/*  {% endif %}*/
/*  {% if paper_size == '80mm' %}*/
/*   .font-store {*/
/*     font-size: 13px;*/
/*   }*/
/*  {% endif %}*/
/* </style>*/
/* <div class="container thermal" style=" text-align:center; max-width: 630px; font-weight:400">*/
/*   <div class="thermal" style="margin: 0 auto;">*/
/*     {% if show_store_logo %}*/
/*     <img src="{{ store_logo }}" title="{{ store_name }}" alt="{{ store_name }}" class="img-responsive" width="300" style="margin: 0 auto;"><br>*/
/*     {% endif %}*/
/* */
/*     {% if show_header %}*/
/*       <span>{{ text_header }}</span><br/>*/
/*     {% endif %}*/
/*   */
/*     {% if show_store_name %}*/
/*     <span style="font-size: 14px; color: red; ">{{ store_name }}</span><br>*/
/*     {% endif %}*/
/*     {% if show_store_address %}*/
/*     <span class="font-store">{{ store_address }}</span><br>*/
/*     {% endif %}*/
/*     <div>*/
/*     {% if store_detail %}*/
/*     <span class="font-store">{{ store_detail }} </span>  */
/*     {% endif %}*/
/*     {% if show_order_date %}*/
/*     {{ text_date }} : <span class="order-date"></span>*/
/*     {% endif %}    */
/*     {% if show_order_time %}*/
/*     {{ text_time }} <span class="order-time"></span><br>*/
/*     {% endif %}*/
/*     </div>*/
/*     <div style="text-align: left">*/
/*     {% if show_order_id %} &nbsp; &nbsp; */
/*     <span class="order-txn" Style="text-align : left; ">{{ text_order_id }}</span> #<span class="oid"></span>  &nbsp; &nbsp; &nbsp; */
/*     {% endif %}*/
/*     {% if show_cashier_name %}*/
/*     {{ text_cashier }} : <span id="cashier-name"></span>*/
/*     {% endif %}*/
/*     </div>  */
/*     {% if show_customer_name %}*/
/*     <br />*/
/*     {{ text_customer }} : <span id="customer-name-in"></span>*/
/*     {% endif %}*/
/*   </div>*/
/*   */
/*   <div class="thermal">*/
/*     {% if show_shipping_mode %}*/
/*     <span>{{ text_shipping_mode }}: </span><span class="oshipping">{{ text_pickup }}</span>*/
/*     {% endif %}*/
/*     {% if show_payment_mode %}*/
/*     <span>{{ text_payment_mode }}</span><span class="opayment"></span>*/
/*     {% endif %}*/
/*   </div>*/
/*   <div class="table thermal">*/
/*     <table style="border-collapse: collapse; border-spacing: 0px; margin: 0 auto; width: 95%;  ">*/
/*     */
/*       <thead  style="border-top: 1px dotted ; border-bottom: 1px dotted ;">*/
/*       */
/*         <tr>*/
/*           <td style="text-align:left; max-width: 250px; ">{{ column_product }}</td>*/
/*           <td>{{ column_quantity }}</td>*/
/*           <td>{{ column_price }}</td>*/
/*           <td style="text-align:right;">{{ column_amount }}</td>*/
/*         </tr>*/
/*       </thead>*/
/* */
/*       */
/*       <tbody id="receiptProducts" >*/
/*       </tbody>*/
/* */
/*  <tbody id="print-totals"  style="border-top: 1px dotted ; border-bottom: 1px dotted ;">*/
/* */
/*             <tr>*/
/*               <td style="text-align: left; " colspan="3";>{{ text_subtotal }}</td>*/
/*               <td style="text-align:left;">00.00</td>*/
/*             </tr >*/
/*           </tbody>*/
/* 		  */
/* 	<tbody Style="border-top: 1px dotted ; border-bottom: 1px dotted ;">*/
/*             <tr id="grandtotal-tr">*/
/*               <!--td id="total-quantity-text"></td>*/
/*               <td id="total-quantity" style="font-weight: bold;"></td -->*/
/*               */
/*               <td style="text-align: left; " colspan="3"; >{{ text_grandtotal }}</td>*/
/*               <td style="text-align: right;  "><span class="oTotal">00.00</span></td>*/
/*             </tr>*/
/*           </tbody>*/
/*       */
/* */
/*     </table>*/
/*     {% if show_note %}*/
/*     <div class="text-left">*/
/*       <strong>{{ text_note }}: </strong><span class="onote"></span>*/
/*     </div>*/
/*     {% endif %}*/
/* */
/*     {% if show_footer %}*/
/*       <span>{{ text_footer }}</span>*/
/*     {% else %}*/
/*       <span>{{ text_nice_day }}</span><br>*/
/*       <span>{{ text_thank_you }}</span>*/
/*     {% endif %}*/
/* */
/*   </div>*/
/* </div>*/
